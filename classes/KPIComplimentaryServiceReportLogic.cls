public with sharing class KPIComplimentaryServiceReportLogic implements KPIReportInterface {

	public KPIComplimentaryServiceReportLogic() {}
	
	public KPI_Value__c execute(KPI_Value__c kpiValue) {
		AggregateResult[] result = [
			select	sum(Total_Cost__c) total
			from	Complimentary_Service__c
			where	Time_Period__c = :kpiValue.Time_Period__c
		];
		
		double returnValue = 0;
		
		if (result != null && result.size() > 0) {
			returnValue = (Double)result[0].get('total');
			if (returnValue == null) {
				returnValue = 0;
			}
		} 
		
		kpiValue.Actual__c = returnValue;
		
		return kpiValue;
	}

}