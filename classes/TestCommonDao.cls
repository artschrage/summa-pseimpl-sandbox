/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCommonDao {

    static testMethod void testPreviousQuarterFetch() {
		stageTestData();
		CommonDao dao = new CommonDao();
		
		Test.startTest();
		pse__Time_Period__c timePeriod = dao.fetchPreviousQuarterTimePeriod();
		system.assertEquals('Last Quarter', timePeriod.Name, 'Incorrect time period returned.');
		Test.stopTest();
    }

    static testMethod void testCurrentQuarterFetch() {
		stageTestData();
		CommonDao dao = new CommonDao();
		
		Test.startTest();
		pse__Time_Period__c timePeriod = dao.fetchCurrentQuarterTimePeriod();
		system.assertEquals('This Quarter', timePeriod.Name, 'Incorrect time period returned.');
		Test.stopTest();
    }

    static testMethod void testFutureQuarterFetch() {
		stageTestData();
		CommonDao dao = new CommonDao();
		
		Test.startTest();
		pse__Time_Period__c timePeriod = dao.fetchNextQuarterTimePeriod();
		system.assertEquals('Next Quarter', timePeriod.Name, 'Incorrect time period returned.');
		Test.stopTest();
    }

    static testMethod void testAccountFetch() {
		stageTestData();
		CommonDao dao = new CommonDao();
		
		Test.startTest();
		List<Account> accounts = dao.fetchAccountsWithIdOnly();
		system.assertEquals(6, accounts.size(), 'Incorrect number of accounts returned.');
		Test.stopTest();
    }

    private static void stageTestData() {
        //setup accounts with corresponding opportunities
        
        Account accountA = new Account(Name='Acct A Prior Only', CurrencyIsoCode='USD');
        Account accountB = new Account(Name='Acct B Current Only', CurrencyIsoCode='USD');
        Account accountC = new Account(Name='Acct C Future Only', CurrencyIsoCode='USD');
        Account accountD = new Account(Name='Acct D Prior and Current', CurrencyIsoCode='USD');
        Account accountE = new Account(Name='Acct E Current and Future', CurrencyIsoCode='USD');
        Account accountF = new Account(Name='Acct F Prior and Current and Future', CurrencyIsoCode='USD');

		List<Account> newAccounts = new List<Account>();
		newAccounts.add(accountA);
		newAccounts.add(accountB);
		newAccounts.add(accountC);
		newAccounts.add(accountD);
		newAccounts.add(accountE);
		newAccounts.add(accountF);
		insert newAccounts;

		Date now = Date.today();
		decimal thisQuarterMonth = Decimal.valueOf(now.month());
		thisQuarterMonth = (math.ceil(thisQuarterMonth.divide(3,2))*3)-2;

		Date startCurrentQuarter = Date.newInstance(now.year(), Integer.valueOf(thisQuarterMonth), 1);
		Date startPriorQuarter = startCurrentQuarter.addMonths(-3);
		Date startNexQuarter = startCurrentQuarter.addMonths(3);
		
		system.debug('startCurrentQuarter = ' + startCurrentQuarter);
		system.debug('startPriorQuarter = ' + startPriorQuarter);
		system.debug('startNexQuarter = ' + startNexQuarter);
		
		List<pse__Time_Period__c> periods = new List<pse__time_period__c>();
		periods.add(new pse__Time_Period__c(Name='This Quarter', pse__Type__c='Quarter', pse__Start_Date__c=startCurrentQuarter, pse__End_Date__c=startNexQuarter.addDays(-1)));
		periods.add(new pse__Time_Period__c(Name='Last Quarter', pse__Type__c='Quarter', pse__Start_Date__c=startPriorQuarter, pse__End_Date__c=startCurrentQuarter.addDays(-1)));
		periods.add(new pse__Time_Period__c(Name='Next Quarter', pse__Type__c='Quarter', pse__Start_Date__c=startNexQuarter, pse__End_Date__c=startNexQuarter.addMonths(3).addDays(-1)));
		insert periods;
    }
}