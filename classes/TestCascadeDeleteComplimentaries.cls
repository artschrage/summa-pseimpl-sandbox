/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCascadeDeleteComplimentaries {

    static testMethod void testDelete() {
        stageDataDelete();
        Integer beforeCount = [select count() from Complimentary_Service__c];
        
        Test.startTest();
        pse__Proj__c project = [select id from pse__Proj__c where name = 'Test Project'];
        system.assertEquals(3, beforeCount);
        delete project;
        Integer afterCount = [select count() from Complimentary_Service__c];
        system.assertEquals(0, afterCount);
        Test.stopTest();
    }
    
    private static void stageDataDelete() {
    	//region
    	pse__Region__c regionCorporate = new pse__Region__c(Name='Corporate', CurrencyIsoCode='USD');
    	insert regionCorporate;
    	
    	//practice
    	pse__Practice__c practiceCore = new pse__Practice__c(Name='Core', CurrencyIsoCode='USD');
    	insert practiceCore;
    	
    	//contact
    	String currentUserId = UserInfo.getUserId();
    	Contact johnSmail = new Contact(FirstName='John', LastName='Smail', CurrencyIsoCode='USD', pse__Is_Resource__c=TRUE, pse__Is_Resource_Active__c=TRUE, pse__Region__c=regionCorporate.Id, pse__Practice__c=practiceCore.Id, pse__Salesforce_User__c=currentUserId);
    	insert johnSmail;
    	
    	pse__Permission_Control__c permission = new pse__Permission_Control__c(
	    	pse__Billing__c=TRUE,
			pse__Cascading_Permission__c=TRUE,
			pse__Expense_Entry__c=TRUE,
			pse__Expense_Ops_Edit__c=TRUE,
			pse__Forecast_Edit__c=TRUE,
			pse__Forecast_View__c=TRUE,
			pse__Invoicing__c=TRUE,
			pse__Region__c=regionCorporate.Id,
			pse__Resource_Request_Entry__c=TRUE,
			pse__Staffing__c=TRUE,
			pse__Timecard_Entry__c=TRUE,
			pse__Timecard_Ops_Edit__c=TRUE,
			pse__User__c=currentUserId);
		insert permission;
		    	
    	//account
    	List<Account> accounts = new List<Account>();
    	accounts.add(new Account(Name='Account A', CurrencyIsoCode='USD'));
    	accounts.add(new Account(Name='Account B', CurrencyIsoCode='USD'));
		insert accounts;
		
		//time periods
		List<pse__Time_Period__c> timePeriods = new List<pse__Time_Period__c>();
		pse__Time_Period__c janfy14 = new pse__Time_Period__c(Name='Jan FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 1, 1), pse__End_Date__c=Date.newInstance(2014, 1, 31));
		pse__Time_Period__c q3fy14 = new pse__Time_Period__c(Name='Q3 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2014, 1, 1), pse__End_Date__c=Date.newInstance(2014, 3, 31));
		pse__Time_Period__c fy14 = new pse__Time_Period__c(Name='FY2014', pse__Type__c='Year', pse__Start_Date__c=Date.newInstance(2014, 4, 1), pse__End_Date__c=Date.newInstance(2014, 6, 30));

		timePeriods.add(janfy14);
		timePeriods.add(q3fy14);
		timePeriods.add(fy14);
		insert timePeriods;
    	
    	Account account = [select id from Account where name = 'Account A'];
    	List<pse__Time_Period__c> periods = [select id from pse__Time_Period__c];
    	 
		pse__Proj__c project = new pse__Proj__c();
	    project.Name = 'Test Project';
	    project.pse__Account__c = account.Id;
	    project.pse__Is_Active__c = true; 
	    project.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	    project.pse__Billing_Type__c = 'Time and Materials';
	    project.pse__Start_Date__c = Date.newInstance(2014,1,1);
	    project.pse__End_Date__c = Date.newInstance(2014,1,31);
	    project.Date_Became_Inactive__c = null;
	    project.pse__Project_Manager__c = johnSmail.Id;
	    project.Delivery_Engagement_Manager__c = johnSmail.Id;
	    project.pse__Region__c = regionCorporate.Id;
	    project.pse__Practice__c = practiceCore.Id;
	    insert project;

		list<Complimentary_Service__c> services = new list<Complimentary_Service__c>();
		Complimentary_Service__c service = new Complimentary_Service__c();
		
		service.Project__c = project.Id;
		service.Time_Period__c = janfy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		service = new Complimentary_Service__c();
		service.Project__c = project.Id;
		service.Time_Period__c = q3fy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		service = new Complimentary_Service__c();
		service.Project__c = project.Id;
		service.Time_Period__c = fy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);
		
		insert services;
    }
    
}