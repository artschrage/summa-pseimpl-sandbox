/**
 * NAME: LMSCertificateTest
 * DEVELOPER: Andrey Melnikov
 * CREATED DATE: 2013/11/29
 *
 * DESCRIPTION:
 * Test controller for LMSCertificate class
 *
 * CHANGE LOG
 * DEVELOPER NAME             CHANGE DESCRIPTION                      MODIFY DATE
 * ------------------------------------------------------------------------------
 * Andrey Melnikov               Created class                           2013/11/29
 */


@isTest
private class LMSCertificateTest {
/**
    * Method: testCertificate2
    *
    * Test gid=NULL parameter
    *
    * @param None
    * @return void
    */
    public static testmethod void testCertificate2(){
        test.startTest();
        //created date for test
        Profile profile = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'test', email='test@test.com', emailencodingkey='UTF-8', lastname='test', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, timezonesidkey='America/Los_Angeles', username='test@testSF22223c.com', lmscons__Cornerstone_ID__c='newtest 261');
        insert u;
        lmscons__Transcript__c t = new lmscons__Transcript__c(lmscons__Trainee__c = u.id);
        insert t;

        system.runAs(u){

            LMSCertificate TCCD3 = new LMSCertificate ();
            System.assertEquals(TRUE, TRUE);
        }

        System.assertEquals(TRUE, TRUE);
    }
/**
    * Method: testCertificate2
    *
    * Test every type object
    *
    * @param None
    * @return void
    */

    public static testmethod void testCertificate(){
        test.startTest();

        //created data for test
        Profile profile = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'test', email='test@test.com', emailencodingkey='UTF-8', lastname='test', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, timezonesidkey='America/Los_Angeles', username='test@testSF22223c.com', lmscons__Cornerstone_ID__c='newtest 261');
        insert u;
        lmscons__Transcript__c t = new lmscons__Transcript__c(lmscons__Trainee__c = u.id);
        insert t;

        system.runAs(u){
            lmscons__Training_Content__c c = new lmscons__Training_Content__c( lmscons__Title__c = 'Training me', lmscons__Cornerstone_ID__c = 'af' );
            insert c;
            lmscons__Training_Content_License__c cl =  new lmscons__Training_Content_License__c( lmscons__Training_Content__c = c.id );
            insert cl;
            lmscons__Training_User_License__c tul = new lmscons__Training_User_License__c( lmscons__Content_License__c = cl.id, lmscons__User__c = u.id  );
            insert tul;
            lmscons__Training_Path__c tp = new lmscons__Training_Path__c( name = 'andy path');
            insert tp;
            lmscons__Training_Path_Item__c ti = new lmscons__Training_Path_Item__c(lmscons__Training_Path__c = tp.id, lmscons__Training_Content__c=c.id, lmscons__Sequence__c=1);
            insert ti;
            lmscons__Transcript_Line__c tl = new lmscons__Transcript_Line__c(lmscons__Training_Path_Item__c=ti.id, lmscons__Transcript__c = t.Id, lmscons__Training_User_License__c = tul.id  );
            insert tl;
            lmscons__Training_Path_Assignment_Progress__c tpap = new lmscons__Training_Path_Assignment_Progress__c(lmscons__Transcript__c = t.id, lmscons__Training_Path__c = tp.id);
            insert(tpap);
            lmscons__Learning_Path__c LP = new lmscons__Learning_Path__c(Name = 'LP Name');
            insert LP;
            lmscons__Learning_Path_Item__c lpi = new lmscons__Learning_Path_Item__c(lmscons__Learning_Path__c = LP.id, lmscons__Module__c = c.id);
            insert lpi;
            lmscons__Learning_Path_Assignment__c LPA = new lmscons__Learning_Path_Assignment__c(lmscons__Transcript__c = t.id, lmscons__Learning_Path__c = LP.id);
            insert LPA;

            lmscons__Curriculum__c Cu = new lmscons__Curriculum__c(Name = 'Cu Name');
            insert Cu;
            lmscons__Curriculum_Item__c Cui = new lmscons__Curriculum_Item__c(lmscons__Curriculum__c = Cu.id, lmscons__Module__c = c.id);
            insert Cui;
            lmscons__Curriculum_Assignment__c CuA = new lmscons__Curriculum_Assignment__c(lmscons__Transcript__c = t.id, lmscons__Curriculum__c = Cu.id);
            insert CuA;
            test.stopTest();

            //test every Learning Object
            ApexPages.currentPage().getParameters().put('id',tl.id);

            LMSCertificate TCCD = new LMSCertificate ();
            System.assertEquals('Training me', TCCD.title);

            ApexPages.currentPage().getParameters().put('id',tpap.id);

            LMSCertificate TCCD2 = new LMSCertificate ();
            System.assertEquals('andy path', TCCD2.title);

            ApexPages.currentPage().getParameters().put('id',LPA.id);

            LMSCertificate TCCD3 = new LMSCertificate ();
            System.assertEquals('LP Name', TCCD3.title);

            ApexPages.currentPage().getParameters().put('id',CuA.id);

            LMSCertificate TCCD4 = new LMSCertificate ();
            System.assertEquals('Cu Name', TCCD4.title);
        }

        System.assertEquals(TRUE, TRUE);
    }
}