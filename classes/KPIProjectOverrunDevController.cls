public with sharing class KPIProjectOverrunDevController {

	private final KPIComplimentaryServiceReportLogic reportEngine;

	public String CurrentQuarter { get; set; }
	private sObject currentPeriod;
	private List<pse__Time_Period__c> periods;
	private KPI_Value__c kpi;
	private String kpiTitle = 'Project Overruns';
	
	public KPIProjectOverrunDevController() {
		reportEngine = new KPIComplimentaryServiceReportLogic();
	}
	
	public List<SelectOption> getAllPeriods() {
		periods = [SELECT Id, Name from pse__Time_Period__c where pse__Type__c = 'Quarter' and pse__Start_Date__c <= today order by pse__Start_Date__c DESC LIMIT 6];

		List<SelectOption> options = new List<SelectOption>();
		for(pse__Time_Period__c period : periods) {
			options.add(new SelectOption(period.Name, period.Name));
		}
		return options;
	}
	
	public PageReference runReport() {
		currentPeriod = [SELECT Id FROM pse__Time_Period__c WHERE Name = :CurrentQuarter];
		kpi = [SELECT Name, Title__c, Target__c, Actual__c, Time_Period__c from KPI_Value__c where Title__c = :kpiTitle and Time_Period__c  = :currentPeriod.Id];
		
		System.debug('Current Quarter is now: ' + CurrentQuarter);
		System.debug('Current Period is now: ' + currentPeriod);
		System.debug('KPI is now: ' + kpi.Title__c);

		return null;
	}
}