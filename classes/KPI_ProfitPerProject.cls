public class KPI_ProfitPerProject extends KPIBase {
	/**
		Implement the method that calculates the KPI value
		for the KPI returned by the implementation in
		the specified time period.
	*/
    public override KPI_Value__c calculateImpl(pse__Time_Period__c timePeriod) {
    	// Retrieve the KPI value for the time period and 
    	// KPI.  If no record exists, then create one with zero
    	// actuals
    	KPI_Value__c kpiValue = super.getKPIValue(timePeriod);
    	if (kpiValue == null) {
    		System.debug('No existing value found');
    		kpiValue = new KPI_Value__c();
    		kpiValue.KPI__c = getTargetKPI().Id;
    		kpiValue.Time_Period__c = timePeriod.Id;
			kpiValue.Actual__c = 0.0;
    	}
    	else {
    		System.debug('Existing value found');
    	}
    	Decimal actual = 0.0;

        AggregateResult[] results = [SELECT  
	            SUM(pse__Billings__c) billsum, SUM(pse__Total_Costs__c) costsum
	            FROM pse__Project_Actuals__c 
		        WHERE pse__Time_Period__c = :timePeriod.Id and 
		        pse__Project__r.pse__Master_Project__r.Name = '' AND 
		        pse__Project__r.pse__Practice__r.Name != '' AND 
		        pse__Project__r.pse__Is_Billable__c = true];
		        
        for (AggregateResult result : results) {
            Decimal costsum = (Decimal) result.get('costsum');
            Decimal billsum = (Decimal) result.get('billsum');
            System.debug(LoggingLevel.FINE, 'Total Cost: ' + costsum);
            System.debug(LoggingLevel.FINE, 'Billings: ' + billsum);
            
            if (billsum == 0.0) {
            	actual = 0.0;
            }
            else {
	            actual = billsum - costsum;
            }
            System.debug(LoggingLevel.FINE, 'Profit Per Project: ' + actual);
        }
        
        kpiValue.Actual__c = actual;
        System.debug(kpiValue);
		return kpiValue;
    }
    
    public override KPI__c getTargetKPI() {
        KPI__c kpi = [select Id, Name from KPI__c where Name = 'Profit per Project' limit 1];
        return kpi;
    }
}