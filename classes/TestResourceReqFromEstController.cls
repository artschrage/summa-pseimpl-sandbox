@isTest(SeeAllData=true)
private class TestResourceReqFromEstController{

    private static final String OPP_ID = '006f0000003wll1';
    private static final String GOOGLE_CODE = '4/LPRf0x6EpZUCHM_5ofVfHgMDaUta.cipqj1IrAksfaDn_6y0ZQNjILzr7jgI';
    private static final String SS_URL = 'https://docs.google.com/a/summa-tech.com/spreadsheet/ccc?key=0AnZQyrzWPTSedG1xTmJPUTh1Y2RuZkkyYW5kcG9wZGc#gid=25';

    static testMethod void testController200(){
		
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TestResourceReqMockHttpResponse());

        // Create a test Resource Request
        pse__Resource_Request__c[] rrs = new pse__Resource_Request__c[]{};
        rrs.add(new pse__Resource_Request__c(
        	pse__Resource_Role__c='Consultant'
        ));

        upsert new Opp_Resource_Doc_Link__c(Name = UserInfo.getUserId() + OPP_ID, Google_Doc_Link__c = SS_URL);

        // Start the test
        Test.startTest();

        PageReference startPage = Page.CreateResourceRequestFromEstimates;
		startPage.getParameters().put('code', GOOGLE_CODE); 
        startPage.getParameters().put('state', OPP_ID); 
		Test.setCurrentPage(startPage); //as if it came from browser
		
		ResourceRequestFromEstimatesController c = new ResourceRequestFromEstimatesController(new ApexPages.StandardSetController(rrs));

        Boolean booCode = c.code;
        String googleDocLink = c.GoogleDocLink;
        c.exchangeRequestToken();
        c.importResourceRequests();

        // Stop the test
        Test.stopTest();

    }

    static testMethod void testController403(){
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TestResourceReqMockHttpResponse403());

        // Create a test Resource Request
        pse__Resource_Request__c[] rrs = new pse__Resource_Request__c[]{};
        rrs.add(new pse__Resource_Request__c(
            pse__Resource_Role__c='Consultant'
        ));

        upsert new Opp_Resource_Doc_Link__c(Name = UserInfo.getUserId() + OPP_ID, Google_Doc_Link__c = SS_URL);

        // Start the test
        Test.startTest();

        PageReference startPage = Page.CreateResourceRequestFromEstimates;
        startPage.getParameters().put('code', GOOGLE_CODE); 
        startPage.getParameters().put('state', OPP_ID); 
        Test.setCurrentPage(startPage); //as if it came from browser
        
        ResourceRequestFromEstimatesController c = new ResourceRequestFromEstimatesController(new ApexPages.StandardSetController(rrs));

        Boolean booCode = c.code;
        String googleDocLink = c.GoogleDocLink;
        c.exchangeRequestToken();
        c.importResourceRequests();

        // Stop the test
        Test.stopTest();

    }

    static testMethod void testController404(){
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TestResourceReqMockHttpResponse404());

        // Create a test Resource Request
        pse__Resource_Request__c[] rrs = new pse__Resource_Request__c[]{};
        rrs.add(new pse__Resource_Request__c(
            pse__Resource_Role__c='Consultant'
        ));

        upsert new Opp_Resource_Doc_Link__c(Name = UserInfo.getUserId() + OPP_ID, Google_Doc_Link__c = SS_URL);

        // Start the test
        Test.startTest();

        PageReference startPage = Page.CreateResourceRequestFromEstimates;
        startPage.getParameters().put('code', GOOGLE_CODE); 
        startPage.getParameters().put('state', OPP_ID); 
        Test.setCurrentPage(startPage); //as if it came from browser
        
        ResourceRequestFromEstimatesController c = new ResourceRequestFromEstimatesController(new ApexPages.StandardSetController(rrs));

        Boolean booCode = c.code;
        String googleDocLink = c.GoogleDocLink;
        c.exchangeRequestToken();
        c.importResourceRequests();

        // Stop the test
        Test.stopTest();

    }

}