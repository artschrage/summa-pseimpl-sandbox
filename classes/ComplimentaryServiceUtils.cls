public with sharing class ComplimentaryServiceUtils {
	
	public static Date toFirstOfMonth(Date param) {
		if (param != null) {
			return Date.newInstance(param.year(), param.month(), 1);
		} else {
			return null;
		}
	}
	
	public static Date toLastOfMonth(Date param) {
		if (param != null) {
			return Date.newInstance(param.year(), param.month(), Date.daysInMonth(param.year(), param.month()));
		} else {
			return null;
		}
	}
	
	public static list<pse__Time_Period__c> getMatchingTimePeriods(list<pse__Time_Period__c> allTimePeriods, Date projectStartDate, Date projectEndDate) {
		List<pse__Time_Period__c> returnValue = new List<pse__Time_Period__c>();

		for (pse__Time_Period__c timePeriod : allTimePeriods) {
			if ((timePeriod.pse__Start_Date__c <= projectStartDate && timePeriod.pse__End_Date__c >= projectStartDate) ||
				(timePeriod.pse__Start_Date__c >= projectStartDate && timePeriod.pse__End_Date__c <= projectEndDate) ||
				(timePeriod.pse__Start_Date__c <= projectEndDate && timePeriod.pse__End_Date__c >= projectEndDate)) {
					returnValue.add(timePeriod);
				}
		}
		
		return returnValue;
	}

	public static Map<pse__Time_Period__c,double> buildAccumulatorMap(List<pse__Time_Period__c> timePeriods) {
		Map<pse__Time_Period__c,double> returnValue = new Map<pse__Time_Period__c,double>();

		if (timePeriods != null){
			for (pse__Time_Period__c tp : timePeriods) {
				returnValue.put(tp, 0);
			}
		}
		
		return returnValue;
	}

	public static Map<pse__Time_Period__c,double> aggregateAccumulatorPeriods(Map<pse__Time_Period__c,double> granularMap, List<pse__Time_Period__c> coarseTimePeriods) {
		Map<pse__Time_Period__c,double> coarseMap = new Map<pse__Time_Period__c,double>();
		
		pse__Time_Period__c coarseTimePeriod = null;
		for (pse__Time_Period__c granularTimePeriod : granularMap.keySet()) {
			coarseTimePeriod = getContainingTimePeriod(granularTimePeriod, coarseTimePeriods);
			if (coarseMap.containsKey(coarseTimePeriod)) {
				coarseMap.put(coarseTimePeriod, coarseMap.get(coarseTimePeriod) + granularMap.get(granularTimePeriod));
			} else {
				coarseMap.put(coarseTimePeriod, granularMap.get(granularTimePeriod));
			}
		}
		
		return coarseMap;
	}
	
	public static pse__Time_Period__c getTimePeriodFromDate(Date minDate, List<pse__Time_Period__c> timePeriods) {
		pse__Time_Period__c returnValue = null;
		
		if (minDate != null && timePeriods != null) {
			for (pse__Time_Period__c tp : timePeriods) {
				if (tp.pse__Start_Date__c <= minDate && tp.pse__End_Date__c >= minDate) {
					returnValue = tp;
					break;
				}
			}
		}
		return returnValue;
	}
	
	public static List<pse__Time_Period__c> getTimePeriodsByType(List<pse__Time_Period__c> fullList, string periodType) {
		List<pse__Time_Period__c> returnValue = new List<pse__Time_Period__c>();
		
		for (pse__Time_Period__c timePeriod : fullList) {
			if (periodType.equalsIgnoreCase(timePeriod.pse__Type__c)) {
				returnValue.add(timePeriod);
			}
		}
		
		return returnValue;
	}
	
	public static pse__Time_Period__c getContainingTimePeriod(pse__Time_Period__c granularTimePeriod, List<pse__Time_Period__c> coarseTimePeriods) {
		pse__Time_Period__c returnValue = null;
		
		for (pse__Time_Period__c coarseTimePeriod : coarseTimePeriods) {
			if (coarseTimePeriod.pse__Start_Date__c <= granularTimePeriod.pse__Start_Date__c && coarseTimePeriod.pse__End_Date__c >= granularTimePeriod.pse__End_Date__c) {
				returnValue = coarseTimePeriod;
				break;
			}
		}
		
		return returnValue;
	}
	
}