@isTest
private class KPIHitRatioTest {
    static KPI__c kpi;
    static KPI_Value__c kpiValuePQ;
    static KPI_Value__c kpiValueFY;
    static pse__Time_Period__c prevQuarter;
    static pse__Time_Period__c currQuarter;
    static pse__Time_Period__c fiscalYear;
    static Decimal expected = 2.0 / 3.0;
    static List<Opportunity> opps = new List<Opportunity>();

    static {
        createKPI();
        createOpportunities();
    }

    static testMethod void testHitRatioHitRatioCountPFQ() {
        KPIHitRatioCountReportLogic logic = new KPIHitRatioCountReportLogic();
        kpiValuePQ = logic.execute(kpiValuePQ);
        System.debug(kpiValuePQ.Actual__c);
        System.assert(kpiValuePQ.Actual__c == expected);
    }
    
    static testMethod void testHitRatioHitRatioAmountPFQ() {
        KPIHitRatioAmountReportLogic logic = new KPIHitRatioAmountReportLogic();
        kpiValuePQ = logic.execute(kpiValuePQ);
        System.debug(kpiValuePQ.Actual__c);
        System.assert(kpiValuePQ.Actual__c == expected);
    }

    static testMethod void testHitRatioHitRatioCountYTD() {
        KPIHitRatioCountReportLogic logic = new KPIHitRatioCountReportLogic();
        kpiValueFY = logic.execute(kpiValueFY);
        System.debug(kpiValueFY.Actual__c);
        System.assert(kpiValueFY.Actual__c == expected);
    }
    
    static testMethod void testHitRatioHitRatioAmountYTD() {
        KPIHitRatioAmountReportLogic logic = new KPIHitRatioAmountReportLogic();
        kpiValueFY = logic.execute(kpiValueFY);
        System.debug(kpiValueFY.Actual__c);
        System.assert(kpiValueFY.Actual__c == expected);
    }
    
    static testMethod void testHitRatioBaseClass() {
    	KPIHitRatioBaseReportLogicTest test = 
    						new KPIHitRatioBaseReportLogicTest();
		test.buildHistoryMapTest();
    }
    
    static void createKPI() {
        kpi = new KPI__c();
        kpi.Name = 'Test KPI';
        insert kpi;
        
        kpi = [SELECT Id from KPI__c where Name = 'Test KPI'];
        System.debug('KPI ID = ' + kpi.Id);

        prevQuarter = new pse__Time_Period__c();
        prevQuarter.Name = 'Q3FY2014';
        prevQuarter.pse__Start_Date__c = Date.newInstance(2014, 1, 1);
        prevQuarter.pse__End_Date__c = Date.newInstance(2014, 3, 31);
        prevQuarter.pse__Type__c = 'Quarter';
        insert prevQuarter;
        prevQuarter = [SELECT Id, pse__Start_Date__c, pse__End_Date__c 
                                from pse__Time_Period__c where Name = 'Q3FY2014'];
        System.debug('Prev Q ID = ' + prevQuarter.Id);
        
        currQuarter = new pse__Time_Period__c();
        currQuarter.Name = 'Q4FY2014';
        currQuarter.pse__Start_Date__c = Date.newInstance(2014, 4, 1);
        currQuarter.pse__End_Date__c = Date.newInstance(2014, 6, 30);
        currQuarter.pse__Type__c = 'Quarter';
        insert currQuarter;
        currQuarter = [SELECT Id, pse__Start_Date__c, pse__End_Date__c 
                                from pse__Time_Period__c where Name = 'Q4FY2014'];
        System.debug('Curr Q ID = ' + currQuarter.Id);

        fiscalYear = new pse__Time_Period__c();
        fiscalYear.Name = 'FY2014';
        fiscalYear.pse__Start_Date__c = Date.newInstance(2013, 7, 1);
        fiscalYear.pse__End_Date__c = Date.newInstance(2014, 6, 30);
        fiscalYear.pse__Type__c = 'Year';
        insert fiscalYear;
        fiscalYear= [SELECT Id, pse__Start_Date__c, pse__End_Date__c 
                                from pse__Time_Period__c where Name = 'FY2014'];
        System.debug('Fiscal Year ID = ' + fiscalYear.Id);

        kpiValuePQ = new KPI_Value__c();
        kpiValuePQ.KPI__c = kpi.Id;
        kpiValuePQ.Time_Period__c = prevQuarter.Id;
        String altKey = kpi.Id + ' ' + prevQuarter.Id;
        kpiValuePQ.Alternate_Key__c = altKey;
        insert kpiValuePQ;
        kpiValuePQ = [SELECT Id, Time_Period__c, 
                Time_Period__r.pse__Start_Date__c, Time_Period__r.pse__End_Date__c 
                from KPI_Value__c where Alternate_Key__c = :altKey];
        System.debug('KPIValuePQ ID = ' + kpiValuePQ.Id);
        
        kpiValueFY = new KPI_Value__c();
        kpiValueFY.KPI__c = kpi.Id;
        kpiValueFY.Time_Period__c = fiscalYear.Id;
        altKey = kpi.Id + ' ' + fiscalYear.Id;
        kpiValueFY.Alternate_Key__c = altKey;
        insert kpiValueFY;
        kpiValueFY = [SELECT Id, Time_Period__c, 
                Time_Period__r.pse__Start_Date__c, Time_Period__r.pse__End_Date__c 
                from KPI_Value__c where Alternate_Key__c = :altKey];
        System.debug('KPIValueFY ID = ' + kpiValueFY.Id);
    }
    
    static void createOpportunities() {
        Account account = new Account(Name='TestAcct');
        insert account;
        account = [Select Id from Account where Name = 'TestAcct'];
        System.debug('Account Id = ' + account.Id);
        
        pse__Practice__c practice = new pse__Practice__c(Name='Core');
        insert practice;
        practice = [Select Id from pse__Practice__c where Name = 'Core'];
        System.debug('Practice Id = ' + practice.Id);

        practice = new pse__Practice__c(Name='CRM');
        insert practice;
        practice = [Select Id from pse__Practice__c where Name = 'CRM'];
        System.debug('Practice Id = ' + practice.Id);

        practice = new pse__Practice__c(Name='SMM');
        insert practice;
        practice = [Select Id from pse__Practice__c where Name = 'SMM'];
        System.debug('Practice Id = ' + practice.Id);
        
        pse__Region__c region = new pse__Region__c(Name='Corporate');
        insert region;
        region = [Select Id from pse__Region__c where Name = 'Corporate'];
        System.debug('Region Id = ' + region.Id);
        
        Opportunity o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opportunity1';
        o.pse__Practice__c = practice.Id;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.CloseDate = prevQuarter.pse__Start_Date__c + 1;
        o.Start_Date__c = prevQuarter.pse__Start_Date__c + 2;
        o.pse__Region__c = region.Id;
        o.StageName = 'Won';
        o.ForecastCategoryName = 'Closed';
        o.Amount = 200000.0;
        opps.add(o);

        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opportunity2';
        o.pse__Practice__c = practice.Id;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.CloseDate = prevQuarter.pse__Start_Date__c + 1;
        o.Start_Date__c = prevQuarter.pse__Start_Date__c + 2;
        o.pse__Region__c = region.Id;
        o.StageName = 'Won';
        o.ForecastCategoryName = 'Closed';
        o.Amount = 200000.0;
        opps.add(o);

        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opportunity3';
        o.pse__Practice__c = practice.Id;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.CloseDate = prevQuarter.pse__Start_Date__c + 1;
        o.Start_Date__c = prevQuarter.pse__Start_Date__c + 2;
        o.pse__Region__c = region.Id;
        o.StageName = 'Qualify';
        o.ForecastCategoryName = 'Best Case';
        o.Amount = 200000.0;
        opps.add(o);

        insert opps;
        
        Opportunity opp = [SELECT Id from Opportunity where Name = 'Opportunity3'];
        opp.StageName = 'Analysis';
        update opp;
        
    }
    
    class KPIHitRatioBaseReportLogicTest extends KPIHitRatioBaseReportLogic {
    	List<OpportunityFieldHistory> histories;
    	
    	public KPIHitRatioBaseReportLogicTest() {
    		histories = new List<OpportunityFieldHistory>();	
    		OpportunityFieldHistory history = new OpportunityFieldHistory();
    		history.Field = 'StageName';
	        //history.OldValue = 'Qualify';
	        //history.NewValue = 'Analysis';
	        histories.add(history); 
    	}
    	
		public void buildHistoryMapTest() {
			Map<String, List<String>> histMap = buildHistoryMap(histories);
    	}
    	
	    public KPI_Value__c execute(KPI_Value__c kpiValue) {
	    	return kpiValue;
	    }
    	
    }
}