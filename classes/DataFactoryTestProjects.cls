public with sharing class DataFactoryTestProjects {

	private static list<pse__Proj__c> projects;
	
	static {
		projects = new list<pse__Proj__c>();
	}
	
	public static void createProjects() {
    	pse__Proj__c p;
    	list<Account> accounts = DataFactoryTestAccounts.getAccounts();
		
    	for (Account account : accounts) {
	    	p = new pse__Proj__c();
	        p.Name = '1 Month T+M on time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Billing_Type__c = DataFactoryTestBasicValues.projectBillingTypeTM;
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFirstDay;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateOriginLastDay;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
	    	p = new pse__Proj__c();
	        p.Name = '1 Month T+M off time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFifteenth;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateOneMonthAfterOrigin;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
	    	p = new pse__Proj__c();
	        p.Name = '2 Month T+M off time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Billing_Type__c = DataFactoryTestBasicValues.projectBillingTypeTM;
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFifteenth;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateTwoMonthsAfterOrigin;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
	    	p = new pse__Proj__c();
	        p.Name = '3 Month T+M off time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Billing_Type__c = DataFactoryTestBasicValues.projectBillingTypeTM;
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFifteenth;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateThreeMonthsAfterOrigin;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
	    	p = new pse__Proj__c();
	        p.Name = '6 Month T+M off time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Billing_Type__c = DataFactoryTestBasicValues.projectBillingTypeTM;
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFifteenth;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateSixMonthsAfterOrigin;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
	    	p = new pse__Proj__c();
	        p.Name = '12 Month T+M off time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Billing_Type__c = DataFactoryTestBasicValues.projectBillingTypeTM;
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFifteenth;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateTwelveMonthsAfterOrigin;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
	    	p = new pse__Proj__c();
	        p.Name = '6 Month FF off time period boundaries for ' + account.Name;
	        p.pse__Account__c = account.Id;
	        p.pse__Is_Active__c = true; 
	        p.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	        p.pse__Billing_Type__c = DataFactoryTestBasicValues.projectBillingTypeFixed;
	        p.pse__Start_Date__c = DataFactoryTestBasicValues.dateOriginFifteenth;
	        p.pse__End_Date__c = DataFactoryTestBasicValues.dateSixMonthsAfterOrigin;
	        p.Date_Became_Inactive__c = null;
	        p.pse__Project_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.Delivery_Engagement_Manager__c = DataFactoryTestBasicValues.johnSmail.Id;
	        p.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
	        projects.add(p);
	        
    	}

		for (pse__Proj__c x : projects) {
			system.debug('Before project insert values: id='+ x.id +'; name='+ x.name +'; accountId='+ x.pse__Account__c +'; active='+ x.pse__Is_Active__c +'; billable='+ x.pse__Is_Billable__c +'; billingType='+ x.pse__Billing_Type__c +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; inactiveDate='+ x.Date_Became_Inactive__c +'; pmId='+ x.pse__Project_Manager__c +'; demId='+ x.Delivery_Engagement_Manager__c +';');
		}

		insert projects;
		
		for (pse__Proj__c x : projects) {
			system.debug('After project insert values: id='+ x.id +'; name='+ x.name +'; accountId='+ x.pse__Account__c +'; active='+ x.pse__Is_Active__c +'; billable='+ x.pse__Is_Billable__c +'; billingType='+ x.pse__Billing_Type__c +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; inactiveDate='+ x.Date_Became_Inactive__c +'; pmId='+ x.pse__Project_Manager__c +'; demId='+ x.Delivery_Engagement_Manager__c +';');
		}
    }
	
    public static list<pse__Proj__c> getProjects() {
    	if (projects.size() == 0) {
    		projects = [select id, name, pse__Account__c, pse__Is_Active__c, pse__Is_Billable__c, pse__Billing_Type__c, pse__Start_Date__c, pse__End_Date__c, Date_Became_Inactive__c, pse__Project_Manager__c, Delivery_Engagement_Manager__c, pse__Region__c from pse__Proj__c where pse__Account__c in :DataFactoryTestAccounts.getAccountIds()];
    	}
    	
    	return projects;
    }

    public static set<Id> getProjectIds() {
    	set<Id> returnValue = new set<Id>();
    	
    	for (pse__Proj__c p : getProjects()) {
    		returnValue.add(p.Id);
    	}
    	
    	return returnValue;
    }
}