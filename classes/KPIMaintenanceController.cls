public class KPIMaintenanceController {
    public List<EditableKPIValue> KPIValues { get; set; }
    public String selectedQuarter { get; set; }
    private pse__Time_Period__c selectedPeriod { get; set; }
    private KPIBso bso;
   
    public KPIMaintenanceController() {
        selectedQuarter = ApexPages.currentPage().getParameters().get('selectedQuarter');
        System.debug('Selected Quarter: [' + selectedQuarter + ']');
        if (selectedQuarter == null) {
            selectedQuarter = getDefaultTimePeriod().Name;
        }
        
        selectedPeriod = [SELECT Id, Name FROM pse__Time_Period__c WHERE Name = :selectedQuarter];
        System.debug('Selected Period: ' + selectedPeriod.Name);
        
        bso = new KPIBso();
        //loadKPIValues();
    }
    
    public pse__Time_Period__c getDefaultTimePeriod() {
        pse__Time_Period__c defaultTimePeriod = [SELECT Id, Name from 
                pse__Time_Period__c where pse__Type__c = 'Quarter' and 
                pse__Start_Date__c <= today order by pse__Start_Date__c DESC LIMIT 1];
        System.debug('Default Time Period: ' + defaultTimePeriod.Name);
        return defaultTimePeriod;
    }

    
    public void loadKPIValues() {
        System.debug('loadKPIValues()');
        
        List<KPI__c> kpis = [SELECT Id, Name FROM KPI__c where Is_Active__c = true];
        
        List<KPI_Value__c> missingValues = bso.getMissingKPIValuesInTimePeriod(kpis, selectedPeriod);
        for (KPI_Value__c missingValue : missingValues) {
            System.debug('missing KPI: ' +  missingValue.Title__c);
        }
        bso.saveKPIValues(missingValues);        
        List<KPI_Value__c> rawValues = bso.getKPIValues(kpis, selectedPeriod);
        
        KPIValues = new List<EditableKPIValue>();
        for (KPI_Value__c rawValue : rawValues) {
            System.debug('KPI: ' +  rawValue.kpi__r.name);
            EditableKPIValue value = new EditableKPIValue(rawValue);
            KPIValues.add(value);
        }
    }
    
    public void save() {
        System.debug('save()');
        for (EditableKPIValue value : KPIValues) {
            value.save();
        }
        loadKPIValues();
    }
    
    public class EditableKPIValue {
        public String Title { get; set; }
        public Decimal InitialTarget { get; set; }
        public Decimal NewTarget { get; set; }
        public Boolean Checked { get; set; }
        public KPI_Value__c Original { get; set; }
        public String Format { get; set; }
        
        public EditableKPIValue(KPI_Value__c kpiValue) {
            System.debug('EditableKPIValue ctor()');
            Original = kpiValue;
            Title = kpiValue.kpi__r.name;
            InitialTarget = kpiValue.Target__c;
            NewTarget = 0.0;
            Format = kpiValue.kpi__r.format__c;
            Checked = false;
        }
        
        public void save() {
            System.debug('About to save: ' + Title + ' ' + Checked + ' ' + NewTarget);
            if (Checked == false) {
                return;
            }
            
            System.debug('Save: ' + Title + ' ' + Checked + ' ' + NewTarget);
            Original.Target__c = NewTarget;
            update Original;
        }
    }
}