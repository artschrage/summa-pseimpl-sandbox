public with sharing class KPIBso { 

    private KPIDao kpiDao;
    private CommonDao commonDao;
    
    public KPIBso() {
        commonDao = new CommonDao();
        kpiDao = new KPIDao();
    }
    
    public pse__Time_Period__c getPreviousQuarter() {
        return commonDao.fetchPreviousQuarterTimePeriod();
    }
    
    public pse__Time_Period__c getCurrentQuarter() {
        return commonDao.fetchCurrentQuarterTimePeriod();
    }
    
    public pse__Time_Period__c getCurrentYear() {
        return commonDao.fetchCurrentYearTimePeriod();
    }
    
    public List<KPI__c> getActiveKPIs(String timePeriodType) {
        return kpiDao.getActiveKPIs(timePeriodType);
    }

    public List<KPI_Value__c> getMissingKPIValuesInTimePeriod(
                        List<KPI__c> kpis, pse__Time_Period__c timePeriod) {
        // Due to unique kpi_value__c.alternate_key__c, we can use the list sizes 
        // to determine if a value is missing
        List<KPI_Value__c> values = kpiDao.getKPIValues(kpis, timePeriod);
        if (values == null || (kpis.size() != values.size())) {
            values = identifyAndCreateMissingValues(kpis, values, timePeriod);
        } else {
            values.clear();
        }
        
        return values;
    }
    
    public List<KPI_Value__c> getKPIValues(List<KPI__c> kpis, pse__Time_Period__c timePeriod) {
        // Since KPIs can be made inactive it is not illegal for there to be
        // an empty list of missing values
        if (kpis == null || kpis.isEmpty()) {
            System.debug('No KPIs were provided.');
            return new List<KPI_Value__c>();
        }
        System.debug('KPI count:' + kpis.size());

        // Without a time period, we cannot do any processing
        if (timePeriod == null) {
            throw new KPIDataDependencyException('No Time Period was provided. A pse__Time_Period__c must be provided by the "timePeriod" argument.');
        }
        
        List<KPI_Value__c> values = kpiDao.getKPIValues(kpis, timePeriod);
        System.debug('Value count:' + values.size());

        //due to unique kpi_value__c.alternate_key__c, we can use the list sizes to determine if a value is missing
        if (values == null || (kpis.size() != values.size())) {
            system.debug(LoggingLevel.WARN, 'KPIBso getKPIValues did not retrieve as many KPI_Value__c records as expected for the number of KPI__c records provided.');
        }
        
        return values;
    }
    
    private List<KPI_Value__c> identifyAndCreateMissingValues(List<KPI__c> kpis, List<KPI_Value__c> values, pse__Time_Period__c timePeriod) {
        system.debug('KPIBso identifyAndCreateMissingValues initialize kpi:kpiValue map...');
        Map<id, KPI_Value__c> kpiValueMap = new Map<id, KPI_Value__c>();
        for (KPI__c kpi : kpis) {
            kpiValueMap.put(kpi.id, null);
        }
        
        system.debug('KPIBso identifyAndCreateMissingValues place KPI_Value__c to map...');
        for (KPI_Value__c value : values) {
            kpiValueMap.put(value.kpi__c, value);
        }
        
        system.debug('KPIBso identifyAndCreateMissingValues find nulls in map where KPI_Value__c not returned for KPI__c in pse__Time_Period__c ['+ timePeriod +']...');
        List<KPI_Value__c> newValues = new List<KPI_Value__c>();
        
        for (ID kpiId : kpiValueMap.keySet()) {
            if (kpiValueMap.get(kpiId) == null) {
                newValues.add(new KPI_Value__c(kpi__c=kpiId, time_period__c=timePeriod.id, target__c=0, actual__c=0));
            }
        }
        
        system.debug('KPIBso identifyAndCreateMissingValues instantiated ['+ newValues.size() +'] new KPI_Value__c records to be saved.');
        return newValues; 
    }
    
    public List<KPI_Value__c> executeReports(List<KPI_Value__c> values) {
        List<KPI_Value__c> returnValues = new List<KPI_Value__c>();

        for (KPI_Value__c value : values) {
            System.debug('KPI Value dates: ' +  value.Time_Period__r.pse__Start_Date__c + 
                            ' ' + value.Time_Period__r.pse__End_Date__c);
            if (value.kpi__r.report_class_name__c != null) {
                try {
                    Type t = Type.forName(value.kpi__r.Report_Class_Name__c);
                    KPIReportInterface report = (KPIReportInterface)t.newInstance();

                    system.debug('KPIBso executeReports running ['+ value.kpi__r.Report_Class_Name__c +']...');
                    returnValues.add(report.execute(value));
                } catch (Exception e) {
                    ExceptionUtils.logException(e);
                }
            }
        }
            
        return returnValues;
    } 
    
    public void saveKPIValues(List<KPI_Value__c> values) {
        if (values != null && values.size() > 0) {
            kpiDao.saveValues(values);
        }
    }
}