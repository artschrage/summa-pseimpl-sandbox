/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestKPIBatcher {

    static testMethod void testBatch() {
    	TestKPIBase.stageTestData();
		KPIBatcher batch = new KPIBatcher();
		
		Test.startTest();
		ID batchprocessid = Database.executeBatch(batch);
		Test.stopTest();
		
		KPI__c kpi = [select id from kpi__c where Name='Complimentary Services' limit 1];
		pse__Time_Period__c period = [select id from pse__Time_Period__c where name='Last Quarter' limit 1];
		KPI_Value__c value = [select id, name, actual__c from kpi_value__c where kpi__c = :kpi.Id and time_period__c = :period.Id limit 1];
		
		system.assertNotEquals(null, value, 'Value returned cannot be null');
		system.assertEquals(0, value.actual__c, 'Expected $0 complimentary service return value');
    }

}