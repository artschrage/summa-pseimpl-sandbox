@isTest (SeeAllData = true)

     public class TestTimecardReminder
      {
      static testmethod void Emailtest(){
      
      String CRON_EXP = '0 0 0 3 9 ? 2022';
      Test.startTest();
      User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    System.runAs ( thisUser ) {
      EmailTemplate testtemplate = new EmailTemplate(
      Name = 'testTemplate1',
      Subject = 'test template',
      Body = 'this is a test',
      IsActive = true,
      TemplateType = 'html',
      BrandTemplateId = '016300000000R4rAAE',
      TemplateStyle = 'formalLetter',
      FolderId = '00D300000000ZIgEAM',
      DeveloperName = 'testTemplate1');
      insert testtemplate;
      System.assertNotEquals(null, testtemplate.Id);
      
      Messaging.MassEmailMessage testmail = new Messaging.MassEmailMessage();
      testmail.setTemplateId(testtemplate.Id);
      
      String<Id> template = testtemplate.Id;
      
      Account acct = [Select Id, Name from Account where Name = 'Summa Technologies, Inc.'];
      pse__Work_Calendar__c wc = [Select Id from pse__Work_Calendar__c where Name = 'US'];
      
      Contact con1 = new Contact(
      RecordTypeId = '01240000000M3xOAAS',
      LastName = 'TestCon1',
      FirstName = 'Joe',
      Email = 'keith@summa-tech.com',
      AccountId = acct.Id,
      pse__Work_Calendar__c = wc.Id,
      pse__Is_Resource__c = true,
      pse__Is_Resource_Active__c = true,
      pse__Exclude_From_Missing_Timecards__c = false);
      
      insert con1;
      System.assertNotEquals(null, con1.Id);
      List<Contact> lstcon = new List<Contact>([Select Id, LastName, Email from Contact where LastName = 'TestCon1']);
      System.assertEquals('TestCon1', lstcon[0].LastName);
      
      List<Id> recipients = new List<Id>();
      recipients.add(con1.Id);
      
     //pse__Proj__c testproject = new pse__Proj__c(
      //Name = 'Sandbox test project',
      //pse__Region__c = 'a0nf00000004C9SAAU',
      //pse__Account__c = acct.Id,
      //pse__Is_Active__c = true,
      //pse__Is_Billable__c = true,
      //pse__Allow_Self_Staffing__c = true,
      //pse__Allow_Timecards_Without_Assignment__c = true);
      
      //insert testproject;
      //System.assertNotEquals(null, testproject.Id);
      
      //List<pse__Proj__c> testprojlist = new List<pse__Proj__c>([Select Id, Name from pse__Proj__c where Name like 'Sandbox%']);
      //System.assertEquals('Sandbox test project',testprojlist[0].Name);
      
      //pse__Timecard_Header__c testTC = new pse__Timecard_Header__c(
      //pse__Resource__c = con1.Id,
      //pse__Project__c = testproject.Id,
      //pse__Start_Date__c = date.today() -3,
      //pse__End_Date__c = date.today() +3,
      //pse__Billable__c = true,
      //pse__Monday_Hours__c = 8.0,
      //pse__Tuesday_Hours__c = 8.0,
      //pse__Submitted__c = true,
      //pse__Status__c = 'Approved',
      //pse__Approved__c = true);
      
      //insert testTC;
      //System.assertEquals(16.0, testTC.pse__Total_Hours__c);
      
      //AggregateResult[] ar = [SELECT pse__Resource__c res, SUM(pse__Total_Hours__c)hours from pse__Timecard_Header__c where pse__End_Date__c = THIS_WEEK and pse__Submitted__c = true and pse__External_Resource__c = false and pse__Resource__r.FirstName like 'Joe%' group by pse__Resource__c having SUM(pse__Total_Hours__c)<40];
      //System.assertNotEquals(null, ar.size());
      //if(!ar.IsEmpty()){
      //String<Id> add2list;
      //for(Integer i = 0; i < ar.size(); i++){
      //add2list = (String) ar[i].get('res');
      //recipients.add(add2list);
      //}
      //}
      
      String jobId = System.schedule('testScheduledApex', CRON_EXP, new TimecardReminder());
      CronTrigger ct = [Select Id, CronExpression, TimesTriggered, NextFireTime From CronTrigger Where Id = :jobId];
      System.assertEquals(CRON_EXP, ct.CronExpression);
      System.assertEquals(0, ct.TimesTriggered);
      System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
      
      //TimecardReminder tr = new TimecardReminder(template, recipients);
      
      TimecardReminder trt = new TimecardReminder();
     } 
       Test.stopTest();
    }
    }