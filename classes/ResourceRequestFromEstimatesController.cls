/* This class handles both the resource import and Oauth process.
 * Described below is the sequence of events for the OAuth dance and 
 * resource request import:
 * When import is clicked, ask google for an authorization token using a google issued client id and client secret
 * This will take you to a page where you will be requested to sign in with a google account
 * If the credentials are validated, then google will return an authorization token in the code parameter
 * The application then sends the authorization token to google with a request for an access token
 * Google responds with an access token valid for a certain amount of time
 * Our application then stores this access token in the custom settings
 * This access token will be checked to see if it has expired and if not used to make subsequent requests to google
 * A call is made to the spreadsheet with the access token.
 * If the google account provided has access to the spreadsheet, the records will be pulled from the spreadsheet
 */
 

public with sharing class ResourceRequestFromEstimatesController {
    
    //Oauth Settings - Stored in custom settings
    
    public static final Google_Data_Api_Settings__c oAuthSettings = Google_Data_Api_Settings__c.getValues('Spreadsheet_API_Details');

    public static final string CLIENT_SECRET = oAuthSettings.Client_Secret__c;
    public static final string CLIENT_ID = oAuthSettings.Client_ID__c;
        
    public static final string OAUTH_TOKEN_URL = oAuthSettings.OAuth_Token_Url__c;
    public static final string OAUTH_CODE_END_POINT_URL = oAuthSettings.OAuth_Code_Endpoint_Url__c;
    
    public static final string GRANT_TYPE = oAuthSettings.Grant_Type__c;
    
    //Scope URL as per oauth 2.0 guide of the google 
    public static final string SCOPE = oAuthSettings.Scope__c;
    
    //Approval Prompt Constant
    public static final string APPROVAL_PROMPT = oAuthSettings.Approval_Prompt__c;
    
    public static final Integer XORFactor = 31578;
    
   	public static final Resource_Template_Layout__c resourceTemplateLayout = Resource_Template_Layout__c.getOrgDefaults();
   	
   	//Resource Template Settings - Stored in custom settings
   	public static final Integer ROW_START_INDEX =  Integer.valueOf(resourceTemplateLayout.Row_Start_Index__c);   	
   	public static final Integer ADJUSTED_HOURS_COLUMN = Integer.valueOf(resourceTemplateLayout.Adjusted_Hours_Column__c);   	
   	public static final Integer BILLING_RATE_COLUMN= Integer.valueOf(resourceTemplateLayout.Billing_Rate_Column__c);
   	public static final Integer DURATION_WEEKS_COLUMN= Integer.valueOf(resourceTemplateLayout.Duration_Weeks_Column__c);    	
   	public static final Integer RATE_CODE_COLUMN= Integer.valueOf(resourceTemplateLayout.Rate_Code_Column__c);
   	public static final Integer RESOURCE_NAME_COLUMN= Integer.valueOf(resourceTemplateLayout.Resource_Name_Column__c);
   	
   	private string spreadsheetLink; 

    public ResourceRequestFromEstimatesController(ApexPages.StandardSetController controller) {
		
    }       
    

	//Get the google spreadsheet doc link to access data from
    public string GoogleDocLink {
    	get{
    		/*The link is also temporarily stored in a custom setting because 
    		 *several redirects occur on this page and the link value in the input text field may get
    		 *reset several times. Saving it in custom settings helps persist it throughout the
    		 *Oauth process
    		 */
    		String Id = Apexpages.CurrentPage().getparameters().get('state');
    		if(Id!=null){ 
    			Opp_Resource_Doc_Link__c oppResourceDocLink = Opp_Resource_Doc_Link__c.getValues(UserInfo.getUserId() + Id);
    			if(oppResourceDocLink!=null){
    				return oppResourceDocLink.Google_Doc_Link__c;
    			}
    		}
    		
    		return spreadsheetLink;   		    		 
    	}
    	set {
    		spreadsheetLink = value;
    	}
    }   
    
    //Used to determine whether a request for access token should be made and if import button should be disabled
    public boolean code{
    	get{    	
    		RequestToken__c requestToken= RequestToken__c.getValues(UserInfo.getUserId());
        		 	
    		return (ApexPages.currentPage().getParameters().get('code') != null && (requestToken!=null && !requestToken.ignoreToken__c));
    	}
    }
   
    
    public PageReference importResourceRequests(){   
    	
    	/* This custom setting is required to handle some challenges with viewstate, redirection
    	 * and authetication failure. It tells us when to ignore a google issued token
    	 * present in the url
    	 */
		RequestToken__c requestToken= RequestToken__c.getValues(UserInfo.getUserId());
		
		PageReference redirectPage;  
	    	
    	//Get opportunity Id from the url
        String Id = ApexPages.currentPage().getParameters().get('id');
        if(Id==null){
        	/* Opportunity Id is usually present in the url, but in case of a failed
        	 * authentication and a subsequent redirect, we rely on getting this value
        	 * from the state query parameter returned by google
        	 */
        	Id = ApexPages.currentPage().getParameters().get('state');
        }
        String retUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/' + Id;    	  	
    	
		oAuth_Token_Management__c oAuthManagementSettings = oAuth_Token_Management__c.getValues(UserInfo.getUserId());
		
		//Check to see if access token has already been issued and if it is still valid
		if(oAuthManagementSettings==null || DateTime.valueOf(oAuthManagementSettings.expiryDateTime__c).addSeconds(60)<=DateTime.Now())	{
			if(requestToken==null){
				upsert new RequestToken__c(
					Name= UserInfo.getUserId(),
					ignoreToken__c = false
					);					
			
			} else{						
				requestToken.ignoreToken__c = false;
				update requestToken;
			}
	    	//The currentURL should match the redirect URL set up in google developer console for this project
	    	String currentURL = EncodingUtil.urlEncode(formatCurrentURL(), 'UTF-8'); 	        	       	
	        	  	
	     	//Request google for an authorization token            
	        String redirectUrl = OAUTH_CODE_END_POINT_URL+'?scope='+EncodingUtil.urlEncode(SCOPE,'UTF-8')+'&state='+ Id + '&redirect_uri='+currentURL+'&response_type=code&client_id='+CLIENT_ID+'&approval_prompt='+APPROVAL_PROMPT;
	            
	        redirectPage=new PageReference(redirectUrl);
	        
	        Opp_Resource_Doc_Link__c oppResourceDocLink = Opp_Resource_Doc_Link__c.getValues(UserInfo.getUserId() + Id);
	    	/*Check to see if a document link is already associated to this opportunity
	    	 *If it does not already exist, create a custom setting to hold this info,
	    	 *if not, update the custom setting with the new document link
	    	 */
	    	if(oppResourceDocLink==null){
	    		upsert new Opp_Resource_Doc_Link__c(
			    Name = UserInfo.getUserId() + Id,
			    Google_Doc_Link__c = GoogleDocLink
			);
	    	} else {	        		
	    		oppResourceDocLink.Google_Doc_Link__c = GoogleDocLink;
	    		update oppResourceDocLink;
	    	}    
		} else {
			//If acess token is already available move on to importing the resource requests
			redirectPage=new PageReference(retUrl);
			try{
				createResourceRecordFromSpreadSheet(Id, oAuthManagementSettings.Token__c, GoogleDocLink);
			} catch(Exception error){		
				redirectPage = null;
				ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,error.getMessage());
    			ApexPages.addMessage(errorMessage);
			} finally{
				Opp_Resource_Doc_Link__c oppResourceDocLink = Opp_Resource_Doc_Link__c.getValues(UserInfo.getUserId() + Id);
				if(oppResourceDocLink!=null){
					delete oppResourceDocLink;
				}				
			}				
		}	
    	return redirectPage;
    }  
    
       
    /* Exchanges authorization token for access token. This method is invoked
     * only if access token has not already been issued and valid and when an 
     * authorization token is available
     */   
    public PageReference exchangeRequestToken() {
    	
    	//Code param contains the authorization token
        String codeparam=apexpages.currentpage().getparameters().get('code');
        System.debug('*** codeparam: ' + codeparam);

        //State query param contains the opportunity Id
        String Id = apexpages.currentpage().getparameters().get('state');
        System.debug('*** Id: ' + Id);

      	if(codeparam!=null){
      		// Do anything only if authorization token is available
      		
			//Oauth Dance:	
	        // Instantiate a new http object
	        Http h = new Http();
	        
	        String acessTokenRequestBody='code='+EncodingUtil.urlEncode(codeparam,'UTF-8')+'&client_id='+CLIENT_ID+'&client_secret='+CLIENT_SECRET+'&redirect_uri='+EncodingUtil.urlEncode(formatCurrentURL(), 'UTF-8')+'&grant_type='+GRANT_TYPE;
	        
            System.debug('*** acessTokenRequestBody:' + acessTokenRequestBody);

	    	// Instantiate a new HTTP request, specify the method (POST) as well as the endpoint
	        HttpRequest acessTokenRequest = new HttpRequest();
	        acessTokenRequest.setEndpoint(OAUTH_TOKEN_URL);
	        acessTokenRequest.setHeader('Content-Type','application/x-www-form-urlencoded');
	        acessTokenRequest.setMethod('POST');
	        acessTokenRequest.setBody(acessTokenRequestBody);
	     
	    	// Send the request, and return a response
	        HttpResponse accessTokenResponse = h.send(acessTokenRequest);

            System.debug('*** accessTokenResponse: ' + accessTokenResponse);

	        String acessTokenResponseParams = accessTokenResponse.getBody();	        

            System.debug('*** acessTokenResponseParams: ' + acessTokenResponseParams);
	        
	        //Read the params to retrieve the access token and valididy period
	        Map<String, Object> accessTokenParams = (Map<String, Object>)JSON.deserializeUntyped(acessTokenResponseParams);	
	        String accessToken = '';
	        Integer expiryDuration = 0;
	        if(accessTokenParams.get('access_token')!=null && accessTokenParams.get('expires_in')!=null){
	        	accessToken = EncodingUtil.urlEncode((String)accessTokenParams.get('access_token'),'UTF-8');
	        	expiryDuration =   Integer.valueOf(accessTokenParams.get('expires_in'));
	        } else {
	        	String errorMessage =  'Something went wrong when authenticating this page.Please try again';	    		
	    		throw new ResourceRequestImportException(errorMessage);
	        }
	        
	        Opp_Resource_Doc_Link__c oppResourceDocLink = Opp_Resource_Doc_Link__c.getValues(UserInfo.getUserId() + Id);    
        	String googleDocURL = oppResourceDocLink.Google_Doc_Link__c;   
	         
	        try{ 
	        	//Call the method to access spreadsheet and import the records
	       		createResourceRecordFromSpreadSheet(Id, accessToken, googleDocURL); 
	        } catch(Exception e){
	        	//Ignore existing authorization token if authentication failure occured
				RequestToken__c requestToken = RequestToken__c.getValues(UserInfo.getUserId());
				requestToken.ignoreToken__c = true;
				update requestToken;
				ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
    			ApexPages.addMessage(errorMessage);     
	        	return null;
	        } finally{				
				if(oppResourceDocLink!=null){
					//No need to hold on to google doc link once resources have been imported
					delete oppResourceDocLink;
				}
			}
	        
	        //Store valid authentication details in custom settings to reuse	                
	        oAuth_Token_Management__c oAuthManagementSettings = oAuth_Token_Management__c.getValues(UserInfo.getUserId());
	        if(oAuthManagementSettings!=null){
	        	oAuthManagementSettings.Token__c = accessToken;
	        	oAuthManagementSettings.isAccessToken__c = true;
	        	oAuthManagementSettings.expiryDateTime__c = DateTime.now().addSeconds(expiryDuration);
	        	update oAuthManagementSettings;        	
	        } else {	        	
	        	upsert new oAuth_Token_Management__c(
					    Name = UserInfo.getUserId(),
					    Token__c = accessToken,
					    expiryDateTime__c = DateTime.now().addSeconds(expiryDuration)
					    );
	        }          
        
      	}

        System.debug('*** pageRef1: ' + 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/' + Id);       

        return new PageReference('https://' + ApexPages.currentPage().getHeaders().get('Host') + '/' + Id);
    }  
    
    //This method takes care of importing the resource request records
    private void createResourceRecordFromSpreadSheet(String opportunityId, string accessToken, String googleDocURL){
    	
    	PageReference googleLink = new PageReference(googleDocURL);
    	
    	//Get the spreadsheet key and worksheet id from url
        String googleDocKey = googleLink.getParameters().get('key');
        String gidValue = googleDocURL.split('#')[1].split('=')[1];  
        Integer gid=Integer.valueOf(gidValue);
        
        String errorMessage = '';
        
        if(googleDocKey==null || gidValue == null){
        	errorMessage = 'Enter a valid google spreadsheet link';
        	throw new ResourceRequestImportException(errorMessage);
        }
        
        //Convert GID to worksheetId required by spreadsheet API               
       
        String worksheetID = convertToWorksheetId(gid);
        
        //Call spreadsheet API
        Http h = new Http();
        HttpRequest spreadSheetContentRequest = new HttpRequest();
        spreadSheetContentRequest.setEndpoint('https://spreadsheets.google.com/feeds/cells/' + googleDocKey+ '/' + worksheetID + '/private/full');
        spreadSheetContentRequest.setHeader('Gdata-version','3.0');        
        spreadSheetContentRequest.setHeader('Authorization','Bearer '+ accessToken);
        spreadSheetContentRequest.setMethod('GET');
        HttpResponse spreadSheetContentResponse = h.send(spreadSheetContentRequest);
        
        System.debug('*** spreadSheetContentResponse.getStatusCode(): ' + spreadSheetContentResponse.getStatusCode());

        if(spreadSheetContentResponse.getStatusCode()>299){
		   	if(spreadSheetContentResponse.getStatusCode() == 403){      
		   		//No access to spreadsheet or not shared
        		RequestToken__c requestToken = RequestToken__c.getValues(UserInfo.getUserId());
        		requestToken.ignoreToken__c =  true;        			
        		oAuth_Token_Management__c oAuthManagementSettings = oAuth_Token_Management__c.getValues(UserInfo.getUserId());
        		if(oAuthManagementSettings!=null){
        			delete oAuthManagementSettings;
        		}        		
        		errorMessage =  'You do not have access to this spreadsheet';
        		throw new ResourceRequestImportException(errorMessage); 
        	} else {
        		//Spreadsheet or worksheet does not exist
        		RequestToken__c requestToken = RequestToken__c.getValues(UserInfo.getUserId());
        		requestToken.ignoreToken__c =  true; 
        		errorMessage = 'Please make sure you are pointing to the correct spreadsheet and worksheet';
        		throw new ResourceRequestImportException(errorMessage);  		
        	} 
        }        
        
        //System.debug('*** spreadsheet response: ' + spreadSheetContentResponse.getbody());

        GoogleData feed = new GoogleData(new XMLDom(spreadSheetContentResponse.getbody()));
        xmldom.element wrkSheet = new xmldom.element('entry'); 
        GoogleData.Worksheet ws = new GoogleData.Worksheet(wrksheet);
        Map<String, GoogleData.Cell> cellMap = ws.cellFactoryAsMap( feed.entries ); 
        
        List<Opportunity> opportunity = [SELECT Start_Date__c, pse__Practice__c, pse__Region__c
                                FROM Opportunity 
                                WHERE Id =: opportunityId LIMIT 1];
        
        Integer startIndex = ROW_START_INDEX + 1;
        String cellNumber = 'R' + String.valueOf(startIndex) + 'C';
        Integer durationWeeks;
        Integer longestDuration = 0;
        
        try{
        
	        while(cellMap.get(cellNumber+String.valueOf(RESOURCE_NAME_COLUMN))!=null && cellMap.get(cellNumber+String.valueOf(RATE_CODE_COLUMN))!=null){
	        	System.debug('Resource Name Column' + cellMap.get(cellNumber+String.valueOf(RESOURCE_NAME_COLUMN)).content);
	        	durationWeeks = Integer.valueOf(cellMap.get(cellNumber+String.valueOf(DURATION_WEEKS_COLUMN)).content);
	        	longestDuration = Math.max(longestDuration, durationWeeks);
	        	cellNumber = 'R' + String.valueOf(++startIndex) + 'C';
	        }
	        
	        startIndex = ROW_START_INDEX + 1;
	        cellNumber = cellNumber = 'R' + String.valueOf(startIndex) + 'C';  
	             
	        Date endDate = opportunity[0].Start_Date__c.addDays(longestDuration*7);
	        Integer numberOfResourceRequests = 0;
	        Rate_Code_Resource_Role_Mapping__c roleResourceMapping;
	        
	        while(cellMap.get(cellNumber+String.valueOf(RESOURCE_NAME_COLUMN))!=null && cellMap.get(cellNumber+String.valueOf(RATE_CODE_COLUMN))!=null){
	        	numberOfResourceRequests++;
	        	if(numberOfResourceRequests + Limits.getDmlStatements() > Limits.getLimitDmlStatements()){
	        		errorMessage =  'Too many resource requests to insert. Try with fewer resource requests';
	        		throw new ResourceRequestImportException(errorMessage);
	        	}        	
	    
	    		//If corresponding role is not found in salesforce, do not insert the resource request
	        	roleResourceMapping = Rate_Code_Resource_Role_Mapping__c.getValues(cellMap.get(cellNumber+String.valueOf(RATE_CODE_COLUMN)).content);
				if(roleResourceMapping == null){
					cellNumber = 'R' + String.valueOf(startIndex++) + 'C';
					continue;
				} else{
					pse__Resource_Request__c insertResource = new pse__Resource_Request__c();
		        	insertResource.pse__Resource_Request_Name__c = cellMap.get(cellNumber+String.valueOf(RESOURCE_NAME_COLUMN)).content;
					insertResource.pse__Opportunity__c = opportunityId;
					insertResource.pse__Start_Date__c = opportunity[0].Start_Date__c;						
					insertResource.pse__Resource_Role__c =roleResourceMapping.Resource_Role__c; 
					insertResource.pse__SOW_Hours__c = Integer.valueOf(cellMap.get(cellNumber+String.valueOf(ADJUSTED_HOURS_COLUMN)).content);
					insertResource.pse__Requested_Bill_Rate__c = Decimal.valueOf(cellMap.get(cellNumber+String.valueOf(BILLING_RATE_COLUMN)).content.removeStart('$').removeEnd('$'));
					insertResource.pse__End_Date__c = endDate;
					insertResource.pse__Region__c = opportunity[0].pse__Region__c;
					insertResource.pse__Practice__c = opportunity[0].pse__Practice__c;
		            cellNumber = 'R' + String.valueOf(++startIndex) + 'C';
					insert insertResource;
				}	        	
	        }
        } catch(Exception e){
        	errorMessage = 'Please make sure the spreadsheet is in the correct format';        	    
    		throw new ResourceRequestImportException(errorMessage); 
        }   
        
    }
    
    //Formats and returns the current url without the parameters
    private string formatCurrentURL(){
    	PageReference returnURL = ApexPages.currentPage();
    	returnURL.getParameters().clear();
       	return 'https://' + ApexPages.currentPage().getHeaders().get('Host') + returnURL.getUrl();
    }
    
    //Converts the gid parameter into worksheet id required by the spreadsheet Api
    private String convertToWorksheetId(Integer gid){
    	Integer XordGID = gid^XORFactor;
        //Convert to base36
        String base36 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        string[] charArray = base36.split('');
        charArray.remove(0);
        string sb ='';
         do {
        	sb+=charArray[Math.Mod(XordGID,36)];
        	XordGID /= 36;
    	} while (XordGID != 0);
    	return sb.reverse().toLowerCase();
    }
}