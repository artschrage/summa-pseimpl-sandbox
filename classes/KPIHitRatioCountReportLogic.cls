public class KPIHitRatioCountReportLogic extends KPIHitRatioBaseReportLogic {

    public KPI_Value__c execute(KPI_Value__c kpiValue) {
        Decimal actual = 0.0;
        
        Map<String, Opportunity> oppMap = getOpportunityMap(
                        kpiValue.Time_Period__r.pse__Start_Date__c,
                        kpiValue.Time_Period__r.pse__End_Date__c);
        List<OpportunityFieldHistory> histories = 
                [SELECT Id, OpportunityId, NewValue FROM OpportunityFieldHistory
                        WHERE Field = 'StageName' AND 
                        OpportunityId in :oppMap.keySet()];
        System.debug('History count = ' + histories.size());
        
        Map<String, List<String>> historyMap = buildHistoryMap(histories);
        
        Integer total = 0;
        Integer hit = 0;
        
        for (Opportunity opp : oppMap.values()) {
            String oppDebug = 'Opportunity: ' + opp.Name + ' Amount: ' + opp.Amount;
            
            // If the opportunity is won, don't bother processing
            // any history
            ++total;
            if (opp.StageName == 'Won') {
                oppDebug += ' Won';
                ++hit; 
            }
            else {
                Decimal highest = getHighestProbability(opp,
                                    historyMap.get(opp.Id));
                oppDebug += ' ' + highest;
                if (highest >= 50.00) {
                    ++hit;
                }
            }
            System.debug(LoggingLevel.FINE, oppDebug);
        }
        System.debug('Total count = ' + total);
        System.debug('Hit count = ' + hit);
        if (total > 0.0) {
            actual = (Decimal) hit / (Decimal) total;
        }
        System.debug('hit/total = ' + actual);
        
        kpiValue.Actual__c = actual;
        return kpiValue; 
    }
}