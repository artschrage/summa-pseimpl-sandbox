global class ExistingClientAnalysisBatcher implements Database.Batchable<Account>, Database.Stateful {

	private static ExistingClientAnalysisBso bso;
	private static Date analysisBeginningDate;
	
	static {
		bso = new ExistingClientAnalysisBso();
		analysisBeginningDate = bso.getAnalysisBeginDate(Date.today());
	}
	
	global List<Account> start(Database.BatchableContext BC) {

		return bso.getAccountsForAnalysis();
	}
	
	global void execute(Database.BatchableContext BC, List<Account> accounts) {
		Set<Id> accountIds = new Set<Id>();
		for (Account account : accounts) {
			accountIds.add(account.Id);
		}

		//Work must be broken down into 3 phases due to data and Id reference dependencies

		//First, check the Opportunity records and confirm that all are flagged correctly
		//identifying them as new client opportunities.
		List<Opportunity> opportunityFlags = bso.analyzeOpportunityChanges(accountIds, analysisBeginningDate);
		bso.storeOpportunityChanges(opportunityFlags);
		
		//Second, confirm that all New_Client_Value__c records exist as needed for the 
		//new client opportunities so that opportunities can be related back to them.
		List<New_Client_Value__c> newClientValues = bso.analyzeNewClientValueChanges(accountIds, analysisBeginningDate);
		bso.storeNewClientValueChanges(newClientValues);
		
		//Third, relate all Opportunity records back to the New_Client_Value__c record 
		//that covers its CloseDate. This includes, but is not limited to, the initial
		//Opportunity that is flagged as IsNewClient.
		List<Opportunity> opportunityRelationships = bso.analyzeOpportunityNewClientValueChanges(accountIds, analysisBeginningDate);
		bso.storeOpportunityChanges(opportunityRelationships);
	}
	
	global void finish(Database.BatchableContext BC){

	}
}