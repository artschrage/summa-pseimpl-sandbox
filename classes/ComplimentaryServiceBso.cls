public with sharing class ComplimentaryServiceBso {

    private static Date findAnalysisWindowMinimumBound() {
        Date returnValue = ComplimentaryServiceDao.getEarliestProjectStartDate();

        if (returnValue != null) {
            returnValue = ComplimentaryServiceUtils.toFirstOfMonth(returnValue);
        }
            
        return returnValue;
    }
    
    private static Date findAnalysisWindowMaximumBound() {
        Date returnValue = ComplimentaryServiceDao.getLatestProjectEndDate();
        
        if (returnValue != null) {
            returnValue = ComplimentaryServiceUtils.toLastOfMonth(returnValue);
        }
        
        return returnValue; 
    }
    
    //to be called by batchable start method
    public static List<pse__Proj__c> getProjectsForAnalysis() {
    	return ComplimentaryServiceDao.getAllProjectsForAnalysis();
    }
    
    public static List<pse__Time_Period__c> getTimePeriods() {
    	return ComplimentaryServiceDao.getAnalysisTimePeriods( 
            ComplimentaryServiceBso.findAnalysisWindowMinimumBound(),
            ComplimentaryServiceBso.findAnalysisWindowMaximumBound());
    }

	//to be called by batchable execute method
    public static void runAnalysis(List<pse__Time_Period__c> periods, List<pse__Proj__c> projects) {
    	Set<Id> projectIds = new Set<Id>();
    	for (pse__Proj__c project : projects) {
    		projectIds.add(project.Id);
    	}
    	
        List<Complimentary_Service__c> oldAnalyses = aggregateOldAnalyses(projectIds);
        List<Complimentary_Service__c> newAnalyses = runAggregations(projects, projectIds, periods);
        
        ComplimentaryServiceDao.storeAnalyses(oldAnalyses, newAnalyses);
    }

    private static List<Complimentary_Service__c> aggregateOldAnalyses(Set<Id> projectIds) {
        return ComplimentaryServiceDao.getProjectComplimentaryServices(projectIds);
    }
    
    public static List<Complimentary_Service__c> runAggregations(List<pse__Proj__c> projects, Set<Id> projectIds, List<pse__Time_Period__c> allPeriods) {
        List<Complimentary_Service__c> returnValue = new List<Complimentary_Service__c>();
        Map<pse__Time_Period__c,Complimentary_Service__c> cache = new Map<pse__Time_Period__c,Complimentary_Service__c>(); //this needs to be a map in order to work right later
        
        if (projects != null && projects.size() > 0) {
            List<pse__Time_Period__c> allMonths = ComplimentaryServiceUtils.getTimePeriodsByType(allPeriods, 'Month');
            List<pse__Time_Period__c> allQuarters = ComplimentaryServiceUtils.getTimePeriodsByType(allPeriods, 'Quarter');
            List<pse__Time_Period__c> allYears = ComplimentaryServiceUtils.getTimePeriodsByType(allPeriods, 'Year');
            
            Map<Id, List<pse__Expense__c>> expenses = ComplimentaryServiceDao.getProjectExpenses(projectIds);
            Map<Id, List<pse__Timecard__c>> timecards = ComplimentaryServiceDao.getProjectTimecards(projectIds);
    
            for (pse__Proj__c project : projects) {
                cache.clear();
                cache.putAll(analyzeProject(project, timecards.get(project.Id), expenses.get(project.Id), allMonths, allQuarters, allYears));
                        
                if (cache.size() > 0) {
                    returnValue.addAll(cache.values());
                }       
            }
            
        }
        
        return returnValue;
    }
    
    public static Map<pse__Time_Period__c,Complimentary_Service__c> analyzeProject(pse__Proj__c project, List<pse__Timecard__c> timecardSplits, List<pse__Expense__c> expenses, List<pse__Time_Period__c> allMonths, list<pse__Time_Period__c> allQuarters, List<pse__Time_Period__c> allYears) {
        Map<pse__Time_Period__c,Complimentary_Service__c> returnValue = new Map<pse__Time_Period__c,Complimentary_Service__c>();
        
        List<pse__Time_Period__c> projectMonths = ComplimentaryServiceUtils.getMatchingTimePeriods(allMonths, project.pse__Start_Date__c, project.pse__End_Date__c);
        List<pse__Time_Period__c> projectQuarters = ComplimentaryServiceUtils.getMatchingTimePeriods(allQuarters, project.pse__Start_Date__c, project.pse__End_Date__c);
        List<pse__Time_Period__c> projectYears = ComplimentaryServiceUtils.getMatchingTimePeriods(allYears, project.pse__Start_Date__c, project.pse__End_Date__c);
        
        Map<pse__Time_Period__c,Complimentary_Service__c> monthAnalyses = analyzeProjectMonths(project, timecardSplits, expenses, projectMonths);
        Map<pse__Time_Period__c,Complimentary_Service__c> quarterAnalyses = analyzeParentPeriods(project, monthAnalyses, projectQuarters);
        Map<pse__Time_Period__c,Complimentary_Service__c> yearAnalyses = analyzeParentPeriods(project, quarterAnalyses, projectYears);
        
        returnValue.putAll(monthAnalyses);
        returnValue.putAll(quarterAnalyses);
        returnValue.putAll(yearAnalyses);
        
        return returnValue;
    }
    
    public static Map<pse__Time_Period__c,Complimentary_Service__c> analyzeProjectMonths(pse__Proj__c project, List<pse__Timecard__c> timecardSplits, List<pse__Expense__c> expenses, List<pse__Time_Period__c> projectMonths) {
        Map<pse__Time_Period__c,Complimentary_Service__c> returnValue = new Map<pse__Time_Period__c,Complimentary_Service__c>();

        Map<pse__Time_Period__c,double> assignmentHoursByMonth = ComplimentaryServiceUtils.buildAccumulatorMap(projectMonths);      
        Map<pse__Time_Period__c,double> assignmentCostByMonth = ComplimentaryServiceUtils.buildAccumulatorMap(projectMonths);       
        Map<pse__Time_Period__c,double> expenseCostByMonth = ComplimentaryServiceUtils.buildAccumulatorMap(projectMonths);      
        
        pse__Time_Period__c timePeriod = null;

		if (timecardSplits != null) {
	        for (pse__Timecard__c timecard : timecardSplits) {
	            system.debug('Timecard [' + timecard.name + ']: start=' + timecard.pse__Start_Date__c + '; end=' + timecard.pse__End_Date__c + '; hours=' + timecard.pse__Total_Hours__c + '; cost=' + timecard.pse__Total_Cost__c + ';');
	            timePeriod = ComplimentaryServiceUtils.getTimePeriodFromDate(timecard.pse__start_date__c, projectMonths);
	
	            assignmentHoursByMonth.put(timePeriod, calculateAccumulatedAssignmentHours(assignmentHoursByMonth, timePeriod, timecard));
	            assignmentCostByMonth.put(timePeriod, calculateAccumulatedAssignmentCost(assignmentCostByMonth, timePeriod, timecard));
	        }
		}
		
		if (expenses != null) {            
	        for (pse__Expense__c expense : expenses) {
	            system.debug('Expense [' + expense.name + ']: date=' + expense.pse__Expense_Date__c + '; amount=' + expense.pse__Amount__c + '; billable=' + expense.pse__Billable__c + '; ');
	            timePeriod = ComplimentaryServiceUtils.getTimePeriodFromDate(expense.pse__Expense_Date__c, projectMonths);
	
	            expenseCostByMonth.put(timePeriod, calculateAccumulatedExpenseCost(expenseCostByMonth, timePeriod, expense));
	        }
		}
		        
        for (pse__Time_Period__c tp : projectMonths) {
            returnValue.put(tp, consolidateAnalysis(project, tp, assignmentHoursByMonth, assignmentCostByMonth, expenseCostByMonth));
        }

        return returnValue;
    }
    
    public static Complimentary_Service__c consolidateAnalysis(pse__Proj__c project, pse__Time_Period__c timePeriod, Map<pse__Time_Period__c,double> hours, Map<pse__Time_Period__c,double> timeCost, Map<pse__Time_Period__c,double> expenseCost) {
        Complimentary_Service__c returnValue = new Complimentary_Service__c();      
        returnValue.Project__c = project.Id;
        returnValue.Time_Period__c = timePeriod.Id;
        
        if (hours.containsKey(timePeriod)) {
            returnValue.Number_of_Hours__c = hours.get(timePeriod);
        } else {
            returnValue.Number_of_Hours__c = 0;
        }
        
        if (timeCost.containsKey(timePeriod)) {
            returnValue.Cost_of_Service__c = timeCost.get(timePeriod);
        } else {
            returnValue.Cost_of_Service__c = 0;
        }
        
        if (expenseCost.containsKey(timePeriod)) {
            returnValue.Cost_of_Expenses__c = expenseCost.get(timePeriod);
        } else {
            returnValue.Cost_of_Expenses__c = 0;
        }
        
        return returnValue;
    }
    
    public static Map<pse__Time_Period__c,Complimentary_Service__c> analyzeParentPeriods(pse__Proj__c project, Map<pse__Time_Period__c,Complimentary_Service__c> newGranularList, List<pse__Time_Period__c> coarserPeriods) {
        Map<pse__Time_Period__c,Complimentary_Service__c> returnValue = new Map<pse__Time_Period__c,Complimentary_Service__c>();

        Map<pse__Time_Period__c, Complimentary_Service__c> cache = new Map<pse__Time_Period__c, Complimentary_Service__c>();
        for (pse__Time_Period__c tp : coarserPeriods) {
            cache.put(tp, new Complimentary_Service__c(Project__c=project.Id, Time_Period__c=tp.Id, Number_of_Hours__c=0, Cost_of_Service__c=0, Cost_of_Expenses__c=0));
        }
        
        Complimentary_Service__c coarse;
        Complimentary_Service__c granular;
        for (pse__Time_Period__c key : newGranularList.keySet()) {
            coarse = cache.get(ComplimentaryServiceUtils.getContainingTimePeriod(key, coarserPeriods));
            granular = newGranularList.get(key);
            
            coarse.Number_of_Hours__c = coarse.Number_of_Hours__c + granular.Number_of_Hours__c;
            coarse.Cost_of_Service__c = coarse.Cost_of_Service__c + granular.Cost_of_Service__c; 
            coarse.Cost_of_Expenses__c = coarse.Cost_of_Expenses__c + granular.Cost_of_Expenses__c;
        }

        returnValue.putAll(cache);
        return returnValue;
    }

    public static double calculateAccumulatedAssignmentHours(Map<pse__Time_Period__c,double> accumulatorMap, pse__Time_Period__c timePeriod, pse__Timecard__c timecard) {
        double returnValue = 0;
        
        if (accumulatorMap != null && timePeriod != null && timecard != null) {
            if (accumulatorMap.containsKey(timePeriod)) {
                returnValue = accumulatorMap.get(timePeriod) + (timecard.pse__Total_Hours__c == null ? 0 : timecard.pse__Total_Hours__c);
            }
        }
        
        return returnValue;
    }
    
    public static double calculateAccumulatedAssignmentCost(Map<pse__Time_Period__c,double> accumulatorMap, pse__Time_Period__c timePeriod, pse__Timecard__c timecard) {
        double returnValue = 0;
        
        if (accumulatorMap != null && timePeriod != null && timecard != null) {
            if (accumulatorMap.containsKey(timePeriod)) {
                returnValue = accumulatorMap.get(timePeriod) + (timecard.pse__Total_Cost__c == null ? 0 : timecard.pse__Total_Cost__c);
            }
        }
        
        return returnValue;
    }
    
    public static double calculateAccumulatedExpenseCost(Map<pse__Time_Period__c,double> accumulatorMap, pse__Time_Period__c timePeriod, pse__Expense__c expense) {
        double returnValue = 0;
        
        if (accumulatorMap != null && timePeriod != null && expense != null) {
            if (accumulatorMap.containsKey(timePeriod)) {
                returnValue = accumulatorMap.get(timePeriod) + (expense.pse__Amount__c == null ? 0 : expense.pse__Amount__c);
            }
        }
        
        return returnValue;
    }
    
}