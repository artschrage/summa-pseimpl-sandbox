global class TimecardReminder2 implements Schedulable
{ 

 String<Id> sid;
 List<Id> lst;

 public TimecardReminder2(){
 }
 
 global void execute(SchedulableContext ctx) {
 	sid = getEmailTemplate();
 	lst = getEmailAddresses();
	sendEmail();
 }
 
  public void sendEmail()
  {
     if (sid == null){
     sid = '00X40000001aPlc';
     System.debug('hard coded email template');
     }
     System.debug('Start sendEmail');
      if (lst != null){
       Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
       mail.setTargetObjectIds(lst);
       mail.setTemplateId(sid);
       mail.setReplyTo('psaAdmin@summa-tech.com');
       mail.setSenderDisplayName('PSA Admin');
       Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
      }
      if (lst == null){
      System.debug('no email list, no emails sent');
      }
   }
   
  List<Id> getEmailAddresses(){  //get the list of people who haven't submitted a timecard (min 40 hrs) for this past week
      // this query gets the list of all active employees who have not submitted a timecard
      List<Contact> notimecard = [SELECT Id from Contact where pse__Is_Resource_Active__c = true and pse__Exclude_From_Missing_Timecards__c = false and Id not in
    (SELECT pse__Resource__c FROM pse__Timecard__c where pse__End_Date__c = THIS_WEEK and pse__Submitted__c = true)];
 
     //create the recipient list  
       List<Id> lstids = new List<Id>();
       System.debug('no timecard list size=' +notimecard.size());
   
       if (!notimecard.isEmpty()) { //check to make sure the query results are not null
         for(Integer i = 0; i < notimecard.size(); i++) //create ID list from Contact list
         {
         lstids.add(notimecard[i].Id);
         System.debug('ResourceId =' +lstids.get(i));
        }
         return lstids;
       } else {
      //return null;
      return lst;
      }
  
  }
   
    String<Id> getEmailTemplate(){ //get the email template for the reminder notification
    EmailTemplate e = [Select Id from EmailTemplate where Name = 'Timecard_Not_Submitted'];
        System.debug('email template ID = ' +e.Id);
        if (e != null) {
        return e.Id;
        } else {
        //return null;
        return sid;
        }
    }
        
}