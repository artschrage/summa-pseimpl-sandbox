public class TestProjectGenerator {
    private static pse__Time_Period__c previousFQ = 
                        TimePeriodUtils.getPrevFQ();
    
    public String getPrevFQ { 
        get{
            System.debug(previousFQ + ' ??????');
            return previousFQ.Name;
        }
        set; 
    }
    private static Integer countup = 0;

    public static void createProjects() {
        List<pse__Proj__c> projects = new List<pse__Proj__c>();
        projects.add(createProject('TNM')); 
        projects.add(createProject('TNM'));
        projects.add(createProject('TNM'));
        projects.add(createProject('TNM'));
        projects.add(createProject('TNM'));
        projects.add(createProject('TNM'));
        insert projects;
    }

    public static void updateProjects() {
        pse__Practice__c practice = [select id, Name from pse__Practice__c where Name = 'Core' LIMIT 1];
        System.debug('Practice: ' + practice.Name);
        
        List<pse__Proj__c> projects = [select id, Name from pse__Proj__c where Name like 'TNM%'];
        
        for (pse__Proj__c project : projects) {
            System.debug('Updating: ' + project.Name);
            project.pse__Bookings__c = 200000;
            project.pse__Billings__c = 150000;
            project.pse__Practice__c = practice.Id;

            // Add values to get total costs to equal $50,000
            project.pse__Internal_Costs__c = 20000.0;
            project.pse__External_Costs__c = 20000.0;
            project.pse__Expense_Costs__c = 5000.0;
            project.pse__Other_Costs__c = 5000.0;
            project.pse__End_Date__c = previousFQ.pse__End_Date__c;
            project.pse__Start_Date__c = previousFQ.pse__End_Date__c;
            project.Date_Became_Inactive__c = previousFQ.pse__End_Date__c;
        }
        update projects;
    }
    
    public static void destroyProjects() {
        List<pse__Proj__c> projects = [select id, Name from pse__Proj__c where Name like 'TNM%'];
        for (pse__Proj__c project : projects) {
            System.debug('Deleting: ' + project.Name);
        }
        delete projects;
    }

    public static pse__Proj__c createProject(String prefix) {
        String projectName = generateTestProjectName(prefix);
        System.debug('Creating: ' + projectName);
        pse__Proj__c project = new pse__Proj__c();
        project.Name = projectName;
        project.pse__Billing_Type__c = 'Time and Materials';
        project.pse__Is_Billable__c = true;
        project.pse__Region__c = DataFactoryTestBasicValues.regionCorporateId;
        return project;
    }


    public static String generateTestProjectName(String prefix) {
        String projName = prefix;
        DateTime now = DateTime.now();
        projName += now.format('yyMMddHHmmssSS');
        projName += Integer.valueOf(countup);
        ++countup ;
        return projName;
    }
}