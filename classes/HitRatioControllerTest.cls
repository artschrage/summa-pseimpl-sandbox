/*
    Unit test class for HitRatioController, which is the custom controller for the
    HitRatioReport VF page.
*/
@isTest()
private class HitRatioControllerTest 
{
    /*  Test entry point - create opportunities and instantiate class
    */
    static testMethod void testController() 
    {
        createOpportunities();
        //HitRatioController controller = new HitRatioController();
    }

    static void createOpportunities()
    {
        sObject corpRegion = Database.query('select Id from pse__Region__c where name = \'Corporate\' limit 1');
        ID corpRegionId = (ID)corpRegion.get('Id');
        system.debug('Current Region Id = ' + corpRegionId);
        
        sObject practice = Database.query('select Id from pse__Practice__c where name = \'Core\' limit 1');
        ID practiceId = (ID)practice.get('Id');
        system.debug('Current Practice Id = ' + practiceId);
        
        sObject pmContact = Database.query('select Id from Contact where name = \'Mitch Goldstein\' limit 1');
        ID pmContactId = (ID)pmContact.get('Id');
        system.debug('PM Contact Id = ' + pmContactId);

        Account newAccount = new Account(Name='TestAcct');
        insert newAccount;
        System.debug('Account = ' + newAccount.Name);
        System.assertEquals('TestAcct', newAccount.Name);
      
        Account account = [Select Id from Account where Name = 'TestAcct'];
        ID accountId = (ID)account.get('Id');
        system.debug('Current Account Id = ' + accountId);

        Opportunity o = new Opportunity();
        o.AccountId = accountId;
        o.Name = 'Opportunity1';
        o.pse__Practice__c = practiceId;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.StageName = 'Negotiate';
        o.CloseDate = Date.parse('09/12/2013');
        o.Start_Date__c = Date.parse('09/30/2013');
        o.pse__Region__c = corpRegionId;
        o.ForecastCategoryName = 'Closed';
        
        insert o;       
    }
}