public with sharing class ExistingClientAnalysisBso {

	private static ExistingClientAnalysisDao dao;
	private static CommonDao commonDao;
	
	static {
		dao = new ExistingClientAnalysisDao();
		commonDao = new CommonDao();
	}
	
	public ExistingClientAnalysisBso() {}
	
	public List<Account> getAccountsForAnalysis() {
		return commonDao.fetchAccountsWithIdOnly(true);
	}
	
	/**
	This method will define how far back we should go to begin pulling opportunity records.
	
	There is a nuance issue here in that at some point, history for accounts and opportunities is not
	stored in Salesforce relative to when the system was adopted. If we begin analysis in FY11, and there
	is an opportunity with no predecessor, is that because the client is new, or is it because the data
	is not resident in Salesforce and we could flag a false-positive?
	
	By defining a 2-year+ look-back window, and only using the first (oldest) year as history to make 
	decisions for the 2nd year, we can insulate against that possibility.
	*/
	public Date getAnalysisBeginDate(Date seedDate) {
		
		if (seedDate.month() >= 7) {
			return Date.newInstance(seedDate.year()-2, 7, 1);
		} else {
			return Date.newInstance(seedDate.year()-3, 7, 1);
		}
	}
	
	public List<Opportunity> analyzeOpportunityChanges(Set<Id> accountIds, Date analysisBeginDate) {
		//Create a list to cache the opportunities whose Is_New_Client_Opp__c needs to be updated.
		List<Opportunity> returnValue = new List<Opportunity>();

		//Get all of the opportunities that have been closed/won in the relevant time period.
		//Convert the list to a map keyed by the AccountId.
		List<Opportunity> opportunities = dao.fetchWonOpportunities(accountIds, analysisBeginDate);
		for (Opportunity opp : opportunities) {
			system.debug('dao.fetchWonOpportunities returned ['+ opp.Name +']');
		}
		Map<Id,List<Opportunity>> mapAccountOpportunities = convertOpportunityListToAccountMap(opportunities);
		
		//Prepare transient tracking support...
		List<Opportunity> accountOpportunities = null;
		Map<Id,Date> mapAccountRecentDealDate = new Map<Id,Date>();
		Date dealAnalysisThresholdDate = analysisBeginDate.addYears(1);
		Date dateOfLastDeal = null;
		Boolean oppRequiresUpdate = false;
		
		//Iterate the accounts and inspect their opportunities in chronological progression
		//to determine if the Is_New_Client_Opp__c is correctly set.
		for (Id accountId : mapAccountOpportunities.keySet()) {
			accountOpportunities = mapAccountOpportunities.get(accountId);
			dateOfLastDeal = null;
		
			for (Opportunity opp : accountOpportunities) {
				oppRequiresUpdate = false;
				
				//If the opportunity record is subject to system maintenance on the Is_New_Client_Opp__c field...
				//(the reasons it may not be are: manual override flag is set to block updates and/or the record
				//is too old in the window)
				if (isNewClientUpdateAllowed(opp, dealAnalysisThresholdDate)) {
					//Lookup with the last opportunity was closed/won with this account.
					dateOfLastDeal = getDateOfLastDeal(opp.AccountId, mapAccountRecentDealDate);
					
					//Determine if the Is_New_Client_Opp__c field should be updated.
					if (isNewClientUpdateNeeded(opp, dateOfLastDeal)) {
						oppRequiresUpdate = true;
						opp.Is_New_Client_Opp__c = true;
					}
				}
				
				//Cache the date of this opportunity to provide updated heuristics for the next opportunity record
				//being evaluated.
				mapAccountRecentDealDate.put(opp.AccountId, opp.CloseDate);
				//If the record was updated, cache it for later commit back to the data store.
				if (oppRequiresUpdate) {
					returnValue.add(opp);
				}
			}
		}
		
		for (Opportunity opp : returnValue) {
			system.debug('analyzeOpportunityChanges returning ['+ opp.Name +']');
		}

		return returnValue;
	}
	
	@TestVisible
	private Date getDateOfLastDeal(Id accountId, Map<Id,Date> mapAccountRecentDealDate) {
		Date returnValue = null;
		if (mapAccountRecentDealDate.containsKey(accountId)) {
			returnValue = mapAccountRecentDealDate.get(accountId);
		}
		return returnValue;
	}
	
	@TestVisible
	private Boolean isNewClientUpdateNeeded(Opportunity opp, Date dateOfLastDeal) {
		Boolean returnValue = (((dateOfLastDeal == null) || (dateOfLastDeal.addYears(1) < opp.CloseDate)) && !opp.Is_New_Client_Opp__c);

		return returnValue;
	}
	
	@TestVisible
	private Boolean isNewClientUpdateAllowed(Opportunity opp, Date dealAnalysisThresholdDate) {
		Boolean returnValue = opp.Allow_System_to_Determine_if_New_Opp__c && (opp.CloseDate > dealAnalysisThresholdDate);
		
		return returnValue;
	}
	
	public void storeOpportunityChanges(List<Opportunity> opportunities) {
		dao.storeOpportunityChanges(opportunities);
	}

	public void storeOpportunityChanges(Set<Opportunity> opportunities) {
		List<Opportunity> opps = new List<Opportunity>();
		for (Opportunity opp : opportunities) {
			opps.add(opp);
		}
		dao.storeOpportunityChanges(opps);
	}
	
	public List<New_Client_Value__c> analyzeNewClientValueChanges(Set<Id> accountIds, Date analysisBeginDate) {
		List<New_Client_Value__c> returnValue = new List<New_Client_Value__c>();
		
		List<Opportunity> opportunities = dao.fetchNewClientOpportunities(accountIds, analysisBeginDate);
		List<New_Client_Value__c> newClientValues = dao.fetchNewClientValues(accountIds, analysisBeginDate);
		
		Map<Id,List<Opportunity>> mapAccountOpportunities = convertOpportunityListToAccountMap(opportunities);
		Map<Id,List<New_Client_Value__c>> mapAccountNewClientValues = convertNewClientValueListToAccountMap(newClientValues);

		for (Id id : mapAccountOpportunities.keySet()) {
			for (Opportunity opp : mapAccountOpportunities.get(id)) {
				system.debug('Account Id ['+ id +'] mapped to Opportunity ['+ opp.Name +']');
			}
		}
		for (Id id : mapAccountNewClientValues.keySet()) {
			for (New_Client_Value__c ncv : mapAccountNewClientValues.get(id)) {
				system.debug('Account Id ['+ id +'] mapped to New Client Value ['+ ncv.Start_Date__c +'/'+ ncv.End_Date__c +']');
			}
		}

		List<Opportunity> accountOpps = null;
		List<New_Client_Value__c> accountNcvs = null;
		Boolean isNcvRequired = false;
		
		for (Id accountId : mapAccountOpportunities.keySet()) {
			accountOpps = mapAccountOpportunities.get(accountId);
			accountNcvs = mapAccountNewClientValues.get(accountId);
			
			for (Opportunity opp : accountOpps) {
				isNcvRequired = isNewClientValueNeeded(opp, accountNcvs);
				
				if (isNcvRequired) {
					New_Client_Value__c ncv = createNewClientValue(accountId, opp);
					addNewClientValueToMap(accountId, ncv, mapAccountNewClientValues);
					
					returnValue.add(ncv);
				}
			}
		}
		
		return returnValue;
	}
	
	@TestVisible
	private void addNewClientValueToMap(Id accountId, New_Client_Value__c ncv, Map<Id,List<New_Client_Value__c>> mapAccountNewClientValues) {
		if (mapAccountNewClientValues.containsKey(accountId)) {
			mapAccountNewClientValues.get(accountId).add(ncv);
		} else {
			mapAccountNewClientValues.put(accountId, new New_Client_Value__c[]{ncv});
		}
	}
	
	@TestVisible
	private New_Client_Value__c createNewClientValue(Id accountId, Opportunity opp) {
		New_Client_Value__c ncv = new New_Client_Value__c();
		ncv.Account__c = accountId;
		ncv.Start_Date__c = opp.CloseDate;
		ncv.End_Date__c = opp.CloseDate.addYears(1);

		return ncv;		
	}
	
	@TestVisible
	private Boolean isNewClientValueNeeded(Opportunity opp, List<New_Client_Value__c> accountNcvs) {
		Boolean	isNcvRequired = false;
		Boolean ncvMatchFound = false;

		if (accountNcvs != null) {
			ncvMatchFound = false;
			for (New_Client_Value__c ncv : accountNcvs) {
				if (isOpportunityInNewClientRange(ncv, opp)) {
					ncvMatchFound = true;
					break;
				}
			}
			isNcvRequired = (!ncvMatchFound);
		} else {
			isNcvRequired = true;
		}
		
		return isNcvRequired;
	}
	
	@TestVisible
	private Boolean isOpportunityInNewClientRange(New_Client_Value__c ncv, Opportunity opp) {
		if ((opp != null) && (ncv != null)) {
			return ((ncv.Start_Date__c <= opp.CloseDate) && (ncv.End_Date__c >= opp.CloseDate));
		} else {
			return false;
		}
	}
	
	public void storeNewClientValueChanges(List<New_Client_Value__c> ncv) {
		dao.storeNewClientValueChanges(ncv);
	}
	
	public List<Opportunity> analyzeOpportunityNewClientValueChanges(Set<Id> accountIds, Date analysisBeginDate) {
		//create a map of Opportunities by their Ids to cache the opportunities whose new client value relationship 
		//needs to be updated. The map.values() will be returned as the List.
		Map<Id,Opportunity> returnValue = new Map<Id,Opportunity>();
		
		//Get all closed/won opportunities for the set of accounts we're working on, not just the ones flagged as "new".
		//By now, all opportunity record Is_New_Client_Opp__c fields should be set correctly.
		//get all new client value records for the set of accounts we're working on
		List<Opportunity> opportunities = dao.fetchWonOpportunities(accountIds, analysisBeginDate);
		List<New_Client_Value__c> newClientValues = dao.fetchNewClientValues(accountIds, analysisBeginDate);
		
		Set<Id> newClientValueIds = new Set<Id>();
		for (New_Client_Value__c ncv : newClientValues) {
			newClientValueIds.add(ncv.Id);
		}

		Map<Id,List<Opportunity>> mapAccountOpportunities = convertOpportunityListToAccountMap(opportunities);
		Map<Id,List<New_Client_Value__c>> mapAccountNewClientValues = convertNewClientValueListToAccountMap(newClientValues);

		List<New_Client_Value__c> accountNewClientValues = null;
		List<Opportunity> accountOpportunities = null;
		Boolean isUpdateRequired = false;
		
		//first things first- there is a chance the ncv was deleted and the opp
		//references a stale id. clean it up. this won't be caught in the next 
		//section because it's ncv-centric.
		for (Opportunity opp : opportunities) {
			if ((opp.New_Client_Value__c != null) && !newClientValueIds.contains(opp.New_Client_Value__c)) {
				opp.New_Client_Value__c = null;
				returnValue.put(opp.Id, opp);
			}
		}

		for (Id accountId : mapAccountNewClientValues.keySet()) {
			accountNewClientValues = mapAccountNewClientValues.get(accountId);
			accountOpportunities = mapAccountOpportunities.get(accountId);

			system.debug('Account ['+ accountId +'] has ['+ accountNewClientValues.size() +'] New_Client_Value__c and ['+ accountOpportunities.size() +'] Opportunity records.');
			
			for (New_Client_Value__c newClientValue : accountNewClientValues) {
				system.debug('Checking opportunities against New_Client_Value__c ['+ newClientValue.Start_Date__c +'/'+ newClientValue.End_Date__c +']');

				for (Opportunity opportunity : accountOpportunities) {
					isUpdateRequired = false;
					
					if (isOpportunityInNewClientRange(newClientValue, opportunity)) {
						if (opportunity.New_Client_Value__c != newClientValue.Id) {
							isUpdateRequired = true;
							opportunity.New_Client_Value__c = newClientValue.Id;
						}
					} else {
						if (opportunity.New_Client_Value__c == newClientValue.Id) {
							isUpdateRequired = true;
							opportunity.New_Client_Value__c = null;
						}
					}
					
					system.debug('Opportunity ['+ opportunity.Name +'] requires update? ['+ isUpdateRequired +'] Already in set? ['+ !returnValue.containsKey(opportunity.Id) +']');

					//contains check here is too late because hash is different and won't match in the set.
					//need to know earlier that set already contains opp so don't re-add it. changes will be
					//captured since reference to record is stored in set, but that's worth testing to verify.
					if (isUpdateRequired && !returnValue.containsKey(opportunity.Id)) {
						returnValue.put(opportunity.Id, opportunity);
					}
				}
			}
		}

		return returnValue.values();
	}
	
	@TestVisible
	private Map<Id,List<Opportunity>> convertOpportunityListToAccountMap(List<Opportunity> opportunities) {
		Map<Id,List<Opportunity>> returnValue = new Map<Id,List<Opportunity>>();
		
		List<Opportunity> newList;
		if (opportunities != null) {
	    	for (Opportunity opportunity : opportunities) {
	    		if (returnValue.containsKey(opportunity.AccountId)) {
	    			returnValue.get(opportunity.AccountId).add(opportunity);
	    		} else {
	    			newList = new List<Opportunity>();
	    			newList.add(opportunity);
	    			returnValue.put(opportunity.AccountId, newList);
	    			newList = null;
	    		}
	    	}
		}		
		
		return returnValue;
	}
	
	@TestVisible
	private Map<Id,List<New_Client_Value__c>> convertNewClientValueListToAccountMap(List<New_Client_Value__c> newClientValues) {
		Map<Id,List<New_Client_Value__c>> returnValue = new Map<Id,List<New_Client_Value__c>>();
		
		List<New_Client_Value__c> newList;
		if (newClientValues != null) {
	    	for (New_Client_Value__c ncv : newClientValues) {
	    		if (returnValue.containsKey(ncv.Account__c)) {
	    			returnValue.get(ncv.Account__c).add(ncv);
	    		} else {
	    			newList = new List<New_Client_Value__c>();
	    			newList.add(ncv);
	    			returnValue.put(ncv.Account__c, newList);
	    			newList = null;
	    		}
	    	}
		}
				
		return returnValue;
	}
}