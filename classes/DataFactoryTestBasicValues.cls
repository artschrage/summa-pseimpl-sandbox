public with sharing class DataFactoryTestBasicValues {

	public static Contact johnSmail;
	public static Contact mitchGoldstein;
	public static Contact jeffHowell;
	public static Contact viaTsuji;
	
	public static ID regionCorporateId;
	public static ID practiceCoreId;

	public static string projectBillingTypeTM = 'Time and Materials';
	public static string projectBillingTypeFixed = 'Fixed Price';
	
	public static date dateOrigin;
	public static date dateOriginFirstDay;
	public static date dateOriginLastDay;
	public static date dateOriginFifteenth;
	public static date dateOneMonthAfterOrigin;
	public static date dateTwoMonthsAfterOrigin;
	public static date dateThreeMonthsAfterOrigin;
	public static date dateSixMonthsAfterOrigin;
	public static date dateTwelveMonthsAfterOrigin;
	
	static {
        johnSmail = [select id, name from Contact where name = 'John Smail' limit 1];
        system.debug('johnSmail Contact Id = ' + johnSmail.id);

        mitchGoldstein = [select id, name from Contact where name = 'Mitch Goldstein' limit 1];
        system.debug('mitchGoldstein Contact Id = ' + mitchGoldstein.id);

        jeffHowell = [select id, name from Contact where name = 'Jeff Howell' limit 1];
        system.debug('jeffHowell Contact Id = ' + jeffHowell.id);

        viaTsuji = [select id, name from Contact where name = 'Via Tsuji' limit 1];
        system.debug('viaTsuji Contact Id = ' + viaTsuji.id);

    	sObject corpRegion = Database.query('select Id from pse__Region__c where name = \'Corporate\' limit 1');
        regionCorporateId = (ID)corpRegion.get('Id');
        system.debug('Corporate Region Id = ' + regionCorporateId);
        
        sObject practice = Database.query('select Id from pse__Practice__c where name = \'Core\' limit 1');
        practiceCoreId = (ID)practice.get('Id');
        system.debug('Core Practice Id = ' + practiceCoreId);
        
        dateOrigin = date.today();
    	dateOriginFirstDay = Date.newInstance(dateOrigin.year(), dateOrigin.month(), 1);
    	dateOriginFifteenth = Date.newInstance(dateOrigin.year(), dateOrigin.month(), 15);
    	dateOriginLastDay = Date.newInstance(dateOrigin.year(), dateOrigin.month(), Date.daysInMonth(dateOrigin.year(), dateOrigin.month()));
    	

    	dateOneMonthAfterOrigin = dateOriginFifteenth.addMonths(1);
    	dateTwoMonthsAfterOrigin = dateOriginFifteenth.addMonths(2);
    	dateThreeMonthsAfterOrigin = dateOriginFifteenth.addMonths(3);
    	dateSixMonthsAfterOrigin = dateOriginFifteenth.addMonths(6);
    	dateTwelveMonthsAfterOrigin = dateOriginFifteenth.addMonths(12);
    }
    
    public static void createDataForTestAggregationAndStorageSingleAccount() {
    	Contact jSmail = [select id, name from Contact where name = 'John Smail' limit 1];
    	pse__Region__c regionCorporate = [select Id from pse__Region__c where name = 'Corporate' limit 1];
		pse__Practice__c practiceCore = [select Id from pse__Practice__c where name = 'Core' limit 1];
    	
    	
    	Account account = new Account(Name='TestComplimentaryServiceTools Account', CurrencyIsoCode='USD');
		insert account;
		
		pse__Proj__c project = new pse__Proj__c();
	    project.Name = 'testAggregationAndStorageSingleAccount Project';
	    project.pse__Account__c = account.Id;
	    project.pse__Is_Active__c = true; 
	    project.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	    project.pse__Billing_Type__c = 'Time and Materials';
	    project.pse__Start_Date__c = Date.newInstance(2014,1,1);
	    project.pse__End_Date__c = Date.newInstance(2014,1,31);
	    project.Date_Became_Inactive__c = null;
	    project.pse__Project_Manager__c = jSmail.Id;
	    project.Delivery_Engagement_Manager__c = jSmail.Id;
	    project.pse__Region__c = regionCorporate.Id;
	    project.pse__Practice__c = practiceCore.Id;
	    insert project;
	    
		pse__Schedule__c schedule = new pse__Schedule__c();
		schedule.pse__Start_Date__c = project.pse__Start_Date__c;
		schedule.pse__End_Date__c = project.pse__End_Date__c;
		schedule.pse__Sunday_Hours__c = 0;
		schedule.pse__Monday_Hours__c = 8;
		schedule.pse__Tuesday_Hours__c = 8;
		schedule.pse__Wednesday_Hours__c = 8;
		schedule.pse__Thursday_Hours__c = 8;
		schedule.pse__Friday_Hours__c = 8;
		schedule.pse__Saturday_Hours__c = 0;
	    insert schedule;
	    
	    pse__Assignment__c assignment = new pse__Assignment__c();
		assignment.name = 'Assignment on ' + project.name;
		assignment.pse__Project__c = project.Id;
		assignment.pse__Resource__c = jSmail.Id;
		assignment.pse__Schedule__c = schedule.Id;
		assignment.pse__Is_Billable__c = false;
		assignment.pse__Bill_Rate__c = 0;
		insert assignment;
	    
	    map<integer,pse__Timecard_Header__c> headers = new map<integer,pse__Timecard_Header__c>();
	    
		pse__Timecard_Header__c	timecardHeader = new pse__Timecard_Header__c();
		timecardHeader.pse__Project__c = assignment.pse__Project__c;
		timecardHeader.pse__Assignment__c = assignment.Id;
		timecardHeader.pse__Resource__c = assignment.pse__Resource__c;
		timecardHeader.pse__Billable__c = assignment.pse__Is_Billable__c;
		timecardHeader.pse__Start_Date__c = Date.newInstance(2013,12,29);
		timecardHeader.pse__End_Date__c = Date.newInstance(2014,1,4);
		timecardHeader.pse__Submitted__c = true;
		timecardHeader.pse__Approved__c = true;
		timecardHeader.pse__Approver__c = UserInfo.getUserId();
		timecardHeader.pse__Status__c = 'Approved';
		timecardHeader.pse__Sunday_Hours__c = 0;
		timecardHeader.pse__Monday_Hours__c = 0;
		timecardHeader.pse__Tuesday_Hours__c = 0;
		timecardHeader.pse__Wednesday_Hours__c = .5;
		timecardHeader.pse__Thursday_Hours__c = .5;
		timecardHeader.pse__Friday_Hours__c = .5;
		timecardHeader.pse__Saturday_Hours__c = 0;
		headers.put(0,timecardHeader);

		timecardHeader = new pse__Timecard_Header__c();
		timecardHeader.pse__Project__c = assignment.pse__Project__c;
		timecardHeader.pse__Assignment__c = assignment.Id;
		timecardHeader.pse__Resource__c = assignment.pse__Resource__c;
		timecardHeader.pse__Billable__c = assignment.pse__Is_Billable__c;
		timecardHeader.pse__Start_Date__c = Date.newInstance(2014,1,5);
		timecardHeader.pse__End_Date__c = Date.newInstance(2014,1,11);
		timecardHeader.pse__Submitted__c = true;
		timecardHeader.pse__Approved__c = true;
		timecardHeader.pse__Approver__c = UserInfo.getUserId();
		timecardHeader.pse__Status__c = 'Approved';
		timecardHeader.pse__Sunday_Hours__c = 0;
		timecardHeader.pse__Monday_Hours__c = .5;
		timecardHeader.pse__Tuesday_Hours__c = .5;
		timecardHeader.pse__Wednesday_Hours__c = .5;
		timecardHeader.pse__Thursday_Hours__c = .5;
		timecardHeader.pse__Friday_Hours__c = .5;
		timecardHeader.pse__Saturday_Hours__c = 0;
		headers.put(1,timecardHeader);
		
		timecardHeader = new pse__Timecard_Header__c();
		timecardHeader.pse__Project__c = assignment.pse__Project__c;
		timecardHeader.pse__Assignment__c = assignment.Id;
		timecardHeader.pse__Resource__c = assignment.pse__Resource__c;
		timecardHeader.pse__Billable__c = assignment.pse__Is_Billable__c;
		timecardHeader.pse__Start_Date__c = Date.newInstance(2014,1,12);
		timecardHeader.pse__End_Date__c = Date.newInstance(2014,1,18);
		timecardHeader.pse__Submitted__c = true;
		timecardHeader.pse__Approved__c = true;
		timecardHeader.pse__Approver__c = UserInfo.getUserId();
		timecardHeader.pse__Status__c = 'Approved';
		timecardHeader.pse__Sunday_Hours__c = 0;
		timecardHeader.pse__Monday_Hours__c = .5;
		timecardHeader.pse__Tuesday_Hours__c = .5;
		timecardHeader.pse__Wednesday_Hours__c = .5;
		timecardHeader.pse__Thursday_Hours__c = .5;
		timecardHeader.pse__Friday_Hours__c = .5;
		timecardHeader.pse__Saturday_Hours__c = 0;
		headers.put(2,timecardHeader);
		
		timecardHeader = new pse__Timecard_Header__c();
		timecardHeader.pse__Project__c = assignment.pse__Project__c;
		timecardHeader.pse__Assignment__c = assignment.Id;
		timecardHeader.pse__Resource__c = assignment.pse__Resource__c;
		timecardHeader.pse__Billable__c = assignment.pse__Is_Billable__c;
		timecardHeader.pse__Start_Date__c = Date.newInstance(2014,1,19);
		timecardHeader.pse__End_Date__c = Date.newInstance(2014,1,25);
		timecardHeader.pse__Submitted__c = true;
		timecardHeader.pse__Approved__c = true;
		timecardHeader.pse__Approver__c = UserInfo.getUserId();
		timecardHeader.pse__Status__c = 'Approved';
		timecardHeader.pse__Sunday_Hours__c = 0;
		timecardHeader.pse__Monday_Hours__c = .5;
		timecardHeader.pse__Tuesday_Hours__c = .5;
		timecardHeader.pse__Wednesday_Hours__c = .5;
		timecardHeader.pse__Thursday_Hours__c = .5;
		timecardHeader.pse__Friday_Hours__c = .5;
		timecardHeader.pse__Saturday_Hours__c = 0;
		headers.put(3,timecardHeader);
		
		timecardHeader = new pse__Timecard_Header__c();
		timecardHeader.pse__Project__c = assignment.pse__Project__c;
		timecardHeader.pse__Assignment__c = assignment.Id;
		timecardHeader.pse__Resource__c = assignment.pse__Resource__c;
		timecardHeader.pse__Billable__c = assignment.pse__Is_Billable__c;
		timecardHeader.pse__Start_Date__c = Date.newInstance(2014,1,26);
		timecardHeader.pse__End_Date__c = Date.newInstance(2014,2,1);
		timecardHeader.pse__Submitted__c = true;
		timecardHeader.pse__Approved__c = true;
		timecardHeader.pse__Approver__c = UserInfo.getUserId();
		timecardHeader.pse__Status__c = 'Approved';
		timecardHeader.pse__Sunday_Hours__c = 0;
		timecardHeader.pse__Monday_Hours__c = .5;
		timecardHeader.pse__Tuesday_Hours__c = .5;
		timecardHeader.pse__Wednesday_Hours__c = .5;
		timecardHeader.pse__Thursday_Hours__c = .5;
		timecardHeader.pse__Friday_Hours__c = .5;
		timecardHeader.pse__Saturday_Hours__c = 0;
		headers.put(4,timecardHeader);
		
		insert headers.values();
		
		list<pse__Timecard__c> splits = new list<pse__Timecard__c>();
	    pse__Timecard__c split;
	    
		timecardHeader = headers.get(0);
	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = timecardHeader.pse__Start_Date__c;
		split.pse__End_Date__c = Date.newInstance(2013,12,31);
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;
		split.pse__Total_Cost__c = 0;
		splits.add(split);

	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = Date.newInstance(2014,1,1);
		split.pse__End_Date__c = timecardHeader.pse__End_Date__c;
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;
		split.pse__Total_Cost__c = 165;
		splits.add(split);
		
		timecardHeader = headers.get(1);
	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = timecardHeader.pse__Start_Date__c;
		split.pse__End_Date__c = timecardHeader.pse__End_Date__c;
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;				
		split.pse__Total_Cost__c = 275;
		splits.add(split);

		timecardHeader = headers.get(2);
	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = timecardHeader.pse__Start_Date__c;
		split.pse__End_Date__c = timecardHeader.pse__End_Date__c;
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;				
		split.pse__Total_Cost__c = 275;
		splits.add(split);

		timecardHeader = headers.get(3);
	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = timecardHeader.pse__Start_Date__c;
		split.pse__End_Date__c = timecardHeader.pse__End_Date__c;
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;				
		split.pse__Total_Cost__c = 275;
		splits.add(split);

		timecardHeader = headers.get(4);
	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = timecardHeader.pse__Start_Date__c;
		split.pse__End_Date__c = Date.newInstance(2014,1,31);
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;				
		split.pse__Total_Cost__c = 275;
		splits.add(split);

	    split = new pse__Timecard__c();		
		split.pse__Timecard_Header__c = timecardHeader.Id;
		split.pse__Project__c = timecardHeader.pse__Project__c;
		split.pse__Resource__c = timecardHeader.pse__Resource__c;
		split.pse__Assignment__c = timecardHeader.pse__Assignment__c;
		split.pse__Submitted__c = timecardHeader.pse__Submitted__c;
		split.pse__Approved__c = timecardHeader.pse__Approved__c;
		split.pse__Billable__c = timecardHeader.pse__Billable__c;
		split.pse__Status__c = timecardHeader.pse__Status__c;
		split.pse__Start_Date__c = Date.newInstance(2014,2,1);
		split.pse__End_Date__c = timecardHeader.pse__End_Date__c;
		split.pse__Sunday_Hours__c = timecardHeader.pse__Sunday_Hours__c;
		split.pse__Monday_Hours__c = timecardHeader.pse__Monday_Hours__c;
		split.pse__Tuesday_Hours__c = timecardHeader.pse__Tuesday_Hours__c;				
		split.pse__Wednesday_Hours__c = timecardHeader.pse__Wednesday_Hours__c;
		split.pse__Thursday_Hours__c = timecardHeader.pse__Thursday_Hours__c;
		split.pse__Friday_Hours__c = timecardHeader.pse__Friday_Hours__c;				
		split.pse__Saturday_Hours__c = timecardHeader.pse__Saturday_Hours__c;				
		split.pse__Total_Cost__c = 0;
		splits.add(split);
		
		insert splits;

		pse__Expense_Report__c report = new pse__Expense_Report__c();
		report.pse__Assignment__c = assignment.Id;
		report.pse__Billable__c = assignment.pse__Is_Billable__c;
		report.CurrencyIsoCode = 'USD';
		report.pse__Project__c = project.Id;
		report.pse__Resource__c = jSmail.Id;
		report.pse__Status__c = 'Draft';
		report.pse__Last_Expense_Date__c = project.pse__End_Date__c;
				
		insert report;

		pse__Expense__c expense = new pse__Expense__c();
		expense.pse__Amount__c = 250;
		expense.pse__Assignment__c = assignment.Id;
		expense.pse__Billable__c = report.pse__billable__c;
		expense.pse__Expense_Date__c = project.pse__End_Date__c;
		expense.pse__Expense_Report__c = report.Id;
		expense.pse__Project__c = project.Id;
		expense.pse__Resource__c = jSmail.Id;
		expense.pse__Type__c = 'Business Meals';
		
		insert expense;
		
		report.pse__Submitted__c = true;
		report.pse__Approved__c = true;
		report.pse__Approver__c = UserInfo.getUserId();
		report.pse__Status__c = 'Approved';
		
		update report;
    }
		
}