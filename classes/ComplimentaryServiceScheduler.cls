global class ComplimentaryServiceScheduler implements Schedulable {
	
	global void execute(SchedulableContext SC) {
		try {
			ComplimentaryServiceBatcher batch = new ComplimentaryServiceBatcher();
			//The last argument will set the BatchableContext chunk size to keep the number of projects concurrently processed low
			String cronID = System.scheduleBatch(batch, 'Recalculate Project Complimentary Services', 1, 20);
		} catch (Exception e) {
			ExceptionUtils.logException(e);
		}
	}
}