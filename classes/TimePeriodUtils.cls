/**
    TimePeriodUtils
    
    Static helper class providing TimePeriod-related helper methods
**/
public class TimePeriodUtils {

    /*
        Retrieve the time period for the previous fiscal quarter
    */
    public static pse__Time_Period__c getPrevFQ() {
        pse__Time_Period__c pfq = [SELECT Id, Name, pse__Start_Date__c, 
              pse__End_Date__c from pse__Time_Period__c 
                    where pse__Type__c = 'Quarter' and 
                    pse__End_Date__c <= TODAY 
                    order by pse__End_Date__c DESC LIMIT 1]; 
        System.debug(System.LoggingLevel.FINE, 'PFQ = ' + pfq.Name);
        return pfq;
    }
}