public abstract class KPIHitRatioBaseReportLogic implements KPIReportInterface {
    protected Map<String, Decimal> stageMap;
  
    protected KPIHitRatioBaseReportLogic() {
        buildStageMap();
    }
 
    protected void buildStageMap() {
        stageMap = new Map<String, Decimal>();
        List<OpportunityStage> allStages =
                    [SELECT MasterLabel, DefaultProbability from 
                        OpportunityStage order by DefaultProbability];
        for (OpportunityStage stage : allStages) {
            System.debug(logginglevel.FINE, stage.MasterLabel + ' ' + 
                            stage.DefaultProbability);
            stageMap.put(stage.MasterLabel, stage.DefaultProbability);
        }
        System.debug('StageMap built with ' + stageMap.size() + ' keys');
    }
 
    protected Map<String, Opportunity> getOpportunityMap(Date startDate, Date endDate) {
        System.debug('GOM: start=' + startDate + ' end=' + endDate);
        List<Opportunity> oppList = [SELECT Id, Name, Amount, CloseDate, StageName
                    FROM Opportunity WHERE CloseDate >= :startDate AND
                                CloseDate <= :endDate ORDER BY CloseDate];
        System.debug('Opportunity map: ' + oppList.size() + ' records');
        Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
        for (Opportunity opp : oppList) {
            oppMap.put(opp.Id, opp);
        }
        return oppMap;
    }
    
    // BASE CLASS
    protected Map<String, List<String>> buildHistoryMap(List<OpportunityFieldHistory> histories) {
        Map<String, List<String>> historyMap = new Map<String, List<String>>();

        // Since the history values are the string version and not the 
        // numeric probability version we can't get a sort from the query.
        for (OpportunityFieldHistory history : histories) {
            // If we don't have an entry for an opportunity, create an entry
            if (!historyMap.containsKey(history.OpportunityId)) {
                historyMap.put(history.OpportunityId, new List<String>());
            }
            // Add the string value to the list.  We will translate the 
            // string value to a numeric value when we iterate again.
            List<String> oppList = historyMap.get(history.OpportunityId);
            oppList.add((String) history.NewValue);
        }
        return historyMap;
    }
    
    // BASE CLASS
    protected Decimal getHighestProbability(Opportunity opp, List<String> values) {
        // Start off with whatever value the opportunity currently had in case
        // there are no historic values
        Decimal maxProb = stageMap.get(opp.StageName);
        if (maxProb == null) {
            System.debug(LoggingLevel.WARN, 'maxProb is null?');
            return 0.0;
        }
        if (values == null) {
            return maxProb;
        }
        
        // If this history record has a probabilty any higher
        // that the current highest, replace that max value
        for (String value : values) {
            Decimal prob = stageMap.get(value);
            if (prob > maxProb) {
                maxProb = prob;
            }
        }
        return maxProb;
    }
}