public with sharing class DataFactoryTestAccounts {

	private static list<Account> accounts;
	
	private static string accountNameA = 'Test Acct A';
	private static string accountNameB = 'Test Acct B';
	private static string accountNameC = 'Test Acct C';
	private static string accountNameD = 'Test Acct D';
	private static string accountNameE = 'Test Acct E';
	private static string accountNameF = 'Test Acct F';
	
	static {
		accounts = new list<Account>();
	}
	
	public static boolean isDataValid() {
		//need to check on accounts in db and verify expectations are met
		//is this even viable? what would cleanup look like due to dependencies?
		return false;
	}
	
    public static void createAccounts() {
        Account accountA = new Account(Name=accountNameA, CurrencyIsoCode='USD');
        Account accountB = new Account(Name=accountNameB, CurrencyIsoCode='USD');
        Account accountC = new Account(Name=accountNameC, CurrencyIsoCode='USD');
        Account accountD = new Account(Name=accountNameD, CurrencyIsoCode='USD');
        Account accountE = new Account(Name=accountNameE, CurrencyIsoCode='USD');
        Account accountF = new Account(Name=accountNameF, CurrencyIsoCode='USD');

		accounts.add(accountA);
		accounts.add(accountB);
		accounts.add(accountC);
		accounts.add(accountD);
		accounts.add(accountE);
		accounts.add(accountF);

		for (Account a : accounts) {
			system.debug('Before account insert values: id='+ a.id +'; name='+ a.name +';');
		}

		insert accounts;
		
		for (Account a : accounts) {
			system.debug('After account insert values: id='+ a.id +'; name='+ a.name +';');
		}
    }
    
    public static list<Account> getAccounts() {
    	if (accounts.size() == 0) {
    		accounts = [select id, name from Account where name in (:accountNameA, :accountNameB, :accountNameC, :accountNameD, :accountNameE, :accountNameF)];
    	}
    	
    	return accounts;
    }
    
    public static set<Id> getAccountIds() {
    	set<Id> returnValue = new set<Id>();
    	
    	for (Account a : getAccounts()) {
    		returnValue.add(a.Id);
    	}
    	
    	return returnValue;
    }
}