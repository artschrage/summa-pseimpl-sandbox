public class KPITotalGrossMarginReportLogic implements KPIReportInterface {
    public KPI_Value__c execute(KPI_Value__c kpiValue) {
     Decimal actual = 0.0;
        AggregateResult[] results = [SELECT  
              SUM(pse__Billings__c) billsum, SUM(pse__Margin__c) margsum
              FROM pse__Project_Actuals__c
                  WHERE pse__Time_Period__r.Id= :kpiValue.Time_Period__r.Id AND
                    pse__Project__r.pse__Master_Project__r.Name = '' AND 
                    pse__Project__r.pse__Practice__r.Name != '' AND 
                    pse__Project__r.pse__Is_Billable__c = true];
                    
        for (AggregateResult result : results) {
            Decimal margsum = (Decimal) result.get('margsum');
            Decimal billsum = (Decimal) result.get('billsum');
            System.debug(LoggingLevel.FINE, 'Margin: ' + margsum);
            System.debug(LoggingLevel.FINE, 'Billings: ' + billsum);
            
            if (billsum == null || billsum == 0.0) {
              System.debug('No billings, setting actual to zero');
              actual = 0.0;
            }
            else {
              actual = margsum / billsum;
            }
            System.debug(LoggingLevel.FINE, 'Total Gross Margin: ' + actual);
        }
        
        kpiValue.Actual__c = actual;
        return kpiValue;
    }
}