/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class TestComplimentaryServiceUtils {

    static testMethod void testFirstOfMonth() {
    	Test.startTest();
    	
		Date originalDate = Date.newInstance(2014, 1, 15);
		Date newDate = ComplimentaryServiceUtils.toFirstOfMonth(originalDate);
		
		system.assertEquals(newDate.day(), 1);
		
		Test.stopTest();
    }
    
    static testMethod void testLastOfMonth31() {
    	Test.startTest();
    	
		Date originalDate = Date.newInstance(2014, 1, 15);
		Date newDate = ComplimentaryServiceUtils.toLastOfMonth(originalDate);
		
		system.assertEquals(newDate.day(), 31);
		
		Test.stopTest();
    }

    static testMethod void testLastOfMonthLeap() {
    	Test.startTest();
    	
		Date originalDate = Date.newInstance(2012, 2, 15);
		Date newDate = ComplimentaryServiceUtils.toLastOfMonth(originalDate);
		
		system.assertEquals(newDate.day(), 29);
		
		Test.stopTest();
    }
    
    static testMethod void testProjectTimePeriods() {
    	Date startDate = Date.newInstance(2014, 2, 1);
    	Date endDate = Date.newInstance(2014, 8, 17);
    	
    	list<pse__Time_Period__c> allPeriods = new list<pse__Time_Period__c>();
    	allPeriods.add(new pse__Time_Period__c(name='Nov FY2014', pse__Start_Date__c=Date.newInstance(2013,11,1), pse__End_Date__c=Date.newInstance(2013,11,30), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Dec FY2014', pse__Start_Date__c=Date.newInstance(2013,12,1), pse__End_Date__c=Date.newInstance(2013,12,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Jan FY2014', pse__Start_Date__c=Date.newInstance(2014,1,1), pse__End_Date__c=Date.newInstance(2014,1,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Feb FY2014', pse__Start_Date__c=Date.newInstance(2014,2,1), pse__End_Date__c=Date.newInstance(2014,2,28), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Mar FY2014', pse__Start_Date__c=Date.newInstance(2014,3,1), pse__End_Date__c=Date.newInstance(2014,3,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Apr FY2014', pse__Start_Date__c=Date.newInstance(2014,4,1), pse__End_Date__c=Date.newInstance(2014,4,30), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='May FY2014', pse__Start_Date__c=Date.newInstance(2014,5,1), pse__End_Date__c=Date.newInstance(2014,5,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Jun FY2014', pse__Start_Date__c=Date.newInstance(2014,6,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Jul FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2014,7,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Aug FY2015', pse__Start_Date__c=Date.newInstance(2014,8,1), pse__End_Date__c=Date.newInstance(2014,8,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Sep FY2015', pse__Start_Date__c=Date.newInstance(2014,9,1), pse__End_Date__c=Date.newInstance(2014,9,30), pse__Type__c='Month'));
    	
     	allPeriods.add(new pse__Time_Period__c(name='Q2 FY2014', pse__Start_Date__c=Date.newInstance(2013,10,1), pse__End_Date__c=Date.newInstance(2013,12,31), pse__Type__c='Quarter'));
    	allPeriods.add(new pse__Time_Period__c(name='Q3 FY2014', pse__Start_Date__c=Date.newInstance(2014,1,1), pse__End_Date__c=Date.newInstance(2014,3,31), pse__Type__c='Quarter'));
     	allPeriods.add(new pse__Time_Period__c(name='Q4 FY2014', pse__Start_Date__c=Date.newInstance(2014,4,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Quarter'));
    	allPeriods.add(new pse__Time_Period__c(name='Q1 FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2014,9,30), pse__Type__c='Quarter'));
    	
    	allPeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	allPeriods.add(new pse__Time_Period__c(name='FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2015,6,30), pse__Type__c='Year'));
    	
    	map<string, integer> mapTypeToCount = new map<string, integer>();
    	mapTypeToCount.put('Month', 0);
    	mapTypeToCount.put('Quarter', 0);
    	mapTypeToCount.put('Year', 0);
    	
    	Test.startTest();
    	
    	list<pse__Time_Period__c> projectPeriods = ComplimentaryServiceUtils.getMatchingTimePeriods(allPeriods, startDate, endDate);
    	
    	for (pse__Time_Period__c tp : projectPeriods) {
    		mapTypeToCount.put(tp.pse__type__c,  mapTypeToCount.containsKey(tp.pse__type__c) ? mapTypeToCount.get(tp.pse__type__c)+1 : 1);
    	}
    	
    	system.assertEquals(mapTypeToCount.get('Month'), 7, 'Month failed expectations');
    	system.assertEquals(mapTypeToCount.get('Quarter'), 3, 'Quarter failed expectations');
    	system.assertEquals(mapTypeToCount.get('Year'), 2, 'Year failed expectations');
		
		Test.stopTest();
    }
    
    static testMethod void testBuildAccumulatorMap() {
    	Date minDate = Date.newInstance(2013, 6, 15);
    	Date maxDate = Date.newInstance(2014, 3, 12);
    	
    	map<string, integer> mapTypeToCount = new map<string, integer>();
    	mapTypeToCount.put('Month', 0);
    	mapTypeToCount.put('Quarter', 0);
    	mapTypeToCount.put('Year', 0);
    	
    	list<pse__Time_Period__c> allPeriods = new list<pse__Time_Period__c>();
    	allPeriods.add(new pse__Time_Period__c(name='Nov FY2014', pse__Start_Date__c=Date.newInstance(2013,11,1), pse__End_Date__c=Date.newInstance(2013,11,30), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Dec FY2014', pse__Start_Date__c=Date.newInstance(2013,12,1), pse__End_Date__c=Date.newInstance(2013,12,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Jan FY2014', pse__Start_Date__c=Date.newInstance(2014,1,1), pse__End_Date__c=Date.newInstance(2014,1,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Feb FY2014', pse__Start_Date__c=Date.newInstance(2014,2,1), pse__End_Date__c=Date.newInstance(2014,2,28), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Mar FY2014', pse__Start_Date__c=Date.newInstance(2014,3,1), pse__End_Date__c=Date.newInstance(2014,3,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Apr FY2014', pse__Start_Date__c=Date.newInstance(2014,4,1), pse__End_Date__c=Date.newInstance(2014,4,30), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='May FY2014', pse__Start_Date__c=Date.newInstance(2014,5,1), pse__End_Date__c=Date.newInstance(2014,5,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Jun FY2014', pse__Start_Date__c=Date.newInstance(2014,6,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Jul FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2014,7,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Aug FY2015', pse__Start_Date__c=Date.newInstance(2014,8,1), pse__End_Date__c=Date.newInstance(2014,8,31), pse__Type__c='Month'));
    	allPeriods.add(new pse__Time_Period__c(name='Sep FY2015', pse__Start_Date__c=Date.newInstance(2014,9,1), pse__End_Date__c=Date.newInstance(2014,9,30), pse__Type__c='Month'));
    	
     	allPeriods.add(new pse__Time_Period__c(name='Q2 FY2014', pse__Start_Date__c=Date.newInstance(2013,10,1), pse__End_Date__c=Date.newInstance(2013,12,31), pse__Type__c='Quarter'));
    	allPeriods.add(new pse__Time_Period__c(name='Q3 FY2014', pse__Start_Date__c=Date.newInstance(2014,1,1), pse__End_Date__c=Date.newInstance(2014,3,31), pse__Type__c='Quarter'));
     	allPeriods.add(new pse__Time_Period__c(name='Q4 FY2014', pse__Start_Date__c=Date.newInstance(2014,4,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Quarter'));
    	allPeriods.add(new pse__Time_Period__c(name='Q1 FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2014,9,30), pse__Type__c='Quarter'));
    	
    	allPeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	allPeriods.add(new pse__Time_Period__c(name='FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2015,6,30), pse__Type__c='Year'));
    	
    	Test.startTest();
    	
    	map<pse__Time_Period__c,double> testMap = ComplimentaryServiceUtils.buildAccumulatorMap(allPeriods);
    	system.assertEquals(testMap.values().size(), 17, 'Number of values in the set failed expectations');
    	
    	for (pse__Time_Period__c key : testMap.keySet()) {
	    	system.assertEquals(testMap.get(key), 0, key.pse__type__c + ' failed expectations');
    	}
    	
		Test.stopTest();
    }
    
    static testMethod void testGetTimePeriodMonth() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
    	timePeriods.add(new pse__Time_Period__c(name='Apr FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,4,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='May FY2013', pse__Start_Date__c=Date.newInstance(2013,5,1), pse__End_Date__c=Date.newInstance(2013,5,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jun FY2013', pse__Start_Date__c=Date.newInstance(2013,6,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jul FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,7,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Aug FY2014', pse__Start_Date__c=Date.newInstance(2013,8,1), pse__End_Date__c=Date.newInstance(2013,8,31), pse__Type__c='Month'));
    	
    	Test.startTest();
    	
    	Date minDate = Date.newInstance(2013,7,13);
    	pse__Time_Period__c testValue = ComplimentaryServiceUtils.getTimePeriodFromDate(minDate, timePeriods);
    	
    	system.assertEquals(testValue.name, 'Jul FY2014', 'Month TimePeriod failed expectations');
		
		Test.stopTest();
    }
    
    static testMethod void testGetTimePeriodQuarter() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
    	timePeriods.add(new pse__Time_Period__c(name='Q4 FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q1 FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,9,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q2 FY2014', pse__Start_Date__c=Date.newInstance(2013,10,1), pse__End_Date__c=Date.newInstance(2013,12,31), pse__Type__c='Quarter'));
    	
    	Test.startTest();
    	
    	Date minDate = Date.newInstance(2013,7,13);
    	pse__Time_Period__c testValue = ComplimentaryServiceUtils.getTimePeriodFromDate(minDate, timePeriods);
    	
    	system.assertEquals(testValue.name, 'Q1 FY2014', 'Quarter TimePeriod failed expectations');
		
		Test.stopTest();
    }
    
    static testMethod void testGetTimePeriodYear() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
    	timePeriods.add(new pse__Time_Period__c(name='FY2012', pse__Start_Date__c=Date.newInstance(2011,7,1), pse__End_Date__c=Date.newInstance(2012,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2013', pse__Start_Date__c=Date.newInstance(2012,7,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	
    	Test.startTest();
    	
    	Date minDate = Date.newInstance(2013,7,13);
    	pse__Time_Period__c testValue = ComplimentaryServiceUtils.getTimePeriodFromDate(minDate, timePeriods);
    	
    	system.assertEquals(testValue.name, 'FY2014', 'Year TimePeriod failed expectations');
		
		Test.stopTest();
    }
    
    static testMethod void testTimePeriodSegregationMonth() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
    	timePeriods.add(new pse__Time_Period__c(name='Apr FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,4,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='May FY2013', pse__Start_Date__c=Date.newInstance(2013,5,1), pse__End_Date__c=Date.newInstance(2013,5,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jun FY2013', pse__Start_Date__c=Date.newInstance(2013,6,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jul FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,7,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Aug FY2014', pse__Start_Date__c=Date.newInstance(2013,8,1), pse__End_Date__c=Date.newInstance(2013,8,31), pse__Type__c='Month'));
     	timePeriods.add(new pse__Time_Period__c(name='Q4 FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q1 FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,9,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2013', pse__Start_Date__c=Date.newInstance(2012,7,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	
    	Test.startTest();

    	list<pse__Time_Period__c> subset = ComplimentaryServiceUtils.getTimePeriodsByType(timePeriods, 'Month');

		system.assertEquals(subset.size(), 5, 'Subset size failed expectation');
		
    	for (pse__Time_Period__c tp : subset) {
	    	system.assertEquals(tp.pse__type__c, 'Month', 'TimePeriod type failed expectations');
    	}
		
		Test.stopTest();
    }
    
    static testMethod void testTimePeriodSegregationQuarter() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
    	timePeriods.add(new pse__Time_Period__c(name='Apr FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,4,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='May FY2013', pse__Start_Date__c=Date.newInstance(2013,5,1), pse__End_Date__c=Date.newInstance(2013,5,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jun FY2013', pse__Start_Date__c=Date.newInstance(2013,6,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jul FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,7,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Aug FY2014', pse__Start_Date__c=Date.newInstance(2013,8,1), pse__End_Date__c=Date.newInstance(2013,8,31), pse__Type__c='Month'));
     	timePeriods.add(new pse__Time_Period__c(name='Q4 FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q1 FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,9,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2013', pse__Start_Date__c=Date.newInstance(2012,7,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	
    	Test.startTest();

    	list<pse__Time_Period__c> subset = ComplimentaryServiceUtils.getTimePeriodsByType(timePeriods, 'Quarter');

		system.assertEquals(subset.size(), 2, 'Subset size failed expectation');
		
    	for (pse__Time_Period__c tp : subset) {
	    	system.assertEquals(tp.pse__type__c, 'Quarter', 'TimePeriod type failed expectations');
    	}
		
		Test.stopTest();
    }
    
    static testMethod void testTimePeriodSegregationYear() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
    	timePeriods.add(new pse__Time_Period__c(name='Apr FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,4,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='May FY2013', pse__Start_Date__c=Date.newInstance(2013,5,1), pse__End_Date__c=Date.newInstance(2013,5,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jun FY2013', pse__Start_Date__c=Date.newInstance(2013,6,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Jul FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,7,31), pse__Type__c='Month'));
    	timePeriods.add(new pse__Time_Period__c(name='Aug FY2014', pse__Start_Date__c=Date.newInstance(2013,8,1), pse__End_Date__c=Date.newInstance(2013,8,31), pse__Type__c='Month'));
     	timePeriods.add(new pse__Time_Period__c(name='Q4 FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q1 FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,9,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2013', pse__Start_Date__c=Date.newInstance(2012,7,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	
    	Test.startTest();

    	list<pse__Time_Period__c> subset = ComplimentaryServiceUtils.getTimePeriodsByType(timePeriods, 'Year');

		system.assertEquals(subset.size(), 2, 'Subset size failed expectation');
		
    	for (pse__Time_Period__c tp : subset) {
	    	system.assertEquals(tp.pse__type__c, 'Year', 'TimePeriod type failed expectations');
    	}
		
		Test.stopTest();
    }
    
    static testMethod void testTimePeriodCoarseMatchQuarter() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
     	timePeriods.add(new pse__Time_Period__c(name='Q4 FY2013', pse__Start_Date__c=Date.newInstance(2013,4,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q1 FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2013,9,30), pse__Type__c='Quarter'));
    	timePeriods.add(new pse__Time_Period__c(name='Q2 FY2014', pse__Start_Date__c=Date.newInstance(2013,10,1), pse__End_Date__c=Date.newInstance(2013,12,31), pse__Type__c='Quarter'));

    	pse__Time_Period__c granularPeriod = new pse__Time_Period__c(name='Nov FY2014', pse__Start_Date__c=Date.newInstance(2013,11,1), pse__End_Date__c=Date.newInstance(2013,11,30), pse__Type__c='Month');
    	
    	Test.startTest();

    	pse__Time_Period__c testValue = ComplimentaryServiceUtils.getContainingTimePeriod(granularPeriod, timePeriods);

		system.assertEquals(testValue.name, 'Q2 FY2014', 'Coarse Quarter match failed expectation');
		
		Test.stopTest();
    }
    
    static testMethod void testTimePeriodCoarseMatchYear() {
    	list<pse__Time_Period__c> timePeriods = new list<pse__Time_Period__c>();
     	timePeriods.add(new pse__Time_Period__c(name='FY2013', pse__Start_Date__c=Date.newInstance(2012,7,1), pse__End_Date__c=Date.newInstance(2013,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2014', pse__Start_Date__c=Date.newInstance(2013,7,1), pse__End_Date__c=Date.newInstance(2014,6,30), pse__Type__c='Year'));
    	timePeriods.add(new pse__Time_Period__c(name='FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2015,6,30), pse__Type__c='Year'));

    	pse__Time_Period__c granularPeriod = new pse__Time_Period__c(name='Q1 FY2015', pse__Start_Date__c=Date.newInstance(2014,7,1), pse__End_Date__c=Date.newInstance(2014,9,30), pse__Type__c='Quarter');
    	
    	Test.startTest();

    	pse__Time_Period__c testValue = ComplimentaryServiceUtils.getContainingTimePeriod(granularPeriod, timePeriods);

		system.assertEquals(testValue.name, 'FY2015', 'Coarse Year match failed expectation');
		
		Test.stopTest();
    }
    
}