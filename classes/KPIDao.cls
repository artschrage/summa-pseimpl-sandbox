public with sharing class KPIDao {
    
    public List<KPI__c> getActiveKPIs(String timePeriodType) {
        return [select Id, Name, Report_Class_Name__c, Period_Type__c from KPI__c 
                where Report_Class_Name__c != null and
                Period_Type__c = :timePeriodType and
                Is_Active__c = true];
    }

    public List<KPI_Value__c> getKPIValues(List<KPI__c> kpis, pse__Time_Period__c timePeriod) {
        Set<id> kpiIds = new Set<Id>();
        for (KPI__c kpi : kpis) {
            kpiIds.add(kpi.id);
        }
        return [select Id, Actual__c, Target__c, KPI__c, KPI__r.name, 
                KPI__r.Report_Class_Name__c, KPI__r.format__c, Time_Period__c, 
                Time_Period__r.pse__Start_Date__c, 
                Time_Period__r.pse__End_Date__c,
                Time_Period__r.Name from KPI_Value__c 
                where KPI__c in :kpiIds and Time_Period__c = :timePeriod.Id];
    }
    
    public void saveValues(List<KPI_Value__c> values) {
        if (values != null && !values.isEmpty()) {
            upsert values;
        }
    }
}