public class TestKPIBase {

	 public static void stageTestData() {
    	KPI__c kpi = new KPI__c(Name='Complimentary Services',Category__c='Financial',Is_Active__c=True,Report_Class_Name__c='KPIComplimentaryServiceReportLogic');
    	List<KPI__c> kpis = new List<KPI__c>();
    	kpis.add(kpi);
    	kpis.add(new KPI__c(Name='New Client Ratio', Category__c='Financial', 
                        Period_Type__c='Quarter', Is_Active__c=True,
                        Report_Class_Name__c='KPINewAccountRatioReportLogic'));
    	kpis.add(new KPI__c(Name='Account Expansion',Category__c='Financial',
                        Period_Type__c='Quarter', Is_Active__c=True, 
                        Report_Class_Name__c='KPIAccountExpandRatioReportLogic'));
    	kpis.add(new KPI__c(Name='Total Gross Margin',Category__c='Financial',
                        Period_Type__c='Quarter', Is_Active__c=True,
                        Report_Class_Name__c='KPITotalGrossMarginReportLogic'));
    	kpis.add(new KPI__c(Name='Hit Ratio Amount YTD',Category__c='Financial',
                        Period_Type__c='Year', Is_Active__c=True,
                        Report_Class_Name__c='KPIHitRatioAmountLogic'));
    	insert kpis;

		Date now = Date.today(); 
		decimal thisQuarterMonth = Decimal.valueOf(now.month());
		thisQuarterMonth = (math.ceil(thisQuarterMonth.divide(3,2))*3)-2;

		Date startCurrentQuarter = Date.newInstance(now.year(), Integer.valueOf(thisQuarterMonth), 1);
		Date startPriorQuarter = startCurrentQuarter.addMonths(-3);
		Date startNexQuarter = startCurrentQuarter.addMonths(3);
		
		system.debug('startCurrentQuarter = ' + startCurrentQuarter);
		system.debug('startPriorQuarter = ' + startPriorQuarter);
		system.debug('startNexQuarter = ' + startNexQuarter);
		
		List<pse__Time_Period__c> periods = new List<pse__time_period__c>();
		periods.add(new pse__Time_Period__c(Name='This Quarter', pse__Type__c='Quarter', 
				pse__Start_Date__c=startCurrentQuarter, 
				pse__End_Date__c=startNexQuarter.addDays(-1)));
		periods.add(new pse__Time_Period__c(Name='Last Quarter', pse__Type__c='Quarter', 
				pse__Start_Date__c=startPriorQuarter, 
				pse__End_Date__c=startCurrentQuarter.addDays(-1)));
		periods.add(new pse__Time_Period__c(Name='Next Quarter', pse__Type__c='Quarter', 
				pse__Start_Date__c=startNexQuarter, 
				pse__End_Date__c=startNexQuarter.addMonths(3).addDays(-1)));
		periods.add(new pse__Time_Period__c(Name='This Year', pse__Type__c='Year', 
				pse__Start_Date__c=Date.newInstance(now.year(), 1, 1), 
				pse__End_Date__c=Date.newInstance(now.year(), 12, 31)));
		insert periods;
		
		List<KPI_Value__c> values = new List<KPI_Value__c>();
		for (pse__Time_Period__c tp : periods) {
			values.add(new KPI_Value__c(KPI__c=kpi.Id,Time_Period__c=tp.Id));
		}
		insert values;
    }
}