public with sharing class ComplimentaryServiceDao {

    public static Date getEarliestProjectStartDate() {
        Date returnValue = null;
        
        AggregateResult[] result = [select min(pse__Start_Date__c) start_date from pse__Proj__c where (pse__Is_Active__c = TRUE or Date_Became_Inactive__c = LAST_N_DAYS:180) and pse__Project_Type__c not in ('Internal','Paid Time Off')];
        if (result != null && result.size() > 0) {
            returnValue = (Date)result[0].get('start_date');
        }
            
        return returnValue;
    }
    
    public static Date getLatestProjectEndDate() {
        Date returnValue = null;
        
        AggregateResult[] result = [select max(pse__End_Date__c) end_date from pse__Proj__c where (pse__Is_Active__c = TRUE or Date_Became_Inactive__c = LAST_N_DAYS:180) and pse__Project_Type__c not in ('Internal','Paid Time Off')];
        if (result != null && result.size() > 0) {
            returnValue = (Date)result[0].get('end_date');
        }
        
        return returnValue;
    }
    
    /**
     *  Returns a list of pse__Proj__c that are currently active or inactive within the past 180 days.
     *  The pse__Proj__c records queried contain the following fields:
     *      Id
     *      Name 
     *      pse__Start_Date__c
     *      pse__End_Date__c
     *      pse__Is_Active__c
     *      Date_Became_Inactive__c
     */
    public static List<pse__Proj__c> getAllProjectsForAnalysis() {
        List<pse__Proj__c> projects = [
            select  p.name, p.pse__Start_Date__c, p.pse__Is_Active__c, p.pse__End_Date__c, p.Id, p.Date_Became_Inactive__c 
            from    pse__Proj__c p 
            where   p.pse__Project_Type__c not in ('Internal','Paid Time Off')
            and		(	pse__Is_Active__c = TRUE
            		or	Date_Became_Inactive__c = LAST_N_DAYS:180)];
            
        return projects;        
    }
    
    /**
     *  Returns a map of pse__Expense__c records by project id for projects as specified in the projectIds argument.
     *  The pse__Expense__c records queried contain the following fields:
     *      Name
     *      pse__Billable__c
     *      pse__Status__c
     *      pse__Expense_Date__c
     *      pse__Amount__c
     *      pse__Project__c
     */
    public static Map<Id, List<pse__Expense__c>> getProjectExpenses(Set<Id> projectIds) {
    	List<pse__Expense__c> expenses = [
			select  name, pse__Billable__c, pse__Status__c, pse__Expense_Date__c, pse__Amount__c, pse__Project__c
            from    pse__Expense__c 
            where   pse__Billable__c = FALSE
            and     pse__Status__c = 'Approved'
            and		pse__Project__c in :projectIds];
    	Map<Id, List<pse__Expense__c>> returnValue = new Map<Id, List<pse__Expense__c>>();
    	
    	List<pse__Expense__c> newList;
    	for (pse__Expense__c expense : expenses) {
    		if (returnValue.containsKey(expense.pse__Project__c)) {
    			returnValue.get(expense.pse__Project__c).add(expense);
    		} else {
    			newList = new List<pse__Expense__c>();
    			newList.add(expense);
    			returnValue.put(expense.pse__Project__c, newList);
    			newList = null;
    		}
    	}
    	
    	return returnValue;
    }
    
    /**
     *  Returns a map of pse__Timecard__c records by project id for projects as specified in the projectIds argument.
     *  The pse__Timecard__c records queried contain the following fields:
     *      Id
     *      Name
     *      pse__Start_Date__c
     *      pse__End_Date__c
     *      pse__Total_Hours__c
     *      pse__Total_Cost__c
     *		pse__Project__c
     */
    public static Map<Id, List<pse__Timecard__c>> getProjectTimecards(Set<Id> projectIds) {
    	List<pse__Timecard__c> timecards = [
			select  id, name, pse__Start_Date__c, pse__End_Date__c, pse__Total_Hours__c, pse__Total_Cost__c, pse__Project__c 
			from    pse__Timecard__c
			where   Is_Complimentary_Service__c = TRUE
			and		pse__Project__c in :projectIds];
    	Map<Id, List<pse__Timecard__c>> returnValue = new Map<Id, List<pse__Timecard__c>>();
    	
    	List<pse__Timecard__c> newList;
    	for (pse__Timecard__c timecard : timecards) {
    		if (returnValue.containsKey(timecard.pse__Project__c)) {
    			returnValue.get(timecard.pse__Project__c).add(timecard);
    		} else {
    			newList = new List<pse__Timecard__c>();
    			newList.add(timecard);
    			returnValue.put(timecard.pse__Project__c, newList);
    			newList = null;
    		}
    	}
    	
    	return returnValue;
    }
    
    /**
     *  Returns a map of Complimentary_Service__c records by project id for projects as specified in the projectIds argument.
     *  The Complimentary_Service__c records queried contain the following fields:
     *      Id
     *      Name
     *      Time_Period__c
     *      Project__c
     *      Number_of_Hours__c
     *      Cost_of_Service__c
     *      Cost_of_Expenses__c
     *      Total_Cost__c
     */
    public static List<Complimentary_Service__c> getProjectComplimentaryServices(Set<Id> projectIds) {
    	List<Complimentary_Service__c> returnValue = [
			select  Id, Name, Time_Period__c, Number_of_Hours__c, Cost_of_Service__c, Cost_of_Expenses__c, Total_Cost__c, Project__c 
			from    Complimentary_Service__c
			where	Project__c in :projectIds];

    	return returnValue;
    }

    public static List<pse__Time_Period__c> getAnalysisTimePeriods(Date minDate, Date maxDate) {
        List<pse__Time_Period__c> returnValue = null;
        if (minDate != null && maxDate != null) {
            returnValue = [
                select  id, name, pse__start_date__c, pse__end_date__c, pse__type__c 
                from    pse__time_period__c 
                where   pse__type__c in ('Month','Quarter','Year')
                and     (   (   pse__start_date__c <= :minDate
                            and pse__end_date__c >= :minDate)
                        or  (   pse__start_date__c >= :minDate
                            and pse__end_date__c <= :maxDate)
                        or  (   pse__start_date__c <= :maxDate
                            and pse__end_date__c >= :maxDate)
                        )];
        } else {
            returnValue = new List<pse__Time_Period__c>();
        }
        
        return returnValue;
    }
    
    public static void storeAnalyses(List<Complimentary_Service__c> oldComplimentaryServices, List<Complimentary_Service__c> newComplimentaryServices) {
        SavePoint sp = null;
        
        if (oldComplimentaryServices != null && oldComplimentaryServices.size() > 0) {
            sp = Database.setSavePoint();
            system.debug('Creating SavePoint for ['+ oldComplimentaryServices.size() +'] existing Complimentary Service records.');
        }
        if (newComplimentaryServices != null && newComplimentaryServices.size() > 0) {
            try {
            	if (oldComplimentaryServices != null) {
		            system.debug('Deleting ['+ oldComplimentaryServices.size() +'] existing Complimentary Service records.');
	                delete oldComplimentaryServices;
            	}
	            system.debug('Upserting ['+ newComplimentaryServices.size() +'] new Complimentary Service records.');
                upsert newComplimentaryServices;
            } catch (Exception xcpn) {
                if (sp != null) {
		            system.debug('Rolling back to SavePoint due to ['+ xcpn.getMessage() +'].');
                    Database.rollback(sp);
                }
                throw xcpn;
            }
        }
    }
}