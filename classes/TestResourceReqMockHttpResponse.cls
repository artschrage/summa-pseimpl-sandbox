@isTest
global class TestResourceReqMockHttpResponse implements HttpCalloutMock
{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
       	HttpResponse res = new HttpResponse();
        if (req.getMethod()=='POST'){
        	// Create a fake initial authorization response
        	res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        	res.setBody('{"access_token" : "ya29.UAAzMxBfAYVnGyAAAACxT5zUTGpeupVG4PsMwFyMqY_IeO0nhAbdBNmvtDXolw", "token_type" : "Bearer", "expires_in" : 3600 }');
    	} else {
        	// Create a fake spreadsheet content response
            // Retrieve the xml response from static resources
            StaticResource[] srs = [Select body From StaticResource Where Name='TestResourceReqFromEstController_sample_spreadsheet_xml' Limit 1];
            String returnXml = srs[0].body.toString();
            System.debug('*** returnXmlSample: ' + returnXml.substring(0, 100));
            res.setBody(returnXml);
    	}
       	res.setStatusCode(200);
       	return res;
    }
}