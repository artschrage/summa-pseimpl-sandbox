/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class TestComplimentaryServiceDao {
	
	private static pse__Time_Period__c janfy14;
	private static pse__Time_Period__c q3fy14;
	private static pse__Time_Period__c fy14;
	
	private static pse__Region__c regionCorporate;
	private static pse__Practice__c practiceCore;
	private static Contact johnSmail;
	private static Account account;
	private static List<pse__Time_Period__c> timePeriodDb = new List<pse__Time_Period__c>();
		
//	static {
//		janfy14 = [select id, name, pse__start_date__c, pse__end_date__c, pse__type__c from pse__time_period__c where name = 'Jan FY2014'];
//		q3fy14 = [select id, name, pse__start_date__c, pse__end_date__c, pse__type__c from pse__time_period__c where name = 'Q3 FY2014'];
//		fy14 = [select id, name, pse__start_date__c, pse__end_date__c, pse__type__c from pse__time_period__c where name = 'FY2014'];
//	}

    static testMethod void testTimePeriodsWithinQuarter() {
    	stagePrerequisites();
    	
    	Date minDate = Date.newInstance(2014, 1, 15);
    	Date maxDate = Date.newInstance(2014, 3, 12);
    	
    	map<string, integer> mapTypeToCount = new map<string, integer>();
    	mapTypeToCount.put('Month', 0);
    	mapTypeToCount.put('Quarter', 0);
    	mapTypeToCount.put('Year', 0);
    	
    	Test.startTest();
    	
    	list<pse__Time_Period__c> timePeriods = ComplimentaryServiceDao.getAnalysisTimePeriods(minDate, maxDate);
    	for (pse__Time_Period__c tp : timePeriods) {
    		mapTypeToCount.put(tp.pse__type__c,  mapTypeToCount.containsKey(tp.pse__type__c) ? mapTypeToCount.get(tp.pse__type__c)+1 : 1);
    	}
    	
    	system.assertEquals(3, mapTypeToCount.get('Month'), 'Month failed expectations');
    	system.assertEquals(1, mapTypeToCount.get('Quarter'), 'Quarter failed expectations');
    	system.assertEquals(1, mapTypeToCount.get('Year'), 'Year failed expectations');
		
		Test.stopTest();
    }
    
    static testMethod void testTimePeriodsBeyondQuarter() {
    	stagePrerequisites();

    	Date minDate = Date.newInstance(2013, 6, 15);
    	Date maxDate = Date.newInstance(2014, 3, 12);
    	
    	map<string, integer> mapTypeToCount = new map<string, integer>();
    	mapTypeToCount.put('Month', 0);
    	mapTypeToCount.put('Quarter', 0);
    	mapTypeToCount.put('Year', 0);
    	
    	Test.startTest();
    	
    	list<pse__Time_Period__c> timePeriods = ComplimentaryServiceDao.getAnalysisTimePeriods(minDate, maxDate);
    	for (pse__Time_Period__c tp : timePeriods) {
    		mapTypeToCount.put(tp.pse__type__c, mapTypeToCount.containsKey(tp.pse__type__c) ? mapTypeToCount.get(tp.pse__type__c)+1 : 1);
    	}
    	
    	system.assertEquals(10, mapTypeToCount.get('Month'), 'Month failed expectations');
    	system.assertEquals(4, mapTypeToCount.get('Quarter'), 'Quarter failed expectations');
    	system.assertEquals(2, mapTypeToCount.get('Year'), 'Year failed expectations');
		
		Test.stopTest();
    }

	static testMethod void testGetProjectsForAnalysis() { 
		stageFullProject();
		
    	Test.startTest();
		try {
			list<pse__Proj__c> projects = ComplimentaryServiceDao.getAllProjectsForAnalysis();
			system.assert(projects != null, 'Null returned for projects instead of list');
		} catch (Exception xcpn) {
			system.assert(false, xcpn.getMessage());
		} 
		Test.stopTest();
	}
	
	static testMethod void testEarliestProjectStartDate() {
		stageFullProject();
		
    	Test.startTest();
		Date testValue = ComplimentaryServiceDao.getEarliestProjectStartDate();
		system.assert(testValue != null, 'Null returned for earliest project date');
		
		Test.stopTest();
	}
    
	static testMethod void testLatestProjectEndDate() {
		stageFullProject();
		
    	Test.startTest();
		Date testValue = ComplimentaryServiceDao.getLatestProjectEndDate();
		system.assert(testValue != null, 'Null returned for latest project date');
		
		Test.stopTest();
	}
    
    static testMethod void testStoreAnalysesPriorRecords() {
    	pse__Proj__c project = stageFullProject();
		
    	list<Complimentary_Service__c> newServices = getNewComplimentaryServiceRecords(project);
    	list<Complimentary_Service__c> oldServices = getOldComplimentaryServiceRecords(project);
    	
    	Test.startTest();
		try {
			ComplimentaryServiceDao.storeAnalyses(oldServices, newServices);
		} catch (Exception xcpn) {
			system.assert(false, xcpn.getMessage());
		}
		
		Test.stopTest();
    }

    static testMethod void testStoreAnalysesNoPriorRecords() {
    	pse__Proj__c project = stageFullProject();
		
    	list<Complimentary_Service__c> newServices = getNewComplimentaryServiceRecords(project);
    	
    	Test.startTest();
		try {
			ComplimentaryServiceDao.storeAnalyses(new List<Complimentary_Service__c>(), newServices);
		} catch (Exception xcpn) {
			system.assert(false, xcpn.getMessage());
		}
		
		Test.stopTest();
    }

	private static list<Complimentary_Service__c> getOldComplimentaryServiceRecords(pse__Proj__c project) {
		return [select id, name, Project__c, Time_Period__c, Number_of_Hours__c, Cost_of_Service__c, Cost_of_Expenses__c from Complimentary_Service__c where Project__c = :project.Id];
	}
	
	private static list<Complimentary_Service__c> getNewComplimentaryServiceRecords(pse__Proj__c project) {
		list<Complimentary_Service__c> services = new list<Complimentary_Service__c>();
		Complimentary_Service__c service = new Complimentary_Service__c();
		
		service.Project__c = project.Id;
		service.Time_Period__c = janfy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		service = new Complimentary_Service__c();
		service.Project__c = project.Id;
		service.Time_Period__c = q3fy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		service = new Complimentary_Service__c();
		service.Project__c = project.Id;
		service.Time_Period__c = fy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);
		
		return services;
	}
	
	private static pse__Proj__c stageFullProject() {
		stagePrerequisites();
			    /**
	     *	Setup a single project with time and expense info to run the complimentary service analysis
	     *	against and verify the output specifically. The Id of the created project is returned to
	     *	facilitate testMethod usage and mocking the query for project list.
	     */
    	//project
		pse__Proj__c project = new pse__Proj__c();
	    project.Name = 'ComplimentaryServiceBso TMNonBillableOneMonthProject';
	    project.pse__Account__c = account.Id;
	    project.pse__Is_Active__c = true; 
	    project.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	    project.pse__Billing_Type__c = 'Time and Materials';
	    project.pse__Start_Date__c = Date.newInstance(2014,1,1);
	    project.pse__End_Date__c = Date.newInstance(2014,1,31);
	    project.Date_Became_Inactive__c = null;
	    project.pse__Project_Manager__c = johnSmail.Id;
	    project.Delivery_Engagement_Manager__c = johnSmail.Id;
	    project.pse__Region__c = regionCorporate.Id;
	    project.pse__Practice__c = practiceCore.Id;
	    insert project;
	    
    	//schedule
		pse__Schedule__c schedule = new pse__Schedule__c();
		schedule.pse__Start_Date__c = project.pse__Start_Date__c;
		schedule.pse__End_Date__c = project.pse__End_Date__c;
		schedule.pse__Sunday_Hours__c = 0;
		schedule.pse__Monday_Hours__c = 8;
		schedule.pse__Tuesday_Hours__c = 8;
		schedule.pse__Wednesday_Hours__c = 8;
		schedule.pse__Thursday_Hours__c = 8;
		schedule.pse__Friday_Hours__c = 8;
		schedule.pse__Saturday_Hours__c = 0;
	    insert schedule;
	    
    	//assignment(s)
	    pse__Assignment__c assignment = new pse__Assignment__c();
		assignment.name = project.name;
		assignment.pse__Project__c = project.Id;
		assignment.pse__Resource__c = johnSmail.Id;
		assignment.pse__Schedule__c = schedule.Id;
		assignment.pse__Is_Billable__c = false;
		assignment.pse__Bill_Rate__c = 0;
		insert assignment;

		list<Complimentary_Service__c> services = new list<Complimentary_Service__c>();
		Complimentary_Service__c service = new Complimentary_Service__c();
		
		service.Project__c = project.Id;
		service.Time_Period__c = janfy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		service = new Complimentary_Service__c();
		service.Project__c = project.Id;
		service.Time_Period__c = q3fy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		service = new Complimentary_Service__c();
		service.Project__c = project.Id;
		service.Time_Period__c = fy14.Id;
		service.Number_of_Hours__c = 11.5;
		service.Cost_of_Service__c = 1265;
		service.Cost_of_Expenses__c = 0;
		services.add(service);

		insert services;
		
	    return [
	    	select  p.name, p.pse__Start_Date__c, p.pse__Is_Active__c, p.pse__End_Date__c, p.Id, p.Date_Became_Inactive__c 
	        from    pse__Proj__c p 
	        where   Id = :project.Id];
		
	}
	
	private static void stagePrerequisites() {
    	//region
    	regionCorporate = new pse__Region__c(Name='Corporate', CurrencyIsoCode='USD');
    	insert regionCorporate;
    	
    	//practice
    	practiceCore = new pse__Practice__c(Name='Core', CurrencyIsoCode='USD');
    	insert practiceCore;
    	
    	//contact
    	String currentUserId = UserInfo.getUserId();
    	johnSmail = new Contact(FirstName='John', LastName='Smail', CurrencyIsoCode='USD', pse__Is_Resource__c=TRUE, pse__Is_Resource_Active__c=TRUE, pse__Region__c=regionCorporate.Id, pse__Practice__c=practiceCore.Id, pse__Salesforce_User__c=currentUserId);
    	insert johnSmail;
    	
    	pse__Permission_Control__c permission = new pse__Permission_Control__c(
	    	pse__Billing__c=TRUE,
			pse__Cascading_Permission__c=TRUE,
			pse__Expense_Entry__c=TRUE,
			pse__Expense_Ops_Edit__c=TRUE,
			pse__Forecast_Edit__c=TRUE,
			pse__Forecast_View__c=TRUE,
			pse__Invoicing__c=TRUE,
			pse__Region__c=regionCorporate.Id,
			pse__Resource_Request_Entry__c=TRUE,
			pse__Staffing__c=TRUE,
			pse__Timecard_Entry__c=TRUE,
			pse__Timecard_Ops_Edit__c=TRUE,
			pse__User__c=currentUserId);
		insert permission;
		    	
    	//account
    	account = new Account(Name='TestComplimentaryServiceBso Account A', CurrencyIsoCode='USD');
		insert account;
		
		//time periods
		timePeriodDb.add(new pse__Time_Period__c(Name='Jun FY2013', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 6, 1), pse__End_Date__c=Date.newInstance(2013, 6, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Jul FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 7, 1), pse__End_Date__c=Date.newInstance(2013, 7, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Aug FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 8, 1), pse__End_Date__c=Date.newInstance(2013, 8, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Sep FY2013', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 9, 1), pse__End_Date__c=Date.newInstance(2013, 9, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Oct FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 10, 1), pse__End_Date__c=Date.newInstance(2013, 10, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Nov FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 11, 1), pse__End_Date__c=Date.newInstance(2013, 11, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Dec FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2013, 12, 1), pse__End_Date__c=Date.newInstance(2013, 12, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Jan FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 1, 1), pse__End_Date__c=Date.newInstance(2014, 1, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Feb FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 2, 1), pse__End_Date__c=Date.newInstance(2014, 2, 28)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Mar FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 3, 1), pse__End_Date__c=Date.newInstance(2014, 3, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Apr FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 4, 1), pse__End_Date__c=Date.newInstance(2014, 4, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='May FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 5, 1), pse__End_Date__c=Date.newInstance(2014, 5, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Jun FY2014', pse__Type__c='Month', pse__Start_Date__c=Date.newInstance(2014, 6, 1), pse__End_Date__c=Date.newInstance(2014, 6, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q4 FY2013', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2013, 4, 1), pse__End_Date__c=Date.newInstance(2013, 6, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q1 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2013, 7, 1), pse__End_Date__c=Date.newInstance(2013, 9, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q2 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2013, 10, 1), pse__End_Date__c=Date.newInstance(2013, 12, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q3 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2014, 1, 1), pse__End_Date__c=Date.newInstance(2014, 3, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q4 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2014, 4, 1), pse__End_Date__c=Date.newInstance(2014, 6, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='FY2013', pse__Type__c='Year', pse__Start_Date__c=Date.newInstance(2012, 7, 1), pse__End_Date__c=Date.newInstance(2013, 6, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='FY2014', pse__Type__c='Year', pse__Start_Date__c=Date.newInstance(2013, 7, 1), pse__End_Date__c=Date.newInstance(2014, 6, 30)));
		insert timePeriodDb;
		
		janfy14 = timePeriodDb.get(7);
		q3fy14 = timePeriodDb.get(16);
		fy14 = timePeriodDb.get(19);
		
	}

}