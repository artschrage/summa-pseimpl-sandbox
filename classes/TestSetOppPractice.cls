@isTest (SeeAllData = true)

private class TestSetOppPractice {
      private static User thisUser;
      private static pse__Region__c testRegion;
      private static Account testAccount;
      private static Opportunity testOppCRM;
      private static pse__Proj__c testProject;
      private static pse__Practice__c testPracticeCRM;

      static {
            stageTestData();
      }

      private static void stageTestData(){
         thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
         testRegion = [Select Id from pse__Region__c where Name = 'Corporate'];
         testPracticeCRM = [Select Id from pse__Practice__c where Name = 'Salesforce'];
         //testPracticeCRM = [Select Id from pse__Practice__c where Name = 'CRM'];
         testAccount = new Account(
            Name='TestAcct',
            CurrencyIsoCode='USD');
            insert testAccount;
            System.assertEquals('TestAcct', testAccount.Name);
         testProject = new pse__Proj__c(
            Name='TestProjectOppTrigger',
            pse__Region__c= testRegion.Id);
            insert testProject;
            System.assertEquals('TestProjectOppTrigger', testProject.Name); 
      }

	static testmethod void setPracticeCRM(){
      
            Test.startTest();
            System.runAs ( thisUser ) {
                  testOppCRM = new Opportunity(
                  Name='Test Opportunity Practice CRM',
                  AccountId= testAccount.Id,
                  StageName= 'Create',
                  ForecastCategoryName= 'Omitted',
                  CloseDate= Date.today()+7,
                  Start_Date__c= Date.today()+8,
                  Solution_Area1__c= 'SFDC - Sales Cloud',
                  pse__Region__c= testRegion.Id);
                  
                  insert testOppCRM;
                  System.assertEquals('Create', testOppCRM.StageName);
            }
                           
            Test.stopTest();
      }

	static testmethod void setPracticeSMM(){
              
            Test.startTest();
            System.runAs ( thisUser ) {
                  Opportunity testOppSMM = new Opportunity(
                  Name='Test Opportunity Practice SMM',
                  AccountId= testAccount.Id,
                  StageName= 'Create',
                  ForecastCategoryName= 'Omitted',
                  CloseDate= Date.today()+7,
                  Start_Date__c= Date.today()+8,
                  Solution_Area1__c= 'SMM - Sales Cloud',
                  pse__Region__c= testRegion.Id);
                  insert testOppSMM;
                  System.assertEquals('Create', testOppSMM.StageName);
            }
            Test.stopTest();
      }

      static testmethod void setPracticeCore(){
      	
            Test.startTest();
            System.runAs ( thisUser ) {
                  Opportunity testOppCore = new Opportunity(
                  Name='Test Opportunity Practice Core',
                  AccountId= testAccount.Id,
                  StageName= 'Create',
                  ForecastCategoryName= 'Omitted',
                  CloseDate= Date.today()+7,
                  Start_Date__c= Date.today()+8,
                  Solution_Area1__c= 'AD&I - SOA/Integration',
                  pse__Region__c= testRegion.Id);
                  insert testOppCore;
                  System.assertEquals('Create', testOppCore.StageName);
           } //System.runAs
            Test.stopTest();
      } //static testmethod
 
      static testmethod void setPracticeNull(){
            
            Test.startTest();
            System.runAs ( thisUser ) {
                  Opportunity testOppNull = new Opportunity(
                  Name='Test Opportunity Practice Null',
                  AccountId= testAccount.Id,
                  StageName= 'Create',
                  ForecastCategoryName= 'Omitted',
                  CloseDate= Date.today()+7,
                  Start_Date__c= Date.today()+8,
                  Solution_Area1__c= 'Software IBM',
                  pse__Region__c= testRegion.Id);
                  insert testOppNull;
                  System.assertEquals('Create', testOppNull.StageName);
           } //System.runAs
            Test.stopTest();
      } //static testmethod

      static testmethod void changePractice(){
      	
            Test.startTest();
            System.runAs ( thisUser ) {
                  Opportunity testChangeOpp = new Opportunity(
                  Name='Test Change Practice',
                  AccountId= testAccount.Id,
                  StageName= 'Create',
                  ForecastCategoryName= 'Omitted',
                  CloseDate= Date.today()+7,
                  Start_Date__c= Date.today()+8,
                  Solution_Area1__c= 'AD&I - SOA/Integration',
                  pse__Region__c= testRegion.Id);
                  insert testChangeOpp;
                  System.assertEquals('Create', testChangeOpp.StageName);
                  
                  List<Opportunity> kick = [Select Id, Solution_Area1__c from Opportunity where Name = 'Test Change Practice'];
                  kick.get(0).Solution_Area1__c = 'SFDC - Sales Cloud';
                  update kick;
            
                  System.assertEquals(testPracticeCRM.Id,[Select pse__Practice__c from Opportunity where Name = 'Test Change Practice'].pse__Practice__c);
           } //System.runAs
            Test.stopTest();
      } //static testmethod

       static testmethod void updateProject(){
            
            Test.startTest();
            System.runAs ( thisUser ) {
                  Opportunity testOppProject = new Opportunity(
                  Name='Test Opportunity Update Project',
                  AccountId= testAccount.Id,
                  StageName= 'Create',
                  ForecastCategoryName= 'Omitted',
                  CloseDate= Date.today()+7,
                  Start_Date__c= Date.today()+8,
                  Solution_Area1__c= 'AD&I - SOA/Integration',
                  pse__Primary_Project__c= testProject.Id,
                  pse__Region__c= testRegion.Id);
                  insert testOppProject;
                  System.assertEquals('Create', testOppProject.StageName);
                  List<Opportunity> trip = [Select Id, Solution_Area1__c, pse__Primary_Project__c from Opportunity where Name = 'Test Opportunity Update Project'];
                  trip.get(0).Solution_Area1__c = 'SFDC - Sales Cloud';
                  update trip;
                  System.assertEquals(testPracticeCRM.Id,[Select pse__Practice__c from pse__Proj__c where Name = 'TestProjectOppTrigger'].pse__Practice__c);
           } //System.runAs
            Test.stopTest();
      } //static testmethod     
} //class TestSetOppPractice