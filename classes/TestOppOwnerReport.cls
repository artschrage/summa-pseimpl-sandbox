@isTest (SeeAllData = true)

     public class TestOppOwnerReport
      {
      static testmethod void Emailtest(){
      

      List<Id> defaultRecipient;
      String CRON_EXP = '0 0 0 3 9 ? 2022';
      Test.startTest();
      User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
      System.runAs ( thisUser ) {
      EmailTemplate testtemplate = new EmailTemplate(
      Name = 'testTemplate1',
      Subject = 'test template',
      Body = 'this is a test',
      IsActive = true,
      TemplateType = 'html',
      BrandTemplateId = '016300000000R4rAAE',
      TemplateStyle = 'formalLetter',
      FolderId = '00D300000000ZIgEAM',
      DeveloperName = 'testTemplate1');
      insert testtemplate;
    System.assertNotEquals(null, testtemplate.Id);
            
      //String<Id> template = testtemplate.Id;
      //template = testtemplate.Id;
      
      Account acct = [Select Id, Name from Account where Name = 'Summa Technologies, Inc.'];
      pse__Work_Calendar__c wc = [Select Id from pse__Work_Calendar__c where Name = 'US'];
      
      Contact con1 = new Contact(
      RecordTypeId = '01240000000M3xOAAS',
      LastName = 'TestCon1',
      FirstName = 'Joe',
      Email = 'keith@summa-tech.com',
      AccountId = acct.Id,
      pse__Work_Calendar__c = wc.Id,
      pse__Is_Resource__c = true,
      pse__Is_Resource_Active__c = true,
      pse__Exclude_From_Missing_Timecards__c = false);
      
      insert con1;
      System.assertNotEquals(null, con1.Id);
      List<Contact> lstcon = new List<Contact>([Select Id, LastName, Email from Contact where LastName = 'TestCon1']);
      System.assertEquals('TestCon1', lstcon[0].LastName);
      
      List<Id> recipients = new List<Id>();
      recipients.add(con1.Id);
       String[] toAddresses = new String[] {lstcon[0].Email} ;
  
       Messaging.SingleEmailMessage testmail = new Messaging.SingleEmailMessage();
       testmail.setSubject('Test');
       testmail.setHtmlBody('Test');
       testmail.setToAddresses(toAddresses);
       testmail.setReplyTo('psaAdmin@summa-tech.com');
       testmail.setSenderDisplayName('PSA Admin');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { testmail });
      
      
      
      
      
     /*
     pse__Proj__c testproject = new pse__Proj__c(
      //Name = 'Sandbox test project',
      //pse__Region__c = 'a0nf00000004C9SAAU',
      //pse__Account__c = acct.Id,
      //pse__Is_Active__c = true,
      //pse__Is_Billable__c = true,
      //pse__Allow_Self_Staffing__c = true,
      //pse__Allow_Timecards_Without_Assignment__c = true);
      
      //insert testproject;
      //System.assertNotEquals(null, testproject.Id);
      
      //List<pse__Proj__c> testprojlist = new List<pse__Proj__c>([Select Id, Name from pse__Proj__c where Name like 'Sandbox%']);
      //System.assertEquals('Sandbox test project',testprojlist[0].Name);
      

      */
      
      String jobId = System.schedule('testScheduledApex', CRON_EXP, new OppOwnerReport());
      CronTrigger ct = [Select Id, CronExpression, TimesTriggered, NextFireTime From CronTrigger Where Id = :jobId];
      System.assertEquals(CRON_EXP, ct.CronExpression);
      System.assertEquals(0, ct.TimesTriggered);
      System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
      
      
      
      OppOwnerReport oor = new OppOwnerReport();
      
      
     } 
       Test.stopTest();
    }
    }