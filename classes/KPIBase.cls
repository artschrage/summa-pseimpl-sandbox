/**
    KPIBase - Base class for KPI recalculation classes.
    
    All KPI recalculators will inherit from this class
*/
public abstract class KPIBase {
    // Trigger the recalculation of the KPI value.  When this
    // method is complete, the value of the calculation will
    // appear in the 'Value' property.
    public KPI_Value__c calculate(pse__Time_Period__c  timePeriod) {
        return calculateImpl(timePeriod);
    }
 
	// 
	public KPI_Value__c getKPIValue(pse__Time_Period__c timePeriod) {
		KPI__c kpi = getTargetKPI();
		List<KPI_Value__c> values = [SELECT Id, Name, Actual__c, 
				KPI__c FROM KPI_Value__c WHERE Time_Period__c = :timePeriod.Id
				 //and  KPI__r.Id = :kpi.Id];
				 ];
		if (values.size() == 0)
			return null;
	 	return values.get(0);
	}
    
    // Subclasses will implement this method to update the Value 
    // property for the KPI for the supplied time period
    public abstract KPI_Value__c calculateImpl(pse__Time_Period__c timePeriod);
    
	// Return the KPI object (not the value) with which the
	// value will be stored
    protected abstract KPI__c getTargetKPI();
    
}