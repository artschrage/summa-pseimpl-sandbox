@isTest(seealldata=false)
private class MDGTestReportLogic {
    static Account account;
    static pse__Region__c region;
    static pse__Proj__c project;
    static pse__Practice__c practice;
    static pse__Time_Period__c lastQuarter;
    static KPI__c totalGrossMarginKPI;
    static KPI_Value__c totalGrossMarginKPIValue;
    static KPI__c revenueCaptureKPI;
    static KPI_Value__c revenueCaptureKPIValue;
    static pse__Project_Actuals__c actuals;
    
    static {
        buildTestData();
    }
    
    static testMethod void testTotalGrossMarginReport() {
        Test.startTest();
        KPITotalGrossMarginReportLogic logic = new 
                        KPITotalGrossMarginReportLogic();
        KPI_Value__c value = logic.execute(totalGrossMarginKPIValue);
        System.assertEquals(0.8, value.Actual__c, 'Actuals should be 0.8');
        Test.stopTest();
    }
    
    static testMethod void testRevenueCaptureReport() {
        Test.startTest();
        KPIRevenueCaptureReportLogic logic = new 
                        KPIRevenueCaptureReportLogic();
        KPI_Value__c value = logic.execute(revenueCaptureKPIValue);
        System.assertEquals(160000.0, value.Actual__c, 'Actuals should be 160000.0');
        Test.stopTest();
    }
    
    
    static void buildTestData() {
        account = new Account(Name='TestReportLogicAccount');
        insert account;
        region = new pse__Region__c(Name='Corporate');
        insert region;
        practice = new pse__Practice__c(Name='Core');
        insert practice;
        
        buildLastQuarter();
        buildKPIs();
        buildKPIValues();
        buildProject();
        buildProjectActuals();
    }
    
    static void buildLastQuarter() {
        lastQuarter = new pse__Time_Period__c(
            name = 'Q2 FY2014', 
            pse__Start_Date__c = Date.newInstance(2013, 10, 1),
            pse__End_Date__c = Date.newInstance(2013, 12, 31),
            pse__Type__c = 'Quarter');
        insert lastQuarter;
    }

    static void buildKPIs() {
        totalGrossMarginKPI = new KPI__c(
            Name = 'Project Gross Margin',
            Category__c = 'Delivery', 
            Display_Order__c = 2,
            Format__c = '0.00%',
            Is_Active__c = true,
            Report_Class_Name__c = 'KPITotalGrossMarginReportLogic');
        insert totalGrossMarginKPI;
        
        revenueCaptureKPI = new KPI__c(
            Name = 'T&M Revenue Capture',
            Category__c = 'Delivery', 
            Display_Order__c = 2,
            Format__c = '$###,###,##0.00',
            Is_Active__c = true,
            Report_Class_Name__c = 'KPIRevenueCaptureReportLogic');
        insert revenueCaptureKPI;
    }
    
    static void buildKPIValues() {
        totalGrossMarginKPIValue = new KPI_Value__c(
            KPI__c = totalGrossMarginKPI.Id,
            Target__c = 0.0, Actual__c = 0.0,
            Time_Period__c = lastQuarter.Id);
        insert totalGrossMarginKPIValue;
        
        totalGrossMarginKPIValue =
                [SELECT Time_Period__r.Name from KPI_Value__c 
                    where Time_Period__r.Id = :lastQuarter.Id AND
                    KPI__r.Name = 'Project Gross Margin' LIMIT 1];
                  
        revenueCaptureKPIValue = new KPI_Value__c(
            KPI__c = revenueCaptureKPI.Id,
            Target__c = 0.0, Actual__c = 0.0,
            Time_Period__c = lastQuarter.Id);
        insert revenueCaptureKPIValue;
        
        revenueCaptureKPIValue =
                [SELECT Time_Period__r.Name from KPI_Value__c 
                    where Time_Period__r.Id = :lastQuarter.Id AND
                    KPI__r.Name = 'T&M Revenue Capture' LIMIT 1];
    }
    
    static void buildProject() {
        project = new pse__Proj__c();
        project.Name = 'TestReportLogicProject';
        project.pse__Account__c = project.Id;
        project.pse__Is_Active__c = true;
        project.pse__Is_Billable__c = true;
        project.pse__Billing_Type__c = 'Time and Materials';
        project.pse__Start_Date__c = Date.newInstance(2013, 12, 1);
        project.pse__End_Date__c = Date.newInstance(2013, 12, 31);
        project.pse__Practice__c = practice.Id;
        insert project;
    }

    static void buildProjectActuals() {
        actuals = new pse__Project_Actuals__c();
        actuals.Name = 'TestReportLogicActuals';
        actuals.pse__Project__c = project.Id;
        actuals.pse__Unique_Name__c = 'TestReportLogicActuals';
        actuals.pse__Time_Period__c = lastQuarter.Id;
        actuals.pse__Billings__c = 200000.0;
        actuals.pse__Internal_Costs__c = 10000.0;
        actuals.pse__External_Costs__c = 10000.0;
        actuals.pse__Expense_Costs__c = 10000.0;
        actuals.pse__Other_Costs__c = 10000.0;
        insert actuals;
        
        actuals = [select pse__Billings__c, pse__Total_Costs__c FROM
                pse__Project_Actuals__c where Name = 'TestReportLogicActuals'
                LIMIT 1];
    }
}