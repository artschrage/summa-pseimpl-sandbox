public with sharing class CommonDao {
    
    private static pse__Time_Period__c prevQuarter = null;
    private static pse__Time_Period__c currQuarter = null;
    private static pse__Time_Period__c nextQuarter = null;
    private static pse__Time_Period__c currYear = null;

    public static pse__Time_Period__c getPreviousQuarter() {
        if (prevQuarter == null) {
            prevQuarter = [select id, name, pse__start_date__c, 
                    pse__end_date__c, pse__type__c from pse__time_period__c 
                    where pse__start_date__c = LAST_QUARTER and 
                    pse__end_date__c = LAST_QUARTER and pse__type__c = 'Quarter' limit 1];
        }
        return prevQuarter;
    }

    public static pse__Time_Period__c getCurrentQuarter() {
        if (currQuarter == null) {
            currQuarter = [select id, name, pse__start_date__c, 
                    pse__end_date__c, pse__type__c from pse__time_period__c 
                    where pse__start_date__c <= THIS_MONTH and 
                    pse__end_date__c >= THIS_MONTH and pse__type__c = 'Quarter' limit 1];
        }
        return currQuarter;
    }

    public static pse__Time_Period__c getNextQuarter() {
        if (nextQuarter == null) {
            nextQuarter = [select id, name, pse__start_date__c, 
                    pse__end_date__c, pse__type__c from pse__time_period__c 
                    where pse__start_date__c = NEXT_QUARTER and 
                    pse__end_date__c = NEXT_QUARTER and pse__type__c = 'Quarter' limit 1];
        }
        return nextQuarter;
    }

    public static pse__Time_Period__c getCurrentYear() {
        if (currYear == null) {
            currYear = [select id, name, pse__start_date__c, 
            pse__end_date__c, pse__type__c from pse__time_period__c 
            where pse__start_date__c <= THIS_MONTH and 
            pse__end_date__c >= THIS_MONTH and pse__type__c = 'Year' limit 1];
        }
        return currYear;
    }

    // DEPRECATE ME
    public CommonDao() {}

    // DEPRECATE ME
    public pse__Time_Period__c fetchPreviousQuarterTimePeriod() {
        return getPreviousQuarter();
    }

    // DEPRECATE ME
    public pse__Time_Period__c fetchCurrentQuarterTimePeriod() {
        return getCurrentQuarter();
    } 
    
    // DEPRECATE ME
    public pse__Time_Period__c fetchNextQuarterTimePeriod() {
        return getNextQuarter();
    }
    
    // DEPRECATE ME
    public pse__Time_Period__c fetchCurrentYearTimePeriod() {
        return getCurrentYear();
    } 
    
    // DEPRECATE ME
    public List<Account> fetchAccountsWithIdOnly() {
        return fetchAccountsWithIdOnly(false);
    }
    
    // DEPRECATE ME
    public List<Account> fetchAccountsWithIdOnly(boolean activeOnly) {
    	if (activeOnly) {
    		return [select id from Account where Status__c = 'Active'];
    	} else {
    		return [select id from Account];
    	}
    }

}