@isTest
global class TestResourceReqMockHttpResponse404 implements HttpCalloutMock
{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
		// Response to simulate a 403 error (not authorized)
       	HttpResponse res = new HttpResponse();
        if (req.getMethod()=='POST'){
        	// Create a fake initial authorization response
        	res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        	res.setBody('{"access_token" : "ya29.UAAzMxBfAYVnGyAAAACxT5zUTGpeupVG4PsMwFyMqY_IeO0nhAbdBNmvtDXolw", "token_type" : "Bearer", "expires_in" : 3600 }');
       		res.setStatusCode(200);
    	} else {
        	// Create a fake spreadsheet content response
        	res.setBody('<book  author="Manoj" >My Book</book>');
       		res.setStatusCode(404);
    	}
       	return res;
    }
}