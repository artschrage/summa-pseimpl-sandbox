public with sharing class CoreResourceTimeLastWeek {
    // ApexPages.StandardSetController must be instantiated
    // for standard list controllers
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT pse__Resource__r.ReportsTo.Name, pse__Resource__r.Name, pse__Project__r.Name, pse__Total_Hours__c FROM pse__Timecard__c where pse__End_Date__c = LAST_WEEK and pse__Resource__r.pse__Practice__r.Name = 'Core' order by pse__Resource__r.ReportsTo.Name, pse__Resource__r.Name]));
            }
            setCon.setPageSize(100);
            return setCon;
        }
        set;
    }

    // Initialize setCon and return a list of records
    public List<pse__Timecard__c> getCoreResourceTime() {
        return (List<pse__Timecard__c>) setCon.getRecords();
    }
    
 

    public static testMethod void testMyController() {
    	CoreResourceTimeLastWeek controller = new CoreResourceTimeLastWeek();
    	List<pse__Timecard__c> testList = controller.getCoreResourceTime();
    	System.assertnotequals(null, testList);
    }
    
    
}