global class KPIBatcher implements Database.Batchable<KPI_Value__c>, Database.Stateful { 
	
	private static KPIBso bso;
	private static pse__Time_Period__c prevQuarter;
    private static pse__Time_Period__c fiscalYear;
    private static List<KPI__c> quarterKPIs;
    private static List<KPI__c> yearKPIs;
	
	static {
		bso = new KPIBso();
        prevQuarter = bso.getPreviousQuarter();
        System.debug(LoggingLevel.FINE, 'Prev Qtr=' + prevQuarter.Name);
        fiscalYear = bso.getCurrentYear();
        System.debug(LoggingLevel.FINE, 'Fiscal Yr=' + fiscalYear.Name);

        quarterKPIs = bso.getActiveKPIs('Quarter');
        for (KPI__c kpi : quarterKPIs)
            System.debug(LoggingLevel.FINE, 'Q: ' + kpi.Name);
        yearKPIs = bso.getActiveKPIs('Year');
        for (KPI__c kpi : yearKPIs)
            System.debug(LoggingLevel.FINE, 'Y: ' + kpi.Name);
	}
	
	global List<KPI_Value__c> start(Database.BatchableContext bc) {
        System.debug(LoggingLevel.FINE, 'start()');
        List<KPI_Value__c> quarterValues = 
                    bso.getMissingKPIValuesInTimePeriod(quarterKPIs, prevQuarter);
        for (KPI_Value__c value : quarterValues) {
            System.debug(LoggingLevel.FINE, 'Missing Q: ' + value.Title__c);
        }
        bso.saveKPIValues(quarterValues);

        List<KPI_Value__c> yearValues = 
                    bso.getMissingKPIValuesInTimePeriod(yearKPIs, fiscalYear);
        for (KPI_Value__c value : yearValues) {
            System.debug(LoggingLevel.FINE, 'Missing Y: ' + value.Title__c);
        }
        bso.saveKPIValues(yearValues);

        quarterValues = bso.getKPIValues(quarterKPIs, prevQuarter);
        yearValues = bso.getKPIValues(yearKPIs, fiscalYear);
        List<KPI_Value__c> newValues = new List<KPI_Value__c>();

        for (KPI_Value__c value : quarterValues) {
            System.debug(LoggingLevel.FINE, 'Q: ' + value.kpi__r.Name + ' ' + 
        			value.Time_Period__r.Name + ' ' + value.Actual__c);
        }
        for (KPI_Value__c value : yearValues) {
            System.debug(LoggingLevel.FINE, 'Y: ' + value.kpi__r.Name + ' ' + 
            		value.Time_Period__r.Name + ' ' + value.Actual__c);
        }

        newValues.addAll(quarterValues);
        newValues.addAll(yearValues);
        return newValues;
	}
	
	global void execute(Database.BatchableContext bc, List<KPI_Value__c> kpiValues) {
		List<KPI_Value__c> values = bso.executeReports(kpiValues);
		bso.saveKPIValues(values);
	}
	
	global void finish(Database.BatchableContext bc){

	}

}