@isTest(seealldata=false)
private class TestSalesforceConstructs {
	
    private static pse__Region__c regionCorporate;
    private static pse__Practice__c practiceCore;
    private static Contact johnSmail;
    private static Account account;

    static testMethod void testSetUniqueness() {
        Date startWindow = getDataStageDate();
        stageData(startWindow);

    	Opportunity opp = [select Id, name from Opportunity where name = 'Opp Closed-Won Before Window' limit 1];

    	Set<Opportunity> opps = new Set<Opportunity>();
    	opps.add(opp);

    	opp.Name = 'New Name for Opp';
    	opps.add(opp);

    	for (Opportunity o : opps) {
    		system.debug(o);
    	}
    }

    static testMethod void testSetUniqueness2Opps() {
        Date startWindow = getDataStageDate();
        stageData(startWindow);

    	Opportunity opp = [select Id, name from Opportunity where name = 'Opp Closed-Won Before Window' limit 1];

    	Set<Opportunity> opps = new Set<Opportunity>();
    	opps.add(opp);

    	opp.Name = 'New Name for Opp';
    	opps.add(opp);

    	Opportunity o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Second Opportunity';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow+2;
        o.Start_Date__c = startWindow+3;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Closed';
        o.StageName = 'Won';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';

        insert o;

        opps.add(o);

    	for (Opportunity x : opps) {
    		system.debug(x);
    	}
    }

    static testMethod void testEmail() {
        system.debug('The total number of email invocations allowed is: ' + Limits.getLimitEmailInvocations());
        system.debug(Limits.getEmailInvocations() + ' email invocations have been used thus far.');

        List<Messaging.SingleEmailMessage> allMail = new List<Messaging.SingleEmailMessage>();
        Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();

        mail.setToAddresses(new String[] {'smail@summa-tech.com'});
        mail.setPlainTextBody('This message was generated as part of an automated system test. Please disregard.');
        mail.setSubject('Salesforce Email Test');
        mail.setReplyTo('john.a.smail@gmail.com');
        allMail.add(mail);
            
        Messaging.sendEmail(allMail);

        system.debug(Limits.getEmailInvocations() + ' email invocations have been used thus far.');
        system.assert(true);
    }

    private static void stageData(Date startWindow) {
        stagePrerequisites();
        
        stageOpportunities(startWindow);
    }

    private static Date getDataStageDate() {
        Date now = Date.today();
        if (now.month() >= 7) {
            return Date.newInstance(now.year()-2, 7, 1);
        } else {
            return Date.newInstance(now.year()-3, 7, 1);
        }
    }

    private static void stagePrerequisites() {
        //region
        regionCorporate = new pse__Region__c(Name='Corporate', CurrencyIsoCode='USD');
        insert regionCorporate;
        
        //practice
        List<pse__Practice__c> practices = new List<pse__Practice__c>();
        
        practiceCore = new pse__Practice__c(Name='Core', CurrencyIsoCode='USD');
        practices.add(practiceCore);
        //need these additional to keep the UpdateOpportunityPractice trigger happy.
        practices.add(new pse__Practice__c(Name='CRM', CurrencyIsoCode='USD'));
        practices.add(new pse__Practice__c(Name='SMM', CurrencyIsoCode='USD'));

        insert practices;
        
        //contact
        String currentUserId = UserInfo.getUserId();
        johnSmail = new Contact(FirstName='John', LastName='Smail', Email='smail@summa-tech.com', CurrencyIsoCode='USD', pse__Is_Resource__c=TRUE, pse__Is_Resource_Active__c=TRUE, pse__Region__c=regionCorporate.Id, pse__Practice__c=practiceCore.Id, pse__Salesforce_User__c=currentUserId);
        insert johnSmail;
        
        pse__Permission_Control__c permission = new pse__Permission_Control__c(
            pse__Billing__c=TRUE,
            pse__Cascading_Permission__c=TRUE,
            pse__Expense_Entry__c=TRUE,
            pse__Expense_Ops_Edit__c=TRUE,
            pse__Forecast_Edit__c=TRUE,
            pse__Forecast_View__c=TRUE,
            pse__Invoicing__c=TRUE,
            pse__Region__c=regionCorporate.Id,
            pse__Resource_Request_Entry__c=TRUE,
            pse__Staffing__c=TRUE,
            pse__Timecard_Entry__c=TRUE,
            pse__Timecard_Ops_Edit__c=TRUE,
            pse__User__c=currentUserId);
        insert permission;
                
        //account
        account = new Account(Name='TestExistingClientAnalysisBso Account A', CurrencyIsoCode='USD');
        insert account;
    }
    
    private static void stageOpportunities(Date startWindow) {
        List<Opportunity> newOpps = new List<Opportunity>();
        Opportunity o = null;
        
        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Closed-Won Before Window';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow-1;
        o.Start_Date__c = startWindow;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Closed';
        o.StageName = 'Won';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);
        
        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Pipeline Before Window';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow-1;
        o.Start_Date__c = startWindow;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Pipeline';
        o.StageName = 'Develop';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);

        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Closed-Won On Window Boundary';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow;
        o.Start_Date__c = startWindow+1;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Closed';
        o.StageName = 'Won';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);
        
        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Pipeline On Window Boundary';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow;
        o.Start_Date__c = startWindow+1;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Pipeline';
        o.StageName = 'Develop';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);

        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Closed-Won In Window Before Threshold';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow.addYears(1)-1;
        o.Start_Date__c = startWindow.addYears(1);
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Closed';
        o.StageName = 'Won';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);
        
        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Pipeline In Window Before Threshold';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow.addYears(1)-1;
        o.Start_Date__c = startWindow.addYears(1);
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Pipeline';
        o.StageName = 'Develop';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);

        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Closed-Won In Window On Threshold';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow.addYears(1);
        o.Start_Date__c = startWindow.addYears(1)+1;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Closed';
        o.StageName = 'Won';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);
        
        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Pipeline In Window On Threshold';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow.addYears(1);
        o.Start_Date__c = startWindow.addYears(1)+1;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Pipeline';
        o.StageName = 'Develop';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);

        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Closed-Won In Window After Threshold';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow.addYears(1)+1;
        o.Start_Date__c = startWindow.addYears(1)+2;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Closed';
        o.StageName = 'Won';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);
        
        o = new Opportunity();
        o.AccountId = account.Id;
        o.Name = 'Opp Pipeline In Window After Threshold';
        o.pse__Practice__c = practiceCore.Id;
        o.CloseDate = startWindow.addYears(1)+1;
        o.Start_Date__c = startWindow.addYears(1)+2;
        o.pse__Region__c = regionCorporate.Id;
        o.ForecastCategoryName = 'Pipeline';
        o.StageName = 'Develop';
        o.Amount = 1000;
        o.Primary_Solution_Area__c = 'Application Modernization';
        newOpps.add(o);

        insert newOpps;
    }
	
}