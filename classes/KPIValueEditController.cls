/* Controller to manage updates of target KPI values
*/
public class KPIValueEditController 
{
    private List<SelectOption> futureQuarters;
    private List<EditableKPIValue> kpiValues;
    private Map<String, pse__Time_Period__c> quarterMap;
    
    public KPIValueEditController()
    {
        List<pse__Time_Period__c> quarters = [SELECT Id, Name FROM pse__Time_Period__c 
                           WHERE pse__Type__c = 'Quarter'
                           AND pse__Start_Date__c > TODAY LIMIT 6];

        futureQuarters = new List<SelectOption>();
        quarterMap = new Map<String, pse__Time_Period__c>();
         
        for (pse__Time_Period__c quarter : quarters)
        {
            SelectOption option = new SelectOption(quarter.Name, quarter.Name);
            futureQuarters.add(option);
            quarterMap.put(quarter.Name, quarter);
        }
        
        showEditSection = false;
    }

    public PageReference save()
    {
        for (EditableKPIValue value : kpiValues)
        {
            value.save();
        }
        loadData();
        return null;
    }

    public String currentQuarterName
    { 
        get;
        set
        {
            currentQuarterName = value;
            currentQuarter = quarterMap.get(value);
            loadData();
        } 
    }
    
    // Represents the currently selected quarter's values being edited.
    // It will be <null> when there is no quarter selected.
    public pse__Time_Period__c currentQuarter {get; set;}
    
    // Toggle whether lower portion of the window is visible
    public boolean showEditSection {get; set;}
        

    public List<SelectOption> getFutureQuarters() 
    {
        return futureQuarters;
    }
    
    public List<EditableKPIValue> getKPIValues() 
    {
        return kpiValues;
    }
    
    public void loadData()
    {
        System.debug('loadData()');
        showEditSection = currentQuarter != null;
        
        List<KPI_Value__c> rawValues = [SELECT Id, Title__c, Target__c 
                                        from KPI_Value__c LIMIT 6];
        kpiValues = new List<EditableKPIValue>();
        for (KPI_Value__c rawValue : rawValues)
        {
            System.debug('KPI: ' + rawValue.Title__c);
            EditableKPIValue value = new EditableKPIValue(rawValue);
            kpiValues.add(value);
        }
    }
    
    /**
        This inner class is a wrapper that provides editable self-updating
        version of the KPI_Value object.
    */
    public class EditableKPIValue
    { 
        public String ID { get; set; }
        public String Title { get; set; }
        public Decimal InitialTarget { get; set; }
        public Decimal NewTarget 
        { 
            get; 
            set
            {
                NewTarget = value;
                System.debug('New Target =' + value + ' ' + Title + ' ' + NewTarget); 
            }
        }
        public Boolean Checked 
        { 
            get; 
            set 
            { 
                Checked = value; 
                System.debug('Check! =' + value + ' ' + Title + ' ' + NewTarget); 
            } 
        }
        public KPI_Value__c Original { get; set; }

        /**
            Given a KPI value, propagate the field values
            to the wrapper
        */
        public EditableKPIValue(KPI_Value__c kpiValue)
        {
            System.debug('ctor()');
            Original = kpiValue;
            ID = kpiValue.ID;
            Title = kpiValue.Title__c;
            InitialTarget = kpiValue.Target__c;         
            NewTarget = 0.0; 
            Checked = false;        
        }
        
        
        public void save()
        {
            // Do not save records that are not checked for edit
            System.debug('About to save ID: ' + Checked + ' ' + Title + ' ' + NewTarget);
            if (Checked == null || Checked == false)
            {
                return;
            }
            
            System.debug('Saving ID: ' + ID);
            Original.Target__c = NewTarget;
            update Original;
        }
    }
}