global class OppOwnerReport2 implements Schedulable
{ 

 Map<Id , List<pse__Proj__c>> oppOwnerProjMap {get; set;}
 List<pse__Proj__c> allOppOwnerProj {get; set;}
 Set<Id> oppOwnerIds = new Set<Id>() ;
 String oppOwnerName = '';

 public OppOwnerReport2(){
 }
 
 global void execute(SchedulableContext ctx) {
 
       //Map oppOwners and their project lists
       oppOwnerProjMap = new Map<Id, List<pse__Proj__c>>();

       //All Opp Owners
       List<pse__Proj__c> oppOwners = new List<pse__Proj__c>() ;
       oppOwners = [Select pse__Opportunity_Owner__c from pse__Proj__c where pse__Is_Active__c = true] ;

       //All Opp Owner ids
       //Set<Id> oppOwnerIds = new Set<Id>() ;
       for(pse__Proj__c oo : oppOwners)
       {
        if (oo.pse__Opportunity_Owner__c != null){
           oppOwnerIds.add(oo.pse__Opportunity_Owner__c) ;
           }
       }

       //All opp owner project data
       allOppOwnerProj = new List<pse__Proj__c>() ;
       allOppOwnerProj = [Select Name, pse__Opportunity_Owner__c,
    pse__Project_Manager__r.Name,pse__Billing_Type__c,pse__Project_Status__c,
    pse__Start_Date__c,pse__End_Date__c,pse__Bookings__c,pse__Billings__c,
    pse__Unscheduled_Backlog__c,pse__Planned_Hours__c,
    pse__Total_Assigned_Hours__c from pse__Proj__c where
    pse__Is_Active__c = true and pse__Opportunity_Owner__c in : oppOwnerIds] ;
   
         for(pse__Proj__c proj : allOppOwnerProj)
         {
             if(oppOwnerProjMap.containsKey(proj.pse__Opportunity_Owner__c))
             {
                 //Fetch the list of projects and add the new project
                 List<pse__Proj__c> tempList = oppOwnerProjMap.get(proj.pse__Opportunity_Owner__c) ;
                 tempList.add(proj);
                 //Putting the refreshed list in map
                 oppOwnerProjMap.put(proj.pse__Opportunity_Owner__c , tempList) ;
             }
             else
             {
                 //Creating a list of projects and putting it in map
                 oppOwnerProjMap.put(proj.pse__Opportunity_Owner__c , new List<pse__Proj__c>{proj}) ;
             }
         }
    
     List<Id> oppOwnerList = new List<Id>(oppOwnerIds);
    // List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

    DevLog__c newLog = new DevLog__c();
    String logData = '';
    newLog.Type__c = 'OppOwner Report';
     
     for(Integer i=0; i<oppOwnerList.size();i++)
     {
                   //Fetching all projects for opp owner
                   List<pse__Proj__c> allOppOwnerProjs = oppOwnerProjMap.get(oppOwnerList.get(i)) ;
                   if (allOppOwnerProjs.size() > 0){
                   	allOppOwnerProjs.sort();
                   }
                   
                   List<User> ur = [Select Name, Email, IsActive from User where Id = :oppOwnerList.get(i)];
                   System.debug('OppOwner Id = ' +oppOwnerList.get(i));
                   oppOwnerName = (String)ur[0].Name;
                   
                   if(ur[0].IsActive == true){
                   logData = logData + i + '. ' + oppOwnerName + '\n';
     
                   String body = '' ;
                   //Creating tabular format for the case details
                   body = BodyFormat(allOppOwnerProjs) ;
     
                   //Sending Mail
                   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
     
                   //Setting user email in to address
                   String[] toAddresses = new String[] {ur[0].Email} ;
     
                   // Assign the addresses for the To and CC lists to the mail object
                   mail.setToAddresses(toAddresses) ;
     
                   //Email subject to be changed
                   mail.setSubject('Opportunity Owner Report');
     
                   //Body of email 
                   mail.setHtmlBody(body);
                   
                   //add email to list
                  // emails.add(mail);
                   }
                   //Sending the email (this has been replaced with 'send all the emails')
                   //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }
         //send all the emails
      //Messaging.sendEmail(emails);
      newLog.Data__c = logData;
      insert newLog;
              
 }
    public String BodyFormat(List<pse__Proj__c> proj_list)
    {
        String str = '' ;
        Integer rowcount = 0;
        String rowcolor = '';
        Integer y;
        Integer z;
        
        for(pse__Proj__c cs : proj_list)
        {
        rowcount++;
        System.debug('oppOwnerName = ' +oppOwnerName);
        y = rowcount/2;
        z = y * 2;
        if (rowcount == z){
        rowcolor = '#87cefa';
        } else {
        rowcolor = '#f5f5f5';
        }
  str += '<tbody style="background-color: ' +rowcolor +';text-align:center;"><tr><td>'+ cs.Name +'</td>'+'<td>'+ cs.pse__Bookings__c +'</td>'+'<td>'+ cs.pse__Project_Manager__r.Name +'</td>' +'<td>'+ cs.pse__Billing_Type__c +'</td>' +'<td>'+ cs.pse__Project_Status__c +'</td>'+'<td>'+ String.valueOf(cs.pse__Start_Date__c) +'</td>' +'<td>'+ String.valueOf(cs.pse__End_Date__c) +'</td>' +'<td style="text-align:right;">'+ cs.pse__Bookings__c +'</td>' +'<td style="text-align:right;">'+ cs.pse__Billings__c +'</td>' +'<td style="text-align:right;">'+ cs.pse__Project_Manager__r.Name +'</td>' +'<td style="text-align:right;">'+ cs.pse__Unscheduled_Backlog__c +'</td>' +'<td>'+ cs.pse__Planned_Hours__c +'</td>' +'<td>'+ cs.pse__Total_Assigned_Hours__c +'</td>' +'<td>'+ cs.pse__Project_Manager__r.Name +'</td>' +'</tr>' ;
        }
        str = str.replace('null' , '') ;
        String finalStr = '' ;
        finalStr = '<h2>Weekly Opportunity Owner Report - ' +oppOwnerName +'</h2><table border="0"> <tbody style="background-color: #c0c0c0;font-weight:bold;;text-align:center;"><tr><td>Project</td><td>Delivery Engagement Mgr</td><td>Project Manager</td><td>Billing Type</td><td>Status</td><td>Start Date</td><td>End Date</td><td>Bookings</td><td>Billings</td><td>Remaining Budget</td><td>Unscheduled Backlog</td><td>Planned Hours</td><td>Total Assigned Hrs</td><td>Gross Margin</td></tr> ' + str +'</table>' ;
        return finalStr ;
   }
}