@isTest (SeeAllData = true)

public with sharing class TestRequireRejectionComments {
     static testmethod void rejectionTest(){
      
      Test.startTest();
      User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
      System.runAs ( thisUser ) {
      
      pse__Region__c testRegion = [Select Id from pse__Region__c where Name = 'Corporate'];
      
      Account testAccount = new Account(
      Name='TestAcct',
      CurrencyIsoCode='USD');
      insert testAccount;
      //System.debug('Account = ' +testAccount.Name);
      System.assertEquals('TestAcct', testAccount.Name);
      
      Account acct = [Select Id from Account where Name = 'TestAcct'];
      //System.debug('account query gives testAccount ID = ' +acct.Id);
      
      pse__Work_Calendar__c wc = [Select Id from pse__Work_Calendar__c where Name = 'US'];
      RecordType rt = [Select Id from RecordType where Name = 'Resource'];
      
      Contact con1 = new Contact(
      RecordTypeId = rt.Id,
      LastName = 'TestCon1',
      FirstName = 'Joe',
      Email = 'keith@summa-tech.com',
      AccountId = acct.Id,
      pse__Work_Calendar__c = wc.Id,
      pse__Is_Resource__c = true,
      pse__Is_Resource_Active__c = true,
      pse__Region__c = testRegion.Id,
      pse__Default_Cost_Rate__c = 50.00);
      
      insert con1;
      System.assertNotEquals(null, con1.Id);
      
      pse__Proj__c testproject = new pse__Proj__c(
      Name = 'Sandbox test project',
      pse__Region__c = testRegion.Id,
      pse__Account__c = acct.Id,
      pse__Is_Active__c = true,
      pse__Is_Billable__c = false,
      pse__Allow_Self_Staffing__c = true,
      pse__Allow_Timecards_Without_Assignment__c = true,
      pse__Project_Manager__c = con1.Id);
      
      insert testproject;
      System.assertNotEquals(null, testproject.Id);
      
      pse__Timecard_Header__c testTC = new pse__Timecard_Header__c(
      pse__Resource__c = con1.Id,
      pse__Project__c = testproject.Id,
      pse__Start_Date__c = date.today() -3,
      pse__End_Date__c = date.today() +3,
      pse__Billable__c = false,
      pse__Monday_Hours__c = 8.0,
      pse__Tuesday_Hours__c = 8.0,
      pse__Submitted__c = true,
      pse__Status__c = 'Submitted',
      pse__Approved__c = false,
      pse__Approver__c = thisUser.Id);
      
      insert testTC;
      
      List<ProcessInstanceWorkItem> wi = [Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkItem where ProcessInstance.TargetObjectId = :testTC.Id];
     // System.debug('workitem Id = '+wi[0].Id);
      System.assertNotEquals(null, testTC.Id);
     // System.debug('##testTCId = '+testTC.Id);
     // System.debug('Monday Hours on timecard after insert = '+ testTC.pse__Monday_Hours__c);
     // System.debug('Total Hours on timecard = '+ testTC.pse__Total_Hours__c);
      System.assertEquals(8.0, testTC.pse__Monday_Hours__c);
      
      Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
      req.setComments('rejected');
      req.setAction('Reject');
      req.setWorkitemId(wi[0].Id);
      
      Approval.ProcessResult processResults = Approval.process(req);

     
      } //System.runAs
      
      Test.stopTest();
      
      } //static testmethod
      
      } //class TestRequireRejectionComments