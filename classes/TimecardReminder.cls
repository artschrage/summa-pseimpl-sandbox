global class TimecardReminder implements Schedulable
{ 

 String<Id> sid;
 List<Id> lst;

 public TimecardReminder(){
 }
 
 global void execute(SchedulableContext ctx) {
 sid = getEmailTemplate();
 lst = getEmailAddresses();
 sendEmail();
 }
 
  public void sendEmail()
  {
     if (sid == null){
     sid = '00X40000001aPlc';
     System.debug('hard coded email template');
     }
     System.debug('Start sendEmail');
      if (lst != null){
       Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
       mail.setTargetObjectIds(lst);
       mail.setTemplateId(sid);
       mail.setReplyTo('psaAdmin@summa-tech.com');
       mail.setSenderDisplayName('PSA Admin');
       Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
      }
      if (lst == null){
      System.debug('no email list, no emails sent');
      }
   }
   
  List<Id> getEmailAddresses(){  //get the list of people who haven't submitted a timecard for this past week
            // this query gets the list of active employees who have submitted a timecard for the past week
    List<pse__Timecard_Header__c> submittedList = [Select pse__Resource__c from pse__Timecard_Header__c where pse__End_Date__c = THIS_WEEK and pse__Submitted__c = true];
    Set<Id> setids = new Set<Id>();
             // this creates a set of the Ids for those who have submitted a timecard
        for (Integer i = 0;i < submittedList.size();i++){
            setids.add(submittedList[i].pse__Resource__c);
        }
        System.debug('Set of submitted list = ' +setids);
              // this query gets the list of all active employees who have not submitted a timecard 
     List<Contact> lstcon = [Select Id, Name from Contact where pse__Is_Resource_Active__c = true and pse__Exclude_From_Missing_Timecards__c = false and Id !=:setids];
     List<Id> lstids = new List<Id>();
        for (Integer j=0; j < lstcon.size(); j++) {
            lstids.add(lstcon[j].Id);
        }
        System.debug('List of remindees = ' +lstids);
      return lstids;
  }
   
    String<Id> getEmailTemplate(){ //get the email template for the reminder notification
    EmailTemplate e = [Select Id from EmailTemplate where Name = 'Timecard_Not_Submitted'];
        System.debug('email template ID = ' +e.Id);
        if (e != null) {
        return e.Id;
        } else {
        //return null;
        return sid;
        }
    }
        
}