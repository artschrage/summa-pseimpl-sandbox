/**
    Controller to manage the VF page that serves as the KPI 'Dashboard' and
    displays a set of KPI values.
*/
public class KPIController
{
    private List<pse__Time_Period__c> periods;
    public Boolean HideValues { get {return false;}}
    public Boolean ShowValues {get; set;}
    public List<KPI_Value__c> FinancialValues { get; set; }
    public List<KPI_Value__c> CustomerValues { get; set; }
    public List<KPI_Value__c> DeliveryValues { get; set; }
    public List<KPI_Value__c> EmployeeValues { get; set; }
    
    private pse__Time_Period__c CurrentPeriod;
    public String CurrentQuarter 
    { 
        get; 
        set
        {
            CurrentQuarter = value;
            updateQuarter();
        }
    }

    public KPIController()
    {
        ShowValues = false;
    }
    
    private void updateQuarter() 
    {
        System.debug('Current Quarter is now: ' + CurrentQuarter);
        currentPeriod = [SELECT Id, Name FROM pse__Time_Period__c WHERE Name = :CurrentQuarter];
        System.debug('period: ' + currentPeriod.Name);
    }

    public List<SelectOption> getAllPeriods()
    {
        periods = [SELECT Id, Name from pse__Time_Period__c where pse__Type__c = 'Quarter' and pse__End_Date__c < today order by pse__Start_Date__c DESC LIMIT 6];

        List<SelectOption> options = new List<SelectOption>();
        for(pse__Time_Period__c period : periods)
        {
            options.add(new SelectOption(period.Name, period.Name));
        }
        currentPeriod = periods[0];
        CurrentQuarter = periods[0].Name;
        return options;
    }
    
    public PageReference doSearch()
    {
        ShowValues = true;
        FinancialValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where Time_Period__c  = :currentPeriod.Id 
                    and Category__c = 'Financial' order by Display_Order__c];
        CustomerValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where Time_Period__c  = :currentPeriod.Id 
                    and Category__c = 'Customer' order by Display_Order__c];
        DeliveryValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where Time_Period__c  = :currentPeriod.Id 
                    and Category__c = 'Delivery' order by Display_Order__c];
        EmployeeValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c
                    from KPI_Value__c where Time_Period__c  = :currentPeriod.Id 
                    and Category__c = 'Employee' order by Display_Order__c];
        return null;
    }    

    public PageReference doKPIValueWipe()
    {
        KPI_Value__c[] allValues = [SELECT Id FROM KPI_Value__c];
        Database.DeleteResult[] result = Database.delete(allValues, false);
        System.debug(result.size() + ' records deleted');
        return null;
    }
    
    public PageReference doKPIWipe()
    {
        KPI__c[] allValues = [SELECT Id FROM KPI__c];
        Database.DeleteResult[] result = Database.delete(allValues, false);
        System.debug(result.size() + ' records deleted');
        return null;
    }
    
    public PageReference doKPIValueGeneration()
    {
        List<KPI_Value__c> values = new List<KPI_Value__c>();
        KPI__c[] allKPIs = [SELECT Id FROM KPI__c where Is_Active__c = true];
        
        for (pse__Time_Period__c period : periods)
        {
            for (KPI__c kpi : allKPIs)
            {
                KPI_Value__c value = new KPI_Value__c();
                value.Time_Period__c = period.Id;
                value.KPI__c = kpi.Id;
                value.Actual__c = generateValue();
                value.Target__c = generateValue();
                values.add(value);
            }
        }
        
        Database.SaveResult[] result = Database.insert(values);
        System.debug(result.size() + ' records added');
        return null;
        
    }
    
    private Double generateValue()
    {
        Decimal value = 10.0 + (Math.random() * 30.0);
        return value;
    }
}