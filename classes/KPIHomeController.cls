/*
Apex controller for KPI Home page.  This controller provides values for the previous
fiscal quarter and the current fiscal year.

Plans for expansion include the ability to select another quarter to display to 
enable a user to look at historical quarterly or annual data.
*/
public with sharing class KPIHomeController {

	public pse__Time_Period__c PreviousQuarter { get; protected set; }
	public pse__Time_Period__c CurrentYear { get; protected set; }
	public List<KPI_Value__c> FinancialValues { get; protected set; }
	public List<KPI_Value__c> CustomerValues { get; protected set; }
	public List<KPI_Value__c> DeliveryValues { get; protected set; }
	public List<KPI_Value__c> EmployeeValues { get; protected set; }
	public boolean HasFinancialValues { get; protected set; }
	public boolean HasCustomerValues { get; protected set; }
	public boolean HasDeliveryValues { get; protected set; }
	public boolean HasEmployeeValues { get; protected set; }

	public KPIHomeController() {
		PreviousQuarter = getPreviousQuarter();
		CurrentYear = getCurrentYear();
		getValues();
	}

	public void getValues() {
        FinancialValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where (Time_Period__c  = :PreviousQuarter.Id 
                	or Time_Period__c  = :CurrentYear.Id)
                    and Category__c = 'Financial' order by Display_Order__c];
        CustomerValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where (Time_Period__c  = :PreviousQuarter.Id 
                	or Time_Period__c  = :CurrentYear.Id)
                    and Category__c = 'Customer' order by Display_Order__c];
        DeliveryValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where (Time_Period__c  = :PreviousQuarter.Id 
                	or Time_Period__c  = :CurrentYear.Id)
                    and Category__c = 'Delivery' order by Display_Order__c];
        EmployeeValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c
                    from KPI_Value__c where (Time_Period__c  = :PreviousQuarter.Id 
                	or Time_Period__c  = :CurrentYear.Id)
                    and Category__c = 'Employee' order by Display_Order__c];

        HasFinancialValues = !FinancialValues.isEmpty();
        HasCustomerValues = !CustomerValues.isEmpty();
        HasDeliveryValues = !DeliveryValues.isEmpty();
        HasEmployeeValues = !EmployeeValues.isEmpty();
	}

    public static pse__Time_Period__c getPreviousQuarter() {
	    List<pse__Time_Period__c> quarters = [SELECT Id, Name from 
	            pse__Time_Period__c where pse__Type__c = 'Quarter' and 
	            pse__Start_Date__c <= today order by pse__Start_Date__c DESC LIMIT 2];
	    System.debug('Previous Quarter: ' + quarters[1]);
	    return quarters[1];
    }

    public static pse__Time_Period__c getCurrentYear() {
	    pse__Time_Period__c currYear = [SELECT Id, Name from 
	            pse__Time_Period__c where pse__Type__c = 'Year' and 
	            pse__Start_Date__c <= today and pse__End_Date__c >= today LIMIT 1];
	    System.debug('Current Year: ' + currYear);
	    return currYear;
    }
}