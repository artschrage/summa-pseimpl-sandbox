public with sharing class DataFactoryTestTimecards {

	private static list<pse__Timecard_Header__c> headers;
	private static list<pse__Timecard__c> splits;
	
	static {
		headers = new list<pse__Timecard_Header__c>();
		splits = new list<pse__Timecard__c>();
	}
	
	public static void createTimecards() {
		createHeaders();
		createSplits(false);
	}
	
	public static void createHeaders() {
		list<pse__Assignment__c> assignments = DataFactoryTestAssignments.getAssignments();
		
		pse__Timecard_Header__c tch;

		Date pStart, pEnd;
		Date upperBound, lowerBound;
		Date workingDate;

		integer i, dayOfWeek;
		
		//for each assignment, create timecard headers that cover the duration of the assignment
	   	for (pse__Assignment__c assignment : assignments) {
			pStart = assignment.pse__Start_Date__c;
			pEnd = assignment.pse__End_Date__c;

			system.debug('pStart = ' + pStart);
			system.debug('pEnd = ' + pEnd);

			//find the assignment start/end boundaries normalized to the start of the relative week
			lowerBound = pStart.toStartOfWeek();
			upperBound = pEnd.toStartOfWeek() + 7;

			system.debug('lowerBound = ' + lowerBound);
			system.debug('upperBound = ' + upperBound);

			//reset the assignment day range counter to 0
			//modulus 7 will give us the day of the week where 0 => Sunday and 6 => Saturday
			i = 0;

			while (true) {
				workingDate = lowerBound.addDays(i);
				dayOfWeek = math.mod(i,7);
				
				system.debug('Iteration ' + i);
				system.debug('Working Date = ' + workingDate);
				system.debug('dayOfWeek = ' + dayOfWeek);
	
				//if at the beginning of the week...
				if (dayOfWeek == 0) {
					//if there was a timecard for last week...
					if (tch != null) {
						headers.add(tch);
					}
					//if we've now exceeded the assignment range, pull the eject lever!
					if (workingDate >= upperBound) {
						break;
					}
					
					tch = new pse__Timecard_Header__c();
					
					tch.pse__Project__c = assignment.pse__Project__c;
					tch.pse__Assignment__c = assignment.Id;
					tch.pse__Resource__c = assignment.pse__Resource__c;
					tch.pse__Billable__c = assignment.pse__Is_Billable__c;
					tch.pse__Cost_Rate_Amount__c = assignment.pse__Cost_Rate_Amount__c;
					tch.pse__Cost_Rate_Currency_Code__c = assignment.pse__Cost_Rate_Currency_Code__c;
					tch.pse__Start_Date__c = Date.newInstance(workingDate.year(), workingDate.month(), workingDate.day());
					tch.pse__End_Date__c = Date.newInstance(workingDate.year(), workingDate.month(), workingDate.day()) + 6;
					tch.pse__Submitted__c = true;
					tch.pse__Approved__c = true;
					tch.pse__Approver__c = UserInfo.getUserId();
					tch.pse__Status__c = 'Approved';
					
					tch.pse__Sunday_Hours__c = 0;
				} else if (dayOfWeek == 1) {
					tch.pse__Monday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 2) {
					tch.pse__Tuesday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 3) {
					tch.pse__Wednesday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 4) {
					tch.pse__Thursday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 5) {
					tch.pse__Friday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 6) {
					tch.pse__Saturday_Hours__c = 0;
				}
				
				i++;
			}
    	}

		for (pse__Timecard_Header__c x : headers) {
			system.debug('Before timecard header insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; approver='+ x.pse__Approver__c +';');
		}

		insert headers;
		
		for (pse__Timecard_Header__c x : headers) {
			system.debug('After timecard header insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; approver='+ x.pse__Approver__c +';');
		}
    }
    
    public static void createSplits(boolean loadHeaders) {
    	pse__Timecard__c split, split1, split2;
    	
    	if (loadHeaders) {
    		headers.clear();
    		headers = getHeaders();
    	}
    	
    	for (pse__Timecard_Header__c header : headers) {
			if (header.pse__Start_Date__c.month() < header.pse__End_Date__c.month()) {
				split1 = new pse__Timecard__c();
				split2 = new pse__Timecard__c();

				split1.pse__Timecard_Header__c = header.Id;
				split1.pse__Project__c = header.pse__Project__c;
				split1.pse__Resource__c = header.pse__Resource__c;
				split1.pse__Assignment__c = header.pse__Assignment__c;
				split1.pse__Submitted__c = header.pse__Submitted__c;
				split1.pse__Approved__c = header.pse__Approved__c;
				split1.pse__Billable__c = header.pse__Billable__c;
				split1.pse__Status__c = header.pse__Status__c;

				split2.pse__Timecard_Header__c = header.Id;
				split2.pse__Project__c = header.pse__Project__c;
				split2.pse__Resource__c = header.pse__Resource__c;
				split2.pse__Assignment__c = header.pse__Assignment__c;
				split2.pse__Submitted__c = header.pse__Submitted__c;
				split2.pse__Approved__c = header.pse__Approved__c;
				split2.pse__Billable__c = header.pse__Billable__c;
				split2.pse__Status__c = header.pse__Status__c;

				split1.pse__Start_Date__c = header.pse__Start_Date__c;
				split2.pse__End_Date__c = header.pse__End_Date__c;

				split1.pse__Sunday_Hours__c = header.pse__Sunday_Hours__c;
				
				if (header.Monday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Monday_Hours__c = header.pse__Monday_Hours__c;
					split2.pse__Monday_Hours__c = 0;
				} else {
					split1.pse__Monday_Hours__c = 0;
					split2.pse__Monday_Hours__c = header.pse__Monday_Hours__c;
					
					split1.pse__End_Date__c = header.Sunday_Date__c;
					split2.pse__Start_Date__c = header.Monday_Date__c;
				}

				if (header.Tuesday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Tuesday_Hours__c = header.pse__Tuesday_Hours__c;
					split2.pse__Tuesday_Hours__c = 0;
				} else {
					split1.pse__Tuesday_Hours__c = 0;
					split2.pse__Tuesday_Hours__c = header.pse__Tuesday_Hours__c;
					
					split1.pse__End_Date__c = header.Monday_Date__c;
					split2.pse__Start_Date__c = header.Tuesday_Date__c;
				}

				if (header.Wednesday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Wednesday_Hours__c = header.pse__Wednesday_Hours__c;
					split2.pse__Wednesday_Hours__c = 0;
				} else {
					split1.pse__Wednesday_Hours__c = 0;
					split2.pse__Wednesday_Hours__c = header.pse__Wednesday_Hours__c;
					
					split1.pse__End_Date__c = header.Tuesday_Date__c;
					split2.pse__Start_Date__c = header.Wednesday_Date__c;
				}

				if (header.Thursday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Thursday_Hours__c = header.pse__Thursday_Hours__c;
					split2.pse__Thursday_Hours__c = 0;
				} else {
					split1.pse__Thursday_Hours__c = 0;
					split2.pse__Thursday_Hours__c = header.pse__Thursday_Hours__c;
					
					split1.pse__End_Date__c = header.Wednesday_Date__c;
					split2.pse__Start_Date__c = header.Thursday_Date__c;
				}

				if (header.Friday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Friday_Hours__c = header.pse__Friday_Hours__c;
					split2.pse__Friday_Hours__c = 0;
				} else {
					split1.pse__Friday_Hours__c = 0;
					split2.pse__Friday_Hours__c = header.pse__Friday_Hours__c;
					
					split1.pse__End_Date__c = header.Thursday_Date__c;
					split2.pse__Start_Date__c = header.Friday_Date__c;
				}

				split2.pse__Saturday_Hours__c = header.pse__Saturday_Hours__c;
				if (split1.pse__End_Date__c == null) {
					split1.pse__End_Date__c = header.Friday_Date__c;
					split2.pse__Start_Date__c = header.Saturday_Date__c;
				}
				
				split1.pse__Total_Cost__c = (split1.pse__Sunday_Hours__c + split1.pse__Monday_Hours__c + split1.pse__Tuesday_Hours__c + split1.pse__Wednesday_Hours__c + split1.pse__Thursday_Hours__c + split1.pse__Friday_Hours__c + split1.pse__Saturday_Hours__c) * header.pse__Cost_Rate_Amount__c;
				split2.pse__Total_Cost__c = (split2.pse__Sunday_Hours__c + split2.pse__Monday_Hours__c + split2.pse__Tuesday_Hours__c + split2.pse__Wednesday_Hours__c + split2.pse__Thursday_Hours__c + split2.pse__Friday_Hours__c + split2.pse__Saturday_Hours__c) * header.pse__Cost_Rate_Amount__c;
				
				splits.add(split1);
				splits.add(split2);
			} else {
				split = new pse__Timecard__c();
				
				split.pse__Timecard_Header__c = header.Id;
				split.pse__Project__c = header.pse__Project__c;
				split.pse__Resource__c = header.pse__Resource__c;
				split.pse__Assignment__c = header.pse__Assignment__c;
				split.pse__Submitted__c = header.pse__Submitted__c;
				split.pse__Approved__c = header.pse__Approved__c;
				split.pse__Billable__c = header.pse__Billable__c;
				split.pse__Status__c = header.pse__Status__c;
				
				split.pse__Start_Date__c = header.pse__Start_Date__c;
				split.pse__End_Date__c = header.pse__End_Date__c;

				split.pse__Sunday_Hours__c = header.pse__Sunday_Hours__c;
				split.pse__Monday_Hours__c = header.pse__Monday_Hours__c;
				split.pse__Tuesday_Hours__c = header.pse__Tuesday_Hours__c;				
				split.pse__Wednesday_Hours__c = header.pse__Wednesday_Hours__c;
				split.pse__Thursday_Hours__c = header.pse__Thursday_Hours__c;
				split.pse__Friday_Hours__c = header.pse__Friday_Hours__c;				
				split.pse__Saturday_Hours__c = header.pse__Saturday_Hours__c;				

				split.pse__Total_Cost__c = (split.pse__Sunday_Hours__c + split.pse__Monday_Hours__c + split.pse__Tuesday_Hours__c + split.pse__Wednesday_Hours__c + split.pse__Thursday_Hours__c + split.pse__Friday_Hours__c + split.pse__Saturday_Hours__c) * header.pse__Cost_Rate_Amount__c;

				splits.add(split);
			}
    	}

		for (pse__Timecard__c x : splits) {
			system.debug('Before timecard split insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; sunday='+ x.pse__Sunday_Hours__c +'; monday='+ x.pse__Monday_Hours__c +'; tuesday='+ x.pse__Tuesday_Hours__c +'; wednesday='+ x.pse__Wednesday_Hours__c +'; thursday='+ x.pse__Thursday_Hours__c +'; friday='+ x.pse__Friday_Hours__c +'; saturday='+ x.pse__Saturday_Hours__c + ';');
		}

		insert splits;
		
		for (pse__Timecard__c x : splits) {
			system.debug('After timecard split insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; sunday='+ x.pse__Sunday_Hours__c +'; monday='+ x.pse__Monday_Hours__c +'; tuesday='+ x.pse__Tuesday_Hours__c +'; wednesday='+ x.pse__Wednesday_Hours__c +'; thursday='+ x.pse__Thursday_Hours__c +'; friday='+ x.pse__Friday_Hours__c +'; saturday='+ x.pse__Saturday_Hours__c + ';');
		}
    }
	
	public static void createHeaders(string assignmentId) {
		headers.clear();

		pse__Assignment__c assignment = DataFactoryTestAssignments.getAssignment(assignmentId);
		
		pse__Timecard_Header__c tch;

		Date pStart, pEnd;
		Date upperBound, lowerBound;
		Date workingDate;

		integer i, dayOfWeek;
		
		pStart = assignment.pse__Start_Date__c;
		pEnd = assignment.pse__End_Date__c;

		system.debug('pStart = ' + pStart);
		system.debug('pEnd = ' + pEnd);

		//find the assignment start/end boundaries normalized to the start of the relative week
		lowerBound = pStart.toStartOfWeek();
		upperBound = pEnd.toStartOfWeek() + 7;

		system.debug('lowerBound = ' + lowerBound);
		system.debug('upperBound = ' + upperBound);

		//reset the assignment day range counter to 0
		//modulus 7 will give us the day of the week where 0 => Sunday and 6 => Saturday
		i = 0;

		while (true) {
			workingDate = lowerBound.addDays(i);
			dayOfWeek = math.mod(i,7);
			
			system.debug('Iteration ' + i);
			system.debug('Working Date = ' + workingDate);
			system.debug('dayOfWeek = ' + dayOfWeek);
				
			//if at the beginning of the week...
			if (dayOfWeek == 0) {
				//if there was a timecard for last week...
				if (tch != null) {
					headers.add(tch);
				}
				//if we've now exceeded the assignment range, pull the eject lever!
				if (workingDate >= upperBound) {
					break;
				}
					
				tch = new pse__Timecard_Header__c();
					
				tch.pse__Project__c = assignment.pse__Project__c;
				tch.pse__Assignment__c = assignment.Id;
				tch.pse__Resource__c = assignment.pse__Resource__c;
				tch.pse__Billable__c = assignment.pse__Is_Billable__c;
				tch.pse__Cost_Rate_Amount__c = assignment.pse__Cost_Rate_Amount__c;
				tch.pse__Cost_Rate_Currency_Code__c = assignment.pse__Cost_Rate_Currency_Code__c;
				tch.pse__Start_Date__c = Date.newInstance(workingDate.year(), workingDate.month(), workingDate.day());
				tch.pse__End_Date__c = Date.newInstance(workingDate.year(), workingDate.month(), workingDate.day()) + 6;
				tch.pse__Submitted__c = true;
				tch.pse__Approved__c = true;
				tch.pse__Approver__c = UserInfo.getUserId();
				tch.pse__Status__c = 'Approved';
					
				tch.pse__Sunday_Hours__c = 0;
			} else if (dayOfWeek == 1) {
				tch.pse__Monday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
			} else if (dayOfWeek == 2) {
				tch.pse__Tuesday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
			} else if (dayOfWeek == 3) {
				tch.pse__Wednesday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
			} else if (dayOfWeek == 4) {
				tch.pse__Thursday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
			} else if (dayOfWeek == 5) {
				tch.pse__Friday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
			} else if (dayOfWeek == 6) {
				tch.pse__Saturday_Hours__c = 0;
			}
			
			i++;
		}

		for (pse__Timecard_Header__c x : headers) {
			system.debug('Before timecard header insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; approver='+ x.pse__Approver__c +';');
		}

		insert headers;
		
		for (pse__Timecard_Header__c x : headers) {
			system.debug('After timecard header insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; approver='+ x.pse__Approver__c +';');
		}
    }
    
    public static void createSplits(string assignmentId) {
		splits.clear();

    	list<pse__Timecard_Header__c> localHeaders = getHeaders(assignmentId);
    	pse__Timecard__c split, split1, split2;
    	
    	for (pse__Timecard_Header__c header : localHeaders) {
			if (header.pse__Start_Date__c.month() < header.pse__End_Date__c.month()) {
				split1 = new pse__Timecard__c();
				split2 = new pse__Timecard__c();

				split1.pse__Timecard_Header__c = header.Id;
				split1.pse__Project__c = header.pse__Project__c;
				split1.pse__Resource__c = header.pse__Resource__c;
				split1.pse__Assignment__c = header.pse__Assignment__c;
				split1.pse__Submitted__c = header.pse__Submitted__c;
				split1.pse__Approved__c = header.pse__Approved__c;
				split1.pse__Billable__c = header.pse__Billable__c;
				split1.pse__Status__c = header.pse__Status__c;

				split2.pse__Timecard_Header__c = header.Id;
				split2.pse__Project__c = header.pse__Project__c;
				split2.pse__Resource__c = header.pse__Resource__c;
				split2.pse__Assignment__c = header.pse__Assignment__c;
				split2.pse__Submitted__c = header.pse__Submitted__c;
				split2.pse__Approved__c = header.pse__Approved__c;
				split2.pse__Billable__c = header.pse__Billable__c;
				split2.pse__Status__c = header.pse__Status__c;

				split1.pse__Start_Date__c = header.pse__Start_Date__c;
				split2.pse__End_Date__c = header.pse__End_Date__c;

				double split1TotalHrs = 0;
				double split2TotalHrs = 0;
				
				split1.pse__Sunday_Hours__c = header.pse__Sunday_Hours__c;
				split1TotalHrs = split1TotalHrs + header.pse__Sunday_Hours__c;
				
				if (header.Monday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Monday_Hours__c = header.pse__Monday_Hours__c;
					split2.pse__Monday_Hours__c = 0;

					split1TotalHrs = split1TotalHrs + header.pse__Monday_Hours__c;
				} else {
					split1.pse__Monday_Hours__c = 0;
					split2.pse__Monday_Hours__c = header.pse__Monday_Hours__c;
					
					split1.pse__End_Date__c = header.Sunday_Date__c;
					split2.pse__Start_Date__c = header.Monday_Date__c;

					split2TotalHrs = split2TotalHrs + header.pse__Monday_Hours__c;
				}

				if (header.Tuesday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Tuesday_Hours__c = header.pse__Tuesday_Hours__c;
					split2.pse__Tuesday_Hours__c = 0;

					split1TotalHrs = split1TotalHrs + header.pse__Tuesday_Hours__c;
				} else {
					split1.pse__Tuesday_Hours__c = 0;
					split2.pse__Tuesday_Hours__c = header.pse__Tuesday_Hours__c;
					
					split1.pse__End_Date__c = header.Monday_Date__c;
					split2.pse__Start_Date__c = header.Tuesday_Date__c;

					split2TotalHrs = split2TotalHrs + header.pse__Tuesday_Hours__c;
				}

				if (header.Wednesday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Wednesday_Hours__c = header.pse__Wednesday_Hours__c;
					split2.pse__Wednesday_Hours__c = 0;

					split1TotalHrs = split1TotalHrs + header.pse__Wednesday_Hours__c;
				} else {
					split1.pse__Wednesday_Hours__c = 0;
					split2.pse__Wednesday_Hours__c = header.pse__Wednesday_Hours__c;
					
					split1.pse__End_Date__c = header.Tuesday_Date__c;
					split2.pse__Start_Date__c = header.Wednesday_Date__c;

					split2TotalHrs = split2TotalHrs + header.pse__Wednesday_Hours__c;
				}

				if (header.Thursday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Thursday_Hours__c = header.pse__Thursday_Hours__c;
					split2.pse__Thursday_Hours__c = 0;

					split1TotalHrs = split1TotalHrs + header.pse__Thursday_Hours__c;
				} else {
					split1.pse__Thursday_Hours__c = 0;
					split2.pse__Thursday_Hours__c = header.pse__Thursday_Hours__c;
					
					split1.pse__End_Date__c = header.Wednesday_Date__c;
					split2.pse__Start_Date__c = header.Thursday_Date__c;

					split2TotalHrs = split2TotalHrs + header.pse__Thursday_Hours__c;
				}

				if (header.Friday_Date__c.month() == header.pse__Start_Date__c.month()) {
					split1.pse__Friday_Hours__c = header.pse__Friday_Hours__c;
					split2.pse__Friday_Hours__c = 0;

					split1TotalHrs = split1TotalHrs + header.pse__Friday_Hours__c;
				} else {
					split1.pse__Friday_Hours__c = 0;
					split2.pse__Friday_Hours__c = header.pse__Friday_Hours__c;

					split2TotalHrs = split2TotalHrs + header.pse__Friday_Hours__c;
					
					split1.pse__End_Date__c = header.Thursday_Date__c;
					split2.pse__Start_Date__c = header.Friday_Date__c;
				}

				split2.pse__Saturday_Hours__c = header.pse__Saturday_Hours__c;

				split2TotalHrs = split2TotalHrs + header.pse__Saturday_Hours__c;

				if (split1.pse__End_Date__c == null) {
					split1.pse__End_Date__c = header.Friday_Date__c;
					split2.pse__Start_Date__c = header.Saturday_Date__c;
				}
				
				split1.pse__Total_Cost__c = split1TotalHrs * header.pse__Cost_Rate_Amount__c;
				split2.pse__Total_Cost__c = split2TotalHrs * header.pse__Cost_Rate_Amount__c;

				splits.add(split1);
				splits.add(split2);
			} else {
				split = new pse__Timecard__c();
				
				split.pse__Timecard_Header__c = header.Id;
				split.pse__Project__c = header.pse__Project__c;
				split.pse__Resource__c = header.pse__Resource__c;
				split.pse__Assignment__c = header.pse__Assignment__c;
				split.pse__Submitted__c = header.pse__Submitted__c;
				split.pse__Approved__c = header.pse__Approved__c;
				split.pse__Billable__c = header.pse__Billable__c;
				split.pse__Status__c = header.pse__Status__c;
				
				split.pse__Start_Date__c = header.pse__Start_Date__c;
				split.pse__End_Date__c = header.pse__End_Date__c;

				split.pse__Sunday_Hours__c = header.pse__Sunday_Hours__c;
				split.pse__Monday_Hours__c = header.pse__Monday_Hours__c;
				split.pse__Tuesday_Hours__c = header.pse__Tuesday_Hours__c;				
				split.pse__Wednesday_Hours__c = header.pse__Wednesday_Hours__c;
				split.pse__Thursday_Hours__c = header.pse__Thursday_Hours__c;
				split.pse__Friday_Hours__c = header.pse__Friday_Hours__c;				
				split.pse__Saturday_Hours__c = header.pse__Saturday_Hours__c;				

				split.pse__Total_Cost__c = (header.pse__Sunday_Hours__c + header.pse__Monday_Hours__c + header.pse__Tuesday_Hours__c + header.pse__Wednesday_Hours__c + header.pse__Thursday_Hours__c + header.pse__Friday_Hours__c + header.pse__Saturday_Hours__c) * header.pse__Cost_Rate_Amount__c;

				splits.add(split);
			}
    	}

		for (pse__Timecard__c x : splits) {
			system.debug('Before timecard split insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; sunday='+ x.pse__Sunday_Hours__c +'; monday='+ x.pse__Monday_Hours__c +'; tuesday='+ x.pse__Tuesday_Hours__c +'; wednesday='+ x.pse__Wednesday_Hours__c +'; thursday='+ x.pse__Thursday_Hours__c +'; friday='+ x.pse__Friday_Hours__c +'; saturday='+ x.pse__Saturday_Hours__c + ';');
		}

		insert splits;
		
		for (pse__Timecard__c x : splits) {
			system.debug('After timecard split insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; sunday='+ x.pse__Sunday_Hours__c +'; monday='+ x.pse__Monday_Hours__c +'; tuesday='+ x.pse__Tuesday_Hours__c +'; wednesday='+ x.pse__Wednesday_Hours__c +'; thursday='+ x.pse__Thursday_Hours__c +'; friday='+ x.pse__Friday_Hours__c +'; saturday='+ x.pse__Saturday_Hours__c + ';');
		}
    }
	
	public static void createHeaderForProject(string projectId) {
		list<pse__Assignment__c> assignments = DataFactoryTestAssignments.getProjectAssignments(projectId);

		headers.clear();		
		pse__Timecard_Header__c tch;

		Date pStart, pEnd;
		Date upperBound, lowerBound;
		Date workingDate;

		integer i, dayOfWeek;
		
		//for each assignment, create timecard headers that cover the duration of the assignment
	   	for (pse__Assignment__c assignment : assignments) {
			pStart = assignment.pse__Start_Date__c;
			pEnd = assignment.pse__End_Date__c;

			system.debug('pStart = ' + pStart);
			system.debug('pEnd = ' + pEnd);

			//find the assignment start/end boundaries normalized to the start of the relative week
			lowerBound = pStart.toStartOfWeek();
			upperBound = pEnd.toStartOfWeek() + 7;

			system.debug('lowerBound = ' + lowerBound);
			system.debug('upperBound = ' + upperBound);

			//reset the assignment day range counter to 0
			//modulus 7 will give us the day of the week where 0 => Sunday and 6 => Saturday
			i = 0;

			while (true) {
				workingDate = lowerBound.addDays(i);
				dayOfWeek = math.mod(i,7);
				
				system.debug('Iteration ' + i);
				system.debug('Working Date = ' + workingDate);
				system.debug('dayOfWeek = ' + dayOfWeek);
	
				//if at the beginning of the week...
				if (dayOfWeek == 0) {
					//if there was a timecard for last week...
					if (tch != null) {
						headers.add(tch);
					}
					//if we've now exceeded the assignment range, pull the eject lever!
					if (workingDate >= upperBound) {
						break;
					}
					
					tch = new pse__Timecard_Header__c();
					
					tch.pse__Project__c = assignment.pse__Project__c;
					tch.pse__Assignment__c = assignment.Id;
					tch.pse__Resource__c = assignment.pse__Resource__c;
					tch.pse__Billable__c = assignment.pse__Is_Billable__c;
					tch.pse__Cost_Rate_Amount__c = assignment.pse__Cost_Rate_Amount__c;
					tch.pse__Cost_Rate_Currency_Code__c = assignment.pse__Cost_Rate_Currency_Code__c;
					tch.pse__Start_Date__c = Date.newInstance(workingDate.year(), workingDate.month(), workingDate.day());
					tch.pse__End_Date__c = Date.newInstance(workingDate.year(), workingDate.month(), workingDate.day()) + 6;
					tch.pse__Submitted__c = true;
					tch.pse__Approved__c = true;
					tch.pse__Approver__c = UserInfo.getUserId();
					tch.pse__Status__c = 'Approved';
					
					tch.pse__Sunday_Hours__c = 0;
				} else if (dayOfWeek == 1) {
					tch.pse__Monday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 2) {
					tch.pse__Tuesday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 3) {
					tch.pse__Wednesday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 4) {
					tch.pse__Thursday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 5) {
					tch.pse__Friday_Hours__c = (workingDate < pStart || workingDate > pEnd) ? 0 : .5;
				} else if (dayOfWeek == 6) {
					tch.pse__Saturday_Hours__c = 0;
				}
				
				i++;
			}
    	}

		for (pse__Timecard_Header__c x : headers) {
			system.debug('Before timecard header insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; approver='+ x.pse__Approver__c +';');
		}

		insert headers;
		
		for (pse__Timecard_Header__c x : headers) {
			system.debug('After timecard header insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +'; projectId='+ x.pse__Project__c +'; assignmentId='+ x.pse__Assignment__c +'; resourceId='+ x.pse__Resource__c +'; billable='+ x.pse__Billable__c +'; submitted='+ x.pse__Submitted__c +'; approved='+ x.pse__Approved__c +'; approver='+ x.pse__Approver__c +';');
		}
    }
    
    public static list<pse__Timecard_Header__c> getHeaders() {
    	if (headers.size() == 0) {
    		headers = [select id, name, pse__Project__c, pse__Assignment__c, pse__Resource__c, pse__Billable__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, pse__Status__c, pse__Start_Date__c, pse__End_Date__c, pse__Submitted__c, pse__Approved__c, pse__Approver__c, Sunday_Date__c, pse__Sunday_Hours__c, Monday_Date__c, pse__Monday_Hours__c, Tuesday_Date__c, pse__Tuesday_Hours__c, Wednesday_Date__c, pse__Wednesday_Hours__c, Thursday_Date__c, pse__Thursday_Hours__c, Friday_Date__c, pse__Friday_Hours__c, Saturday_Date__c, pse__Saturday_Hours__c from pse__Timecard_Header__c where pse__Project__c in :DataFactoryTestProjects.getProjectIds()];
    	}
    	
    	return headers;
    }
    
    public static list<pse__Timecard_Header__c> getHeaders(string assignmentId) {
		list<pse__Timecard_Header__c> returnValue = [select id, name, pse__Project__c, pse__Assignment__c, pse__Resource__c, pse__Billable__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, pse__Status__c, pse__Start_Date__c, pse__End_Date__c, pse__Submitted__c, pse__Approved__c, pse__Approver__c, Sunday_Date__c, pse__Sunday_Hours__c, Monday_Date__c, pse__Monday_Hours__c, Tuesday_Date__c, pse__Tuesday_Hours__c, Wednesday_Date__c, pse__Wednesday_Hours__c, Thursday_Date__c, pse__Thursday_Hours__c, Friday_Date__c, pse__Friday_Hours__c, Saturday_Date__c, pse__Saturday_Hours__c from pse__Timecard_Header__c where pse__Assignment__c = :assignmentId];

    	return returnValue;
    }
}