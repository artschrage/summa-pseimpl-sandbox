public class KPIAverageRateReportLogic implements KPIReportInterface {
    public KPI_Value__c execute(KPI_Value__c kpiValue) {
        System.debug('execute KPIAverageRateReportLogic');
        Decimal actual = 0.0;

        AggregateResult[] results = [SELECT 
              sum(pse__Total_Billable_Amount__c) totalbill,  
              sum(pse__Total_Hours__c) totalhrs 
              FROM pse__Timecard_Header__c 
                      WHERE pse__Billable__c = true AND 
                      pse__Total_Hours__c > 0 AND 
                      pse__Total_Billable_Amount__c > 0 AND 
                      pse__Project__r.pse__Practice__r.Name != '' AND 
                      pse__Billable__c = TRUE];

        for (AggregateResult result : results) {
            Decimal totalbill = (Decimal) result.get('totalbill');
            Decimal totalhrs = (Decimal) result.get('totalhrs');
            System.debug('Total Billings: ' + totalbill);
            System.debug('Total Hours: ' + totalhrs);
            
            if (totalhrs == 0.0) {
              actual = 0.0;
            }
            else {
              actual = totalbill / totalhrs;
            }
            System.debug('Average Billing Rate: ' + actual);
        }

        kpiValue.Actual__c = actual;
        System.debug(kpiValue);
        return kpiValue;
    }
}