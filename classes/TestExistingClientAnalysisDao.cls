/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class TestExistingClientAnalysisDao {

	private static pse__Region__c regionCorporate;
	private static pse__Practice__c practiceCore;
	private static Contact johnSmail;
	private static Account account;

    static testMethod void testFetchWonOpportunities() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);
		
		Test.startTest();
		
		List<Opportunity> opps = dao.fetchWonOpportunities(accountIds, startWindow);
		for (Opportunity opp : opps) {
			system.debug('dao.fetchWonOpportunities returned ['+ opp.Name +']');
		}
		system.assertNotEquals(null, opps, 'Expected Opportunities to be returned. Got none.');
		system.assertEquals(4, opps.size(), 'Expected 4 Opportunities to be returned. Got ' + opps.size() + '.');
		
		Test.stopTest();
    }
    
    static testMethod void testFetchNewClientOpportunitiesExpectNone() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);
		
		//stage an opportunity prior to the analysis window to confirm it is not caught
		Opportunity o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp New Client Before Window';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow-10;
		o.Start_Date__c = startWindow;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		o.Is_New_Client_Opp__c = True;
		insert o;
		
		Test.startTest();
		
		List<Opportunity> opps = dao.fetchNewClientOpportunities(accountIds, startWindow);
		for (Opportunity opp : opps) {
			system.debug('dao.fetchNewClientOpportunities returned ['+ opp.Name +']');
		}
		system.assertNotEquals(null, opps, 'Expected a List object even though no Opportunities to be returned, but got null instead.');
		system.assertEquals(0, opps.size(), 'Expected 0 Opportunities to be returned. Got ' + opps.size() + '.');

		Test.stopTest();
    }
    
    static testMethod void testFetchNewClientOpportunitiesExpectOne() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);
		
		//stage an opportunity prior to the analysis window to confirm it is not caught
		Opportunity o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp New Client In Window';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow+10;
		o.Start_Date__c = startWindow+11;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		o.Is_New_Client_Opp__c = True;
		insert o;
		
		Test.startTest();
		
		List<Opportunity> opps = dao.fetchNewClientOpportunities(accountIds, startWindow);
		for (Opportunity opp : opps) {
			system.debug('dao.fetchNewClientOpportunities returned ['+ opp.Name +']');
		}
		system.assertNotEquals(null, opps, 'Expected Opportunities to be returned, but got none instead.');
		system.assertEquals(1, opps.size(), 'Expected 1 Opportunities to be returned. Got ' + opps.size() + '.');
		
		Test.stopTest();
    }
    
    static testMethod void testFetchNewClientValuesExpectNone() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);
		
		delete [select id from New_Client_Value__c];
		
		Test.startTest();
		
		List<New_Client_Value__c> ncvs = dao.fetchNewClientValues(accountIds, startWindow);
		
		for (New_Client_Value__c ncv : ncvs) {
			system.debug('dao.fetchNewClientValues returned ['+ ncv.Name +']');
		}
		system.assertNotEquals(null, ncvs, 'Expected Opportunities to be returned, but got none instead.');
		system.assertEquals(0, ncvs.size(), 'Expected 0 Opportunities to be returned. Got ' + ncvs.size() + '.');
		
		Test.stopTest();
    }
    
    static testMethod void testFetchNewClientValuesExpectOne() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);
		
		delete [select id from New_Client_Value__c];
		stageNewClientValues(startWindow.addYears(1));
		
		Test.startTest();
		
		List<New_Client_Value__c> ncvs = dao.fetchNewClientValues(accountIds, startWindow);
		
		for (New_Client_Value__c ncv : ncvs) {
			system.debug('dao.fetchNewClientValues returned ['+ ncv.Name +']');
		}
		system.assertNotEquals(null, ncvs, 'Expected Opportunities to be returned, but got none instead.');
		system.assertEquals(1, ncvs.size(), 'Expected 1 Opportunities to be returned. Got ' + ncvs.size() + '.');
		
		Test.stopTest();
    }

    static testMethod void testStoreOpportunityChanges() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);

    	Test.startTest();

		List<Opportunity> opps = [select Id, Name, Is_New_Client_Opp__c from Opportunity where Name = 'Opp Closed-Won On Window Boundary'];
		try {
			opps[0].Is_New_Client_Opp__c = True;
			
			dao.storeOpportunityChanges(opps);
			
			List<Opportunity> opps2 = [select Id, Name, Is_New_Client_Opp__c from Opportunity where Name = 'Opp Closed-Won On Window Boundary'];
			system.assert(opps2[0].Is_New_Client_Opp__c, 'Expected Is_New_Client_Opp__c value after refetch to be true.');
		} catch (Exception xcpn) {
			system.assert(false, xcpn.getMessage());
		}
		
		Test.stopTest();
    }
    
    static testMethod void testStoreNewClientValueChanges() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);

    	Test.startTest();

		Date newEndDate = Date.today();
		List<New_Client_Value__c> ncvs = [select Id, Account__c, Start_Date__c, End_Date__c from New_Client_Value__c];
		
		try {
			ncvs[0].End_Date__c = newEndDate;
			
			dao.storeNewClientValueChanges(ncvs);
			
			List<New_Client_Value__c> ncvs2 = [select Id, Account__c, Start_Date__c, End_Date__c from New_Client_Value__c];
			system.assertEquals(newEndDate, ncvs2[0].End_Date__c, 'Expected End_Date__c value after refetch to be '+ newEndDate +'.');
		} catch (Exception xcpn) {
			system.assert(false, xcpn.getMessage());
		}
		
		Test.stopTest();
    }
    
    static testMethod void testStoreOpportunityChangesFailure() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);

    	Test.startTest();

		List<Opportunity> opps = new List<Opportunity>();
		opps.add(new Opportunity());
		try {
			dao.storeOpportunityChanges(opps);
			system.assert(false, 'Expected dao.storeOpportunityChanges to throw an exception');
		} catch (Exception xcpn) {
			system.debug('dao.storeOpportunityChanges threw exception as expected. Exception throw: ' + xcpn.getMessage());
			system.assert(true);
		}
		
		Test.stopTest();
    }
    
    static testMethod void testStoreNewClientValueChangesFailure() {
    	Date startWindow = getDataStageDate();
		stageData(startWindow);

		ExistingClientAnalysisDao dao = new ExistingClientAnalysisDao();
		
		Set<Id> accountIds = new Set<Id>();
		accountIds.add(account.Id);

    	Test.startTest();

		Date newEndDate = Date.today();
		List<New_Client_Value__c> ncvs = new List<New_Client_Value__c>();
		ncvs.add(new New_Client_Value__c(Start_Date__c=newEndDate-10, End_Date__c=newEndDate));
		
		try {
			dao.storeNewClientValueChanges(ncvs);
			
			system.assert(false, 'Expected dao.storeNewClientValueChanges to throw an exception');
		} catch (Exception xcpn) {
			system.debug('dao.storeNewClientValueChanges threw exception as expected. Exception throw: ' + xcpn.getMessage());
			system.assert(true);
		}
		
		Test.stopTest();
    }
    
    private static void stageData(Date startWindow) {
    	stagePrerequisites();
    	
    	stageOpportunities(startWindow);
    	stageNewClientValues(startWindow);
    }

	private static Date getDataStageDate() {
    	Date now = Date.today();
		if (now.month() >= 7) {
			return Date.newInstance(now.year()-2, 7, 1);
		} else {
			return Date.newInstance(now.year()-3, 7, 1);
		}
	}
	private static void stagePrerequisites() {
    	//region
    	regionCorporate = new pse__Region__c(Name='Corporate', CurrencyIsoCode='USD');
    	insert regionCorporate;
    	
    	//practice
    	List<pse__Practice__c> practices = new List<pse__Practice__c>();
    	
    	practiceCore = new pse__Practice__c(Name='Core', CurrencyIsoCode='USD');
    	practices.add(practiceCore);
    	//need these additional to keep the UpdateOpportunityPractice trigger happy.
    	practices.add(new pse__Practice__c(Name='CRM', CurrencyIsoCode='USD'));
    	practices.add(new pse__Practice__c(Name='SMM', CurrencyIsoCode='USD'));

    	insert practices;
    	
    	//contact
    	String currentUserId = UserInfo.getUserId();
    	johnSmail = new Contact(FirstName='John', LastName='Smail', CurrencyIsoCode='USD', pse__Is_Resource__c=TRUE, pse__Is_Resource_Active__c=TRUE, pse__Region__c=regionCorporate.Id, pse__Practice__c=practiceCore.Id, pse__Salesforce_User__c=currentUserId);
    	insert johnSmail;
    	
    	pse__Permission_Control__c permission = new pse__Permission_Control__c(
	    	pse__Billing__c=TRUE,
			pse__Cascading_Permission__c=TRUE,
			pse__Expense_Entry__c=TRUE,
			pse__Expense_Ops_Edit__c=TRUE,
			pse__Forecast_Edit__c=TRUE,
			pse__Forecast_View__c=TRUE,
			pse__Invoicing__c=TRUE,
			pse__Region__c=regionCorporate.Id,
			pse__Resource_Request_Entry__c=TRUE,
			pse__Staffing__c=TRUE,
			pse__Timecard_Entry__c=TRUE,
			pse__Timecard_Ops_Edit__c=TRUE,
			pse__User__c=currentUserId);
		insert permission;
		    	
    	//account
    	account = new Account(Name='TestExistingClientAnalysisDao Account A', CurrencyIsoCode='USD');
		insert account;
	}
	
	private static void stageOpportunities(Date startWindow) {
    	List<Opportunity> newOpps = new List<Opportunity>();
		Opportunity o = null;
		
		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Closed-Won Before Window';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow-1;
		o.Start_Date__c = startWindow;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);
		
		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Pipeline Before Window';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow-1;
		o.Start_Date__c = startWindow;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Pipeline';
		o.StageName = 'Develop';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);

		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Closed-Won On Window Boundary';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow;
		o.Start_Date__c = startWindow+1;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);
		
		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Pipeline On Window Boundary';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow;
		o.Start_Date__c = startWindow+1;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Pipeline';
		o.StageName = 'Develop';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);

		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Closed-Won In Window Before Threshold';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow.addYears(1)-1;
		o.Start_Date__c = startWindow.addYears(1);
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);
		
		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Pipeline In Window Before Threshold';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow.addYears(1)-1;
		o.Start_Date__c = startWindow.addYears(1);
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Pipeline';
		o.StageName = 'Develop';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);

		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Closed-Won In Window On Threshold';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow.addYears(1);
		o.Start_Date__c = startWindow.addYears(1)+1;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);
		
		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Pipeline In Window On Threshold';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow.addYears(1);
		o.Start_Date__c = startWindow.addYears(1)+1;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Pipeline';
		o.StageName = 'Develop';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);

		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Closed-Won In Window After Threshold';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow.addYears(1)+1;
		o.Start_Date__c = startWindow.addYears(1)+2;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Closed';
		o.StageName = 'Won';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);
		
		o = new Opportunity();
		o.AccountId = account.Id;
		o.Name = 'Opp Pipeline In Window After Threshold';
		o.pse__Practice__c = practiceCore.Id;
		o.CloseDate = startWindow.addYears(1)+1;
		o.Start_Date__c = startWindow.addYears(1)+2;
		o.pse__Region__c = regionCorporate.Id;
		o.ForecastCategoryName = 'Pipeline';
		o.StageName = 'Develop';
		o.Amount = 1000;
		o.Primary_Solution_Area__c = 'Application Modernization';
		newOpps.add(o);

		insert newOpps;
	}
	
	private static void stageNewClientValues(Date startWindow) {
		List<New_Client_Value__c> newNcvs = new List<New_Client_Value__c>();

		New_Client_Value__c ncv = new New_Client_Value__c();
		ncv.Account__c = account.Id;
		ncv.Start_Date__c = startWindow;
		ncv.End_Date__c = startWindow.addYears(1);
		newNcvs.add(ncv);
		
		insert newNcvs;		
	}
}