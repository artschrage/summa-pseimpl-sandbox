@isTest
private class KPIAverageRateTest { 
    static KPI__c kpi;
    static KPI_Value__c kpiValue;
    static pse__Time_Period__c timePeriod;
    static Account account;
    static Contact contact;
    static pse__Practice__c practice;
    static pse__Proj__c project;
    static pse__Region__c region;
    static pse__Permission_Control__c permission;
    static pse__Schedule__c schedule;
    static pse__Assignment__c assignment;
    static pse__Timecard_Header__c header;

    static {
        createTimePeriod();
        createKPIAndValue();
        createPractice();
        createProject();
        createTimecardHeaders();
    }

	static testMethod void averageRateTest() {
        KPIAverageRateReportLogic logic = new KPIAverageRateReportLogic();
        logic.execute(kpiValue);
        System.debug(kpiValue);
		System.assert(kpiValue.Actual__c == 100.0);
	}

    static void createTimePeriod() {
        Date now = Date.today(); 
        decimal thisQuarterMonth = Decimal.valueOf(now.month());
        thisQuarterMonth = (math.ceil(thisQuarterMonth.divide(3,2))*3)-2;

        Date startCurrentQuarter = Date.newInstance(now.year(), 
                                Integer.valueOf(thisQuarterMonth), 1);
        Date startPriorQuarter = startCurrentQuarter.addMonths(-3);
        
        system.debug('startCurrentQuarter = ' + startCurrentQuarter);
        system.debug('startPriorQuarter = ' + startPriorQuarter);

        timePeriod = 
                new pse__Time_Period__c(Name = 'Last Quarter', 
                pse__Type__c = 'Quarter', 
                pse__Start_Date__c = startPriorQuarter, 
                pse__End_Date__c = startCurrentQuarter.addDays(-1));
        insert timePeriod;
        timePeriod = [SELECT Id, pse__Start_Date__c, pse__End_Date__c from 
                                    pse__Time_Period__c where Name = 'Last Quarter'];
        System.debug('TimePeriod: ' + timePeriod);
    }

    static void createKPIAndValue() {
        kpi = new KPI__c();
        kpi.Name = 'Average Rate';
        insert kpi;
        System.debug('KPI: ' + kpi);
        
        kpi = [SELECT Id from KPI__c where Name = 'Average Rate'];
        System.debug('KPI ID = ' + kpi.Id);

        kpiValue = new KPI_Value__c();
        kpiValue.KPI__c = kpi.Id;
        kpiValue.Time_Period__c = timePeriod.Id;
        insert kpiValue;
        System.debug('KPI Value: ' + kpiValue);
    }

    static void createPractice() {
        practice = new pse__Practice__c(Name = 'Core');
        insert practice;
        System.debug('Practice: ' + practice);
    }

    static void createProject() {

        region = new pse__Region__c(Name='Corporate', CurrencyIsoCode='USD');
        insert region;
        region = [SELECT Id FROM pse__Region__c where Name='Corporate'];
        System.debug(region);

        account = new Account(Name='Account');
        insert account;
        account = [SELECT Id FROM Account where Name = 'Account'];

        project = new pse__Proj__c();
        project.Name = 'Project 1';
        project.pse__Account__c = account.Id;
        project.pse__Is_Active__c = true; 
        project.pse__Is_Billable__c = true;
        project.pse__Billing_Type__c = 'Time and Materials';
        project.pse__Closed_for_Time_Entry__c = false;
        project.pse__Start_Date__c = Date.newInstance(timePeriod.pse__Start_Date__c.year(), 
                                timePeriod.pse__Start_Date__c.month(), timePeriod.pse__Start_Date__c.day());
        project.pse__End_Date__c = Date.newInstance(timePeriod.pse__Start_Date__c.year(), 
                            timePeriod.pse__Start_Date__c.month(), timePeriod.pse__Start_Date__c.day()) + 6;
        project.Date_Became_Inactive__c = null;
        project.pse__Region__c = region.Id;
        project.pse__Practice__c = practice.Id;

        insert project;
        System.debug('Project: ' + project);
     }

    static void createTimecardHeaders() {
        contact = new Contact(FirstName='Test', LastName='Contact');
        contact.Account = account;
        contact.pse__Region__c = region.Id;
        contact.pse__Practice__c = practice.Id;
        contact.pse__Is_Resource__c = true;
        contact.pse__Is_Resource_Active__c = true;
        contact.pse__Salesforce_User__c = UserInfo.getUserId();
        insert contact;

        contact = [SELECT Id FROM Contact WHERE LastName = 'Contact'];
        System.debug(contact);

        permission = new pse__Permission_Control__c(
            pse__Billing__c = TRUE,
            pse__Cascading_Permission__c = TRUE,
            pse__Expense_Entry__c = TRUE,
            pse__Expense_Ops_Edit__c = TRUE,
            pse__Forecast_Edit__c = TRUE,
            pse__Forecast_View__c = TRUE,
            pse__Invoicing__c = TRUE,
            pse__Region__c = region.Id,
            pse__Resource_Request_Entry__c = TRUE,
            pse__Staffing__c = TRUE,
            pse__Timecard_Entry__c = TRUE,
            pse__Timecard_Ops_Edit__c = TRUE,
            pse__User__c = UserInfo.getUserId());
        insert permission;
        System.debug('Permission: ' + permission);

        schedule = new pse__Schedule__c();
        schedule.pse__Start_Date__c = Date.newInstance(timePeriod.pse__Start_Date__c.year(), 
                                timePeriod.pse__Start_Date__c.month(), timePeriod.pse__Start_Date__c.day());
        schedule.pse__End_Date__c = Date.newInstance(timePeriod.pse__Start_Date__c.year(), 
                            timePeriod.pse__Start_Date__c.month(), timePeriod.pse__Start_Date__c.day()) + 6;
        schedule.pse__Sunday_Hours__c = 0;
        schedule.pse__Monday_Hours__c = 8;
        schedule.pse__Tuesday_Hours__c = 8;
        schedule.pse__Wednesday_Hours__c = 8;
        schedule.pse__Thursday_Hours__c = 8;
        schedule.pse__Friday_Hours__c = 8;
        schedule.pse__Saturday_Hours__c = 0;
        insert schedule;
        schedule = [SELECT Id from pse__Schedule__c limit 1];
        System.debug('Schedule: ' + schedule);

        assignment = new pse__Assignment__c();
        assignment.Name = 'Assignment';
        assignment.pse__Project__c = project.Id;
        assignment.pse__Resource__c = contact.Id;
        assignment.pse__Schedule__c = schedule.Id;
        assignment.pse__Bill_Rate__c = 100.0;
        assignment.pse__Is_Billable__c = true;
        insert assignment;
        assignment = [SELECT Id FROM pse__Assignment__c where Name = 'Assignment'];
        System.debug('Assignment: ' + assignment);

        header = new pse__Timecard_Header__c();
        header.pse__Billable__c = true;
        header.pse__Bill_Rate__c = 100;
        header.pse__Monday_Hours__c = 8;
        header.pse__Tuesday_Hours__c = 8;
        header.pse__Wednesday_Hours__c = 8;
        header.pse__Thursday_Hours__c = 8;
        header.pse__Friday_Hours__c = 8;
        header.pse__Project__c = project.Id;
        header.pse__Resource__c = contact.id;
        header.pse__Assignment__c = assignment.Id;
        header.pse__Start_Date__c = Date.newInstance(timePeriod.pse__Start_Date__c.year(), 
                                timePeriod.pse__Start_Date__c.month(), timePeriod.pse__Start_Date__c.day());
        header.pse__End_Date__c = Date.newInstance(timePeriod.pse__Start_Date__c.year(), 
                            timePeriod.pse__Start_Date__c.month(), timePeriod.pse__Start_Date__c.day()) + 6;
        insert header;
        System.debug('Timecard header: ' + header);
    }
}