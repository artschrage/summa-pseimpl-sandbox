/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private  class TestKPIDao {

    static testMethod void testKPIFetch() {
        TestKPIBase.stageTestData();
        KPIDao dao = new KPIDao();
        
        Test.startTest();
        List<KPI__c> kpis = dao.getActiveKPIs('Quarter');
        system.assertEquals(3, kpis.size(), 'Incorrect number of automated KPIs returned');
        kpis = dao.getActiveKPIs('Year');
        system.assertEquals(1, kpis.size(), 'Incorrect number of automated KPIs returned');
        Test.stopTest();
    }

    static testMethod void testKPIValueFetch() {
        TestKPIBase.stageTestData();
        KPIDao dao = new KPIDao();
        KPI__c kpi = [select id, name from KPI__c where name = 'Complimentary Services' limit 1];
        pse__Time_Period__c period = [select id, name from pse__Time_Period__c where name = 'This Quarter' limit 1];

        List<KPI__c> kpis = new List<KPI__c>();
        kpis.add(kpi);
        
        Test.startTest();
        List<KPI_Value__c> values = dao.getKPIValues(kpis, period);
        system.assertEquals(1, values.size(), 'Incorrect number of KPI Values returned');
        Test.stopTest();
    }
    
    static testMethod void testSaveValues() {
    	TestKPIBase.stageTestData();
        KPIDao dao = new KPIDao();
        List<KPI_Value__c> values = [select id, name, target__c from KPI_Value__c];
		for (KPI_Value__c value : values) {
			value.Target__c = 11;
		}
		
        Test.startTest();
		try {
			dao.saveValues(values);
        	List<KPI_Value__c> postValues = [select id, name, target__c from KPI_Value__c];
        	for (KPI_Value__c value : postValues) {
        		system.assertEquals(11, value.target__c, 'Incorrect target value after save');
        	}
		} catch (Exception xcpn) {
			system.assert(false, xcpn.getMessage());
		}
        Test.stopTest();
    }


}