public class KPIGraphController
{
    private List<KPI__c> kpis;
    
    public KPIGraphController()
    {
        kpis = [SELECT Id, Name FROM KPI__c ORDER BY Name];
    }

    public List<KPI__c> getKPIs() 
    {
        return kpis;
    }
}