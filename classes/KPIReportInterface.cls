public interface KPIReportInterface { 
	KPI_Value__c execute(KPI_Value__c kpiValue);
}