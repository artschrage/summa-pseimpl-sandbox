public class KPI_AverageRate extends KPIBase {

  /**
    Implement the method that calculates the KPI value
    for the KPI returned by the implementation in
    the specified time period.
  */
    public override KPI_Value__c calculateImpl(pse__Time_Period__c timePeriod) {
      // Retrieve the KPI value for the time period and 
      // KPI.  If no record exists, then create one with zero
      // actuals
      KPI_Value__c kpiValue = super.getKPIValue(timePeriod);
      if (kpiValue == null) {
          System.debug('No existing value found');
          kpiValue = new KPI_Value__c();
          kpiValue.KPI__c = getTargetKPI().Id;
          kpiValue.Time_Period__c = timePeriod.Id;
          kpiValue.Actual__c = 0.0;
      }
      else {
        System.debug('Existing value found');
      }
      Decimal actual = 0.0;

      AggregateResult[] results = [SELECT 
              sum(pse__Total_Billable_Amount__c) totalbill,  
              sum(pse__Total_Hours__c) totalhrs 
              FROM pse__Timecard_Header__c 
                      WHERE pse__Billable__c = true AND 
                      pse__Total_Hours__c > 0 AND 
                      pse__Total_Billable_Amount__c > 0 AND 
                      pse__Project__r.pse__Practice__r.Name != '' AND 
                      pse__Billable__c = TRUE];

        for (AggregateResult result : results) {
            Decimal totalbill = (Decimal) result.get('totalbill');
            Decimal totalhrs = (Decimal) result.get('totalhrs');
            System.debug(LoggingLevel.FINE, 'Total Billings: ' + totalbill);
            System.debug(LoggingLevel.FINE, 'Total Hours: ' + totalhrs);
            
            if (totalhrs == 0.0) {
              actual = 0.0;
            }
            else {
              actual = totalbill / totalhrs;
            }
            System.debug(LoggingLevel.FINE, 'Average Billing Rate: ' + actual);
        }
        
        kpiValue.Actual__c = actual;
        System.debug(kpiValue);
        return kpiValue;
    }
    
    public override KPI__c getTargetKPI() {
        KPI__c kpi = [select Id, Name from KPI__c where Name = 'Average Billing Rate' limit 1];
        return kpi;
    }
}