/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class TestKPICompServiceReportLogic {

	private static pse__Time_Period__c timePeriod = null;
	 
	static {
		stageData();
	}
	
    static testMethod void testKPI() {
        KPI__c kpi = [select Id, Name, Category__c from KPI__c where Name = 'Complimentary Services' limit 1];

        KPI_Value__c kpiValue = null;

		list<KPI_Value__c> values = [select Id, KPI__c, Time_Period__c, Time_Period__r.pse__Start_Date__c, Time_Period__r.pse__End_Date__c, Target__c, Actual__c from KPI_Value__c where KPI__c = :kpi.Id and Time_Period__c = :timePeriod.Id];
		if (values != null && values.size() > 0) {
	        kpiValue = values.get(0);
		} else {
	        kpiValue = new KPI_Value__c();
	        kpiValue.KPI__c = kpi.Id;
	        kpiValue.Time_Period__c = timePeriod.Id;
	        kpiValue.Target__c = .87;
	       
	        insert kpiValue;
	        kpiValue = [select Id, KPI__c, Time_Period__c, Time_Period__r.pse__Start_Date__c, Time_Period__r.pse__End_Date__c, Target__c, Actual__c from KPI_Value__c where KPI__c = :kpi.Id and Time_Period__c = :timePeriod.Id];
		}
        
        Test.startTest();

        KPIComplimentaryServiceReportLogic logic = new KPIComplimentaryServiceReportLogic();
        Decimal expectedValue = 2000;
        
        system.debug('time period via KPI start: ' + kpiValue.Time_Period__r.pse__Start_Date__c);
        system.debug('time period via KPI end: ' + kpiValue.Time_Period__r.pse__End_Date__c);

        try {
            logic.execute(kpiValue);
            upsert kpiValue;
            system.assertEquals(expectedValue, kpiValue.Actual__c.setScale(2));
            
            KPI_Value__c refetch = [select Id, Name, Actual__c from KPI_Value__c where time_period__c = :timePeriod.Id and kpi__c = :kpi.Id];
            system.assertEquals(expectedValue, refetch.Actual__c.setScale(2));          
            
        } catch (Exception x) {
            system.assert(FALSE, x.getMessage());
        }
        
        Test.stopTest();
    }
    
    static testMethod void testKPIZero() {
    	delete [select id from complimentary_service__c];
    	
        KPI__c kpi = [select Id, Name, Category__c from KPI__c where Name = 'Complimentary Services' limit 1];

        KPI_Value__c kpiValue = null;

		list<KPI_Value__c> values = [select Id, KPI__c, Time_Period__c, Time_Period__r.pse__Start_Date__c, Time_Period__r.pse__End_Date__c, Target__c, Actual__c from KPI_Value__c where KPI__c = :kpi.Id and Time_Period__c = :timePeriod.Id];
		if (values != null && values.size() > 0) {
	        kpiValue = values.get(0);
		} else {
	        kpiValue = new KPI_Value__c();
	        kpiValue.KPI__c = kpi.Id;
	        kpiValue.Time_Period__c = timePeriod.Id;
	        kpiValue.Target__c = .87;
	       
	        insert kpiValue;
	        kpiValue = [select Id, KPI__c, Time_Period__c, Time_Period__r.pse__Start_Date__c, Time_Period__r.pse__End_Date__c, Target__c, Actual__c from KPI_Value__c where KPI__c = :kpi.Id and Time_Period__c = :timePeriod.Id];
		}
        
        Test.startTest();

        KPIComplimentaryServiceReportLogic logic = new KPIComplimentaryServiceReportLogic();
        Decimal expectedValue = 0;
        
        system.debug('time period via KPI start: ' + kpiValue.Time_Period__r.pse__Start_Date__c);
        system.debug('time period via KPI end: ' + kpiValue.Time_Period__r.pse__End_Date__c);

        try {
            logic.execute(kpiValue);
            upsert kpiValue;
            system.assertEquals(expectedValue, kpiValue.Actual__c.setScale(2));
            
            KPI_Value__c refetch = [select Id, Name, Actual__c from KPI_Value__c where time_period__c = :timePeriod.Id and kpi__c = :kpi.Id];
            system.assertEquals(expectedValue, refetch.Actual__c.setScale(2));          
            
        } catch (Exception x) {
            system.assert(FALSE, x.getMessage());
        }
        
        Test.stopTest();
    }
    
    private static void stageData() {
    	delete [select id from Complimentary_Service__c];
    	
    	Contact johnSmail = [select id, name from Contact where name = 'John Smail' limit 1];
    	pse__Region__c regionCorporate = [select Id from pse__Region__c where name = 'Corporate' limit 1];
		pse__Practice__c practiceCore = [select Id from pse__Practice__c where name = 'App Dev' limit 1];
		timePeriod = [select Id from pse__time_period__c where pse__type__c = 'Quarter' and name = 'Q3 FY2014' limit 1];
    	
    	Account account = new Account(Name='TestKPICompServiceReportLogic Account 1', CurrencyIsoCode='USD');
		insert account;
		
		pse__Proj__c project = new pse__Proj__c();
	    project.Name = 'TestKPICompServiceReportLogic Project';
	    project.pse__Account__c = account.Id;
	    project.pse__Is_Active__c = true; 
	    project.pse__Is_Billable__c = true;	//needs to be set so assignments may be marked billable for thorough exclusion testing
	    project.pse__Billing_Type__c = 'Time and Materials';
	    project.pse__Start_Date__c = Date.newInstance(2014,1,1);
	    project.pse__End_Date__c = Date.newInstance(2014,1,31);
	    project.Date_Became_Inactive__c = null;
	    project.pse__Project_Manager__c = johnSmail.Id;
	    project.Delivery_Engagement_Manager__c = johnSmail.Id;
	    project.pse__Region__c = regionCorporate.Id;
	    project.pse__Practice__c = practiceCore.Id;
	    insert project;
	    
	    list<Complimentary_Service__c> compSvcs = new list<Complimentary_Service__c>();
	    Complimentary_Service__c compSvc = null; 
	    
	    compSvc = new Complimentary_Service__c();      
        compSvc.Project__c = project.Id;
        compSvc.Time_Period__c = timePeriod.Id;        
        compSvc.Number_of_Hours__c = 10;
        compSvc.Cost_of_Service__c = 1000;
        compSvc.Cost_of_Expenses__c = 1000;
        compSvcs.add(compSvc);
	    
	    insert compSvcs;
	    
    }
}