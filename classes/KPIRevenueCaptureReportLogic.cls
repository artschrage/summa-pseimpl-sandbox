public class KPIRevenueCaptureReportLogic  implements KPIReportInterface {
    public KPI_Value__c execute(KPI_Value__c kpiValue) {
        Decimal actual = 0.0;

        AggregateResult[] results = [SELECT  
              SUM(pse__Billings__c) billsum, SUM(pse__Total_Costs__c) costsum
              FROM pse__Project_Actuals__c 
                WHERE pse__Time_Period__c = :kpiValue.Time_Period__r.Id and 
                    pse__Project__r.pse__Master_Project__r.Name = '' AND 
                    pse__Project__r.pse__Practice__r.Name != '' AND 
                    pse__Project__r.pse__Is_Billable__c = true];
            
        for (AggregateResult result : results) {
            Decimal costsum = (Decimal) result.get('costsum');
            Decimal billsum = (Decimal) result.get('billsum');
            System.debug(LoggingLevel.FINE, 'Total Cost: ' + costsum);
            System.debug(LoggingLevel.FINE, 'Billings: ' + billsum);
            
            if (billsum == 0.0) {
              actual = 0.0;
            }
            else {
              actual = billsum - costsum;
            }
            System.debug(LoggingLevel.FINE, 'T&M Revenue Capture: ' + actual);
        }
        
        kpiValue.Actual__c = actual;
        System.debug(kpiValue);
        return kpiValue;
    }
}