/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestKPIValueAlternateKeyTrigger {

	private static List<pse__Time_Period__c> timePeriodDb = new List<pse__Time_Period__c>();
	private static List<KPI__c> kpiDb = new List<KPI__c>();

	static {
		stageData();
	}
	
    static testMethod void testKPIValueInsert() {
    	List<KPI_Value__c> kpiValues = new List<KPI_Value__c>();
    	for (pse__Time_Period__c timePeriod : timePeriodDb) {
    		for (KPI__c kpi : kpiDb) {
		    	kpiValues.add(new KPI_Value__c(KPI__c=kpi.Id, Time_Period__c=timePeriod.Id, Target__c=0));
    		}
    	}
    	insert kpiValues;
    	kpiValues = [select Id, name, Alternate_Key__c, KPI__c, Time_Period__c from KPI_Value__c];
    	
    	Test.startTest();
    	for (KPI_Value__c kpiValue : kpiValues) {
    		system.assertEquals(kpiValue.KPI__c + ' ' + kpiValue.Time_Period__c, kpiValue.Alternate_Key__c);
    	}
    	
    	Test.stopTest();
    }

    static testMethod void testKPIValueUpdate() {
    	KPI_Value__c kpiValue = new KPI_Value__c(KPI__c=kpiDb[0].Id, Time_Period__c=timePeriodDb[0].Id, Target__c=0);
    	insert kpiValue;
    	
    	kpiValue = [select Id, name, Alternate_Key__c, KPI__c, Time_Period__c from KPI_Value__c];
    	
    	Test.startTest();
    	kpiValue.Time_Period__c = timePeriodDb[1].Id;
    	update kpiValue;
		kpiValue = [select Id, name, Alternate_Key__c, KPI__c, Time_Period__c from KPI_Value__c];
		    	
    	system.assertEquals(kpiDb[0].Id + ' ' + timePeriodDb[1].Id, kpiValue.Alternate_Key__c);
    	Test.stopTest();
    }

    static testMethod void testKPIValueDuplicate() {
    	KPI_Value__c kpiValue1 = new KPI_Value__c(KPI__c=kpiDb[0].Id, Time_Period__c=timePeriodDb[0].Id, Target__c=0);
    	KPI_Value__c kpiValue2 = new KPI_Value__c(KPI__c=kpiDb[0].Id, Time_Period__c=timePeriodDb[0].Id, Target__c=0);
    	
    	Test.startTest();
    	try {
    		insert kpiValue1;
    		insert kpiValue2;
    		system.assert(False);
    	} catch (Exception e) {
    		system.assert(True);
    	}

    	Test.stopTest();
    }
    
	private static void stageData() {
		//time periods
		timePeriodDb.add(new pse__Time_Period__c(Name='Q1 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2013, 7, 1), pse__End_Date__c=Date.newInstance(2013, 9, 30)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q2 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2013, 10, 1), pse__End_Date__c=Date.newInstance(2013, 12, 31)));
		timePeriodDb.add(new pse__Time_Period__c(Name='Q3 FY2014', pse__Type__c='Quarter', pse__Start_Date__c=Date.newInstance(2014, 1, 1), pse__End_Date__c=Date.newInstance(2014, 3, 31)));
		insert timePeriodDb;
		
		kpiDb.add(new KPI__c(Name='Complimentary Services'));
		kpiDb.add(new KPI__c(Name='New Client Ratio'));
		kpiDb.add(new KPI__c(Name='Account Expansion'));
		insert kpiDb;
	}
}