@isTest (SeeAllData = true)
public class TestSetFirstContractDate {
	private static User thisUser;
	private static pse__Region__c testRegion;
	private static Account testAccount1;
	private static Account testAccount2;
	
	static {
		stageTestData();
	}
	
	private static void stageTestData() {
		thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		testRegion = [Select Id from pse__Region__c where Name = 'Corporate'];
		
		testAccount1 = new Account(
			Name='TestAcct1',
			CurrencyIsoCode='USD',
			Type ='Prospect');
		insert testAccount1;

		//System.debug('Account = ' +testAccount1.Name);
		System.assertEquals('TestAcct1', testAccount1.Name);

		testAccount2 = new Account(
			Name='TestAcct2',
			CurrencyIsoCode='USD',
			Type ='Customer',
			First_Contract_Date__c = Date.today()-1);
		insert testAccount2;

		//System.debug('Account = ' +testAccount2.Name);
		System.assertEquals('TestAcct2', testAccount2.Name);
	}
	
    static testmethod void insertOppsAcct1(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOpp1 = new Opportunity(
				Name='Test Opportunity1',
				AccountId= testAccount1.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Region__c= testRegion.Id);
			insert testOpp1;
			System.assertEquals('Qualify', testOpp1.StageName);
			      
			Opportunity testOpp2 = new Opportunity(
				Name='Test Opportunity2',
				AccountId= testAccount1.Id,
				StageName= 'Won',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Region__c= testRegion.Id);
			insert testOpp2;
			System.assertEquals('Won', testOpp2.StageName);
		}

		Test.stopTest();
	}

    static testmethod void insertOppsAcct2(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOpp3 = new Opportunity(
				Name='Test Opportunity3',
				AccountId= testAccount2.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Region__c= testRegion.Id);
			insert testOpp3;
			System.assertEquals('Qualify', testOpp3.StageName);
			      
			Opportunity testOpp4 = new Opportunity(
				Name='Test Opportunity4',
				AccountId= testAccount1.Id,
				StageName= 'Won',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Region__c= testRegion.Id);
			insert testOpp4;
			System.assertEquals('Won', testOpp4.StageName);
		}

		Test.stopTest();
	}

    static testmethod void updateAccount(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOpp5 = new Opportunity(
				Name='Test Opportunity5',
				AccountId= testAccount1.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Region__c= testRegion.Id);
			insert testOpp5;
			System.assertEquals('Qualify', testOpp5.StageName);
			      
			List<Opportunity> testOppList = [Select Id, StageName, AccountId from Opportunity where Name = 'Test Opportunity5'];
			testOppList.get(0).StageName = 'Won';
			update testOppList;
			System.assertEquals(Date.today(),[Select First_Contract_Date__c from Account where Name = 'TestAcct1'].First_Contract_Date__c);
		}

		Test.stopTest();
	}

}