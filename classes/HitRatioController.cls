public class HitRatioController {
	private Map<String, Decimal> stageMap;
	
    public pse__Time_Period__c prevFQ { get; set; }
    public pse__Time_Period__c currentFY { get; set; }

	private Decimal AmountPFQVal = 0;
	private Decimal CountPFQVal = 0;
	private Decimal AmountYTDVal = 0;
	private Decimal CountYTDVal = 0;
	
    public Decimal HitRatioAmountPFQ { get {return AmountPFQVal;} }
    public Decimal HitRatioCountPFQ { get {return CountPFQVal;} }
    public Decimal HitRatioAmountYTD { get {return AmountYTDVal;} }
    public Decimal HitRatioCountYTD { get {return CountYTDVal;} }

    /* 	Constructor - set up initial time period values for calculation
    	queries and kick off the calculation methods
    */
    public HitRatioController() {
    	System.debug('HitRatioController ctor');
    	buildStageMap();
        prevFQ = getPrevFQ();
        currentFY = getCurrentFY();
        doPFQCalculations();
        //doYTDCalculations();
    }
    
	/*	Build a map of opportunity stages to probability values so
		we can look them up when we evaluate the opportunity history */
    private void buildStageMap() {
    	stageMap = new Map<String, Decimal>();
    	List<OpportunityStage> allStages = [select MasterLabel, DefaultProbability 
								from OpportunityStage order by DefaultProbability];
		for (OpportunityStage stage : allStages) {
			stageMap.put(stage.MasterLabel, stage.DefaultProbability);
		}
		System.debug('StageMap built with ' + stageMap.size() + ' keys');
    }
    
    
    /* Return the time period reprsenting the previous fiscal quarter */
    private static pse__Time_Period__c getPrevFQ() {
        pse__Time_Period__c pfq = [SELECT Id, Name, pse__Start_Date__c, 
        			pse__End_Date__c from pse__Time_Period__c 
                    where pse__Type__c = 'Quarter' and 
                    pse__End_Date__c <= today 
                    order by pse__End_Date__c DESC LIMIT 1]; 
		System.debug('PFQ = ' + pfq.Name);
        return pfq;
    }

    /* Return the time period reprsenting the current fiscal year to date */
    private static pse__Time_Period__c getCurrentFY() {
        pse__Time_Period__c fy = [SELECT Id, Name, pse__Start_Date__c, 
        			pse__End_Date__c from pse__Time_Period__c 
                    where pse__Type__c = 'Year' and 
                    pse__Start_Date__c <= today 
                    order by pse__Start_Date__c DESC LIMIT 1];
		System.debug('FY = ' + fy.Name);
		return fy;
    }
    
    /*	Calculate hit ratio for the previous fiscal quarter, 
    	in terms of both opportunity amount and opportunity count.
    */
    private void doPFQCalculations() {
    	Map<String, Opportunity>  oppMap = getOpportunities(
							prevFQ.pse__Start_Date__c,
							prevFQ.pse__End_Date__c);
		System.debug('PFQ Opportunity count = ' + oppMap.size());

    	Decimal grandCount = 0.0, hitCount = 0.0;
    	Decimal grandAmount = 0.0, hitAmount = 0.0;
    	
    	List<OpportunityFieldHistory> histories = [SELECT Id, OpportunityId, 
    				NewValue from OpportunityFieldHistory 
    				where Field = 'StageName' 
    				and OpportunityId in :oppMap.keySet()];
		System.debug('PFQ History count = ' + histories.size());

		// Crunch the raw data into a map to make the lookup easier
		Map<String, List<String>> historyMap =
									buildHistoryMap(histories);

		// Iterate through the opportunities and see which ever have been
		// 50% or more during their lifetime
    	for (Opportunity opp : oppMap.values())
    	{
            String dbg = 'PFQ OPP: ' + opp.Id + ' ' + opp.Name + ' ' + opp.Amount;
			Decimal amount = 0.0;
			if (opp.Amount != null)
				amount = opp.Amount;

            ++grandCount;
            grandAmount += amount;

			if (opp.StageName == 'Won') {
                dbg += ' Won HIT!!!';
				++hitCount;
				hitAmount += amount;
			}
			else
			{
	    		Decimal highest = getHighestProbability(opp, 
	    									historyMap.get(opp.Id));
                dbg += ' Probability: ' + highest;
				if (highest >= 50.00) {
                    dbg += ' HIT!!';
                    ++hitCount;
                    hitAmount += amount;
				}
                else {
                    dbg += ' MISS';
                }
			}
            System.debug(dbg);
    	}    	

    	System.debug('PFQ Hit count = ' + hitCount);
    	System.debug('PFQ Grand count = ' + grandCount);
    	System.debug('PFQ Hit amount = ' + hitAmount);
    	System.debug('PFQ Grand amount = ' + grandAmount);
    	
    	if (grandAmount != 0)
    		AmountPFQVal = hitAmount / grandAmount;
		if (grandCount != 0)
    		CountPFQVal = hitCount / grandCount;
    }

    /*	Calculate hit ratio for the fiscal year-to-date, 
    	in terms of both opportunity amount and opportunity count.
	*/
    private void doYTDCalculations() {
    	Map<String, Opportunity>  oppMap = getOpportunities(
    					currentFY.pse__Start_Date__c, Date.today());
		System.debug('YTD Opportunity count = ' + oppMap.size());
    	
    	Decimal grandCount = 0.0, hitCount = 0.0;
    	Decimal grandAmount = 0.0, hitAmount = 0.0;
    	
    	// Collect all the field history at one time.  This is because if 
    	// we did it any other way, we would do way too many queries.
    	List<OpportunityFieldHistory> histories = [SELECT Id, OpportunityId, 
    				NewValue from OpportunityFieldHistory 
    				where Field = 'StageName' 
    				and OpportunityId in :oppMap.keySet()];
		System.debug('YTD History count = ' + histories.size());
		
		// Crunch the raw data into a map to make the lookup easier
		Map<String, List<String>> historyMap =
									buildHistoryMap(histories);
    
		// Iterate through the opportunities and see which ever have been
		// 50% or more during their lifetime
    	for (Opportunity opp : oppMap.values())
    	{
            String dbg = 'YTD OPP: ' + opp.Id + ' ' + opp.Name + ' ' + opp.Amount;
			Decimal amount = 0.0;
			if (opp.Amount != null)
				amount = opp.Amount;
            ++grandCount;
            grandAmount += amount;

			if (opp.StageName == 'Won') {
                dbg += ' Won HIT!!!';
                ++hitCount;
                hitAmount += amount;
			}
			else
			{
	    		Decimal highest = getHighestProbability(opp, 
	    									historyMap.get(opp.Id));
                dbg += ' Probability: ' + highest;
				if (highest >= 50.00) {
                    dbg += ' HIT!!';
                    ++hitCount;
                    hitAmount += amount;
				}
                else {
                    dbg += ' MISS';
                }
			} 
            System.debug(dbg);
    	} 
    	
    	System.debug('YTD Hit count = ' + hitCount);
    	System.debug('YTD Grand count = ' + grandCount);
    	System.debug('YTD Hit amount = ' + hitAmount);
    	System.debug('YTD Grand amount = ' + grandAmount);
    	
    	if (grandAmount != 0)
	    	AmountYTDVal = hitAmount / grandAmount;
    	if (grandCount != 0)
    		CountYTDVal = hitCount / grandCount;
    }
	    
    /*	Build a map so we only have to iterate through the history one time.
    	The map is keyed by the Opportunity ID (as a String) and have a list
    	of stage name strings as the value.
    */
    private static Map<String, List<String>> buildHistoryMap(
    						List<OpportunityFieldHistory> histories) {
		Map<String, List<String>> historyMap = 
								new Map<String, List<String>>();	
			
		// Let's look at all the history.  Since the history values 
		// have a string and not the actual probability value, we can't
		// get a sort from the query.  This still means we have to iterate
		// the history one time.
		for (OpportunityFieldHistory history : histories) {
			
			// If we don't have an entry for the opportnity, create one
			if (!historyMap.containsKey(history.OpportunityId)) {
				historyMap.put(history.OpportunityId, new List<String>());
			}
			
			// Add the string value to the list.  We can translate to the
			// actual probabilty value later when we iterate back through
			// the opportunities.
			List<String> oppList = historyMap.get(history.OpportunityId);
			oppList.add((String) history.NewValue);
		}
		return historyMap;
	}
	    
    /*	Return a map of Opportunities that closed between a start and 
    	end date (inclusive)
	*/
    private static Map<String, Opportunity> getOpportunities(Date startDate, Date endDate)
    {
		// Select all the opportunities in the specified range
		List<Opportunity> opps = [SELECT Id, Name, Amount, CloseDate, 
					StageName from Opportunity where
    				CloseDate >= :startDate and CloseDate <= :endDate
    				order by CloseDate];	

    	// Build a map from the opportunity ID to the opportunity 
    	// record so we can build 
    	Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
    	for (Opportunity opp : opps)
    	{
    		oppMap.put(opp.Id, opp);
    	}
    	
    	return oppMap;
    }
    
    /*	For a given opportunity, figure out what the highest 
    	probability was during its history
    */
    private Decimal getHighestProbability(Opportunity opp, List<String> values)
    {
    	// Start off with whatever the current opportunity stage is
    	// because there will be no change record if opportunities
    	// were created and never changed.
    	Decimal maxProb = stageMap.get(opp.StageName);
    	if (maxProb == null) {
    		System.debug('maxProb is null? [' + opp.StageName + ']');
    		return 0.0;
    	}
    	if (values == null) {
    		return maxProb;
    	}

		// REFACTOR - if we make this history list into a map
		// of maps this will be much more efficient - we can traverse 
		// it just one time and we can build a value tree.
    	for (String value : values) {
			// If the history record has a probability any higher
			// than the current highest, replace the max
    		Decimal prob = stageMap.get(value);
    		if (prob > maxProb)
    			maxProb = prob;
    	}
		return maxProb;
    }
}