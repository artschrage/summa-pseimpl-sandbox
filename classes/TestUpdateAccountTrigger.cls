@isTest (SeeAllData = true)
public class TestUpdateAccountTrigger {
	private static User thisUser;
	private static pse__Region__c testRegion;
	private static pse__Practice__c testPracticeBTS;
	private static pse__Practice__c testPracticeCore;
	private static pse__Practice__c testPracticeCRM;
	private static pse__Practice__c testPracticeUX;
	
	private static Account testAccount;
	
	static {
		stageTestData();
	}
	
	private static void stageTestData() {
		thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		testRegion = [Select Id from pse__Region__c where Name = 'Corporate'];

		testPracticeBTS = [Select Id, Name from pse__Practice__c where Name = 'BTS'];
		System.debug('testPracticeBTS = ' +testPracticeBTS.Name);

		testPracticeCore = [Select Id, Name from pse__Practice__c where Name = 'Core'];
		System.debug('testPracticeCore = ' +testPracticeCore.Name);

		testPracticeCRM = [Select Id, Name from pse__Practice__c where Name = 'CRM'];
		System.debug('testPracticeCRM = ' +testPracticeCRM.Name);

		testPracticeUX = [Select Id, Name from pse__Practice__c where Name = 'UX Design'];
		System.debug('testPracticeUX = ' +testPracticeUX.Name);

		testAccount = new Account(
			Name='TestAcct',
			CurrencyIsoCode='USD');
		insert testAccount;

		System.debug('Account = ' +testAccount.Name);
		System.assertEquals('TestAcct', testAccount.Name);
	}
	
    static testmethod void updateAccountFromOppBTS(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOppBTS = new Opportunity(
				Name='Test Opportunity BTS',
				AccountId= testAccount.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Practice__c= testPracticeBTS.Id,
				pse__Region__c= testRegion.Id);
			insert testOppBTS;
			System.assertEquals('Qualify', testOppBTS.StageName);
			      
			testOppBTS.StageName='LOST';
			testOppBTS.Closed_Lost_Reason__c='lost';
			update testOppBTS;
			System.assertEquals('LOST', testOppBTS.StageName);
			      
			testOppBTS.StageName='WON';
			update testOppBTS;
			System.assertEquals('WON', testOppBTS.StageName);
		}

		Test.stopTest();
	}

    static testmethod void updateAccountFromOppCore(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOppCore = new Opportunity(
				Name='Test Opportunity',
				AccountId= testAccount.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Practice__c= testPracticeCore.Id,
				pse__Region__c= testRegion.Id);
			insert testOppCore;
			System.assertEquals('Qualify', testOppCore.StageName);
			      
			testOppCore.StageName='LOST';
			testOppCore.Closed_Lost_Reason__c='lost';
			update testOppCore;
			System.assertEquals('LOST', testOppCore.StageName);
			      
			testOppCore.StageName='WON';
			update testOppCore;
			System.assertEquals('WON', testOppCore.StageName);
		}

		Test.stopTest();
	}

    static testmethod void updateAccountFromOppCRM(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOppCRM = new Opportunity(
				Name='Test Opportunity',
				AccountId= testAccount.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Practice__c= testPracticeCRM.Id,
				pse__Region__c= testRegion.Id);
			insert testOppCRM;
			System.assertEquals('Qualify', testOppCRM.StageName);
			  
			testOppCRM.StageName='LOST';
			testOppCRM.Closed_Lost_Reason__c='lost';
			update testOppCRM;
			System.assertEquals('LOST', testOppCRM.StageName);
			          
			testOppCRM.StageName='WON';
			update testOppCRM;
			System.assertEquals('WON', testOppCRM.StageName);
		}

		Test.stopTest();
	}

    static testmethod void updateAccountFromOppUX(){
      
		Test.startTest();

		System.runAs ( thisUser ) {
			Opportunity testOppUX = new Opportunity(
				Name='Test Opportunity',
				AccountId= testAccount.Id,
				StageName= 'Qualify',
				ForecastCategoryName= 'Pipeline',
				CloseDate= Date.today()+7,
				Start_Date__c= Date.today()+8,
				pse__Practice__c= testPracticeUX.Id,
				pse__Region__c= testRegion.Id);
			insert testOppUX;
			System.assertEquals('Qualify', testOppUX.StageName);
			      
			testOppUX.StageName='LOST';
			testOppUX.Closed_Lost_Reason__c='lost';
			update testOppUX;
			System.assertEquals('LOST', testOppUX.StageName);
			      
			testOppUX.StageName='WON';
			update testOppUX;
			System.assertEquals('WON', testOppUX.StageName);
		}
      
		Test.stopTest();
	}
}