public with sharing class DataFactoryTestExpenses {
	private static list<pse__Expense_Report__c> reports;
	private static list<pse__Expense__c> items;
	
	static {
		reports = new list<pse__Expense_Report__c>();
		items = new list<pse__Expense__c>();
	}

	public static void createProjectExpenseReports(list<string> projectIds) {
		reports.clear();
		items.clear();
		
		list<pse__Assignment__c> assignments = DataFactoryTestAssignments.getProjectAssignments(projectIds);
		map<string, list<pse__Assignment__c>> mapProjectToAssignments = new map<string, list<pse__Assignment__c>>();
		
		for (string projectId : projectIds) {
			list<pse__Assignment__c> assignmentsForProject = new list<pse__Assignment__c>();
			for (pse__Assignment__c assignment : assignments) {
				if (assignment.pse__Project__c == projectId) {
					assignmentsForProject.add(assignment);
				}
			}
			mapProjectToAssignments.put(projectId, assignmentsForProject);
		} 
		
		pse__Expense_Report__c newReport;
		for (string projectId : mapProjectToAssignments.keySet()) {
			for (pse__Assignment__c assignment : mapProjectToAssignments.get(projectId)) {
				newReport = new pse__Expense_Report__c();
				newReport.pse__Assignment__c = assignment.Id;
				newReport.pse__Billable__c = assignment.pse__Is_Billable__c;
				newReport.CurrencyIsoCode = 'USD';
				newReport.pse__Project__c = assignment.pse__Project__c;
				newReport.pse__Resource__c = assignment.pse__Resource__c;
				newReport.pse__Status__c = 'Draft';
				newReport.pse__Last_Expense_Date__c = assignment.pse__end_date__c;
				
				reports.add(newReport);
			}
		}
		
		for (pse__Expense_Report__c x : reports) {
			system.debug('Before report insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; assignment='+ x.pse__Assignment__c +'; isBillable='+ x.pse__Billable__c +';');
		}

		insert reports;
		
		for (pse__Expense_Report__c x : reports) {
			system.debug('After report insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; assignment='+ x.pse__Assignment__c +'; isBillable='+ x.pse__Billable__c +';');
		}

		pse__Expense__c newItem;
		for (pse__Expense_Report__c report : reports) {
			newItem = new pse__Expense__c();
			newItem.pse__Amount__c = 250;
			newItem.pse__Assignment__c = report.pse__assignment__c;
			newItem.pse__Billable__c = report.pse__billable__c;
			newItem.pse__Expense_Date__c = report.pse__Last_Expense_Date__c;
			newItem.pse__Expense_Report__c = report.Id;
			newItem.pse__Project__c = report.pse__project__c;
			newItem.pse__Resource__c = report.pse__resource__c;
			newItem.pse__Type__c = 'Business Meals';
			
			items.add(newItem);
		}
		
		for (pse__Expense__c x : items) {
			system.debug('Before item insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; assignment='+ x.pse__Assignment__c +'; expenseReport='+ x.pse__Expense_Report__c +'; isBillable='+ x.pse__Billable__c +'; type='+ x.pse__type__c +'; amount='+ x.pse__amount__c +';');
		}

		insert items;
		
		for (pse__Expense__c x : items) {
			system.debug('After item insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; assignment='+ x.pse__Assignment__c +'; expenseReport='+ x.pse__Expense_Report__c +'; isBillable='+ x.pse__Billable__c +'; type='+ x.pse__type__c +'; amount='+ x.pse__amount__c +';');
		}
		
		for (pse__Expense_Report__c report : reports) {
			report.pse__Submitted__c = true;
			report.pse__Approved__c = true;
			report.pse__Approver__c = UserInfo.getUserId();
			report.pse__Status__c = 'Approved';
		}
		
		update reports;
		
		for (pse__Expense__c item : items) {
			item.pse__Submitted__c = true;
			item.pse__Approved__c = true;
			item.pse__Status__c = 'Approved';
		}
		
		update items;
	}
	
	public static void createExpenseReportItems(list<string> projectIds) {
		reports.clear();
		items.clear();
		
		reports = [select id, name, pse__Approved__c, pse__Approver__c, pse__Assignment__c, pse__Billable__c, CurrencyIsoCode, pse__Project__c, pse__Resource__c, pse__Status__c, pse__Submitted__c, pse__Last_Expense_Date__c from pse__Expense_Report__c where pse__project__c in :projectIds];
		
		pse__Expense__c item;
		for (pse__Expense_Report__c report : reports) {
			item = new pse__Expense__c();
			item.pse__Amount__c = 250;
			item.pse__Approved__c = true;
			item.pse__Assignment__c = report.pse__assignment__c;
			item.pse__Billable__c = report.pse__billable__c;
			item.pse__Expense_Date__c = report.pse__Last_Expense_Date__c;
			item.pse__Expense_Report__c = report.Id;
			item.pse__Project__c = report.pse__project__c;
			item.pse__Resource__c = report.pse__resource__c;
			item.pse__Status__c = 'Approved';
			item.pse__Submitted__c = true;
			item.pse__Type__c = 'Business Meals';
			
			items.add(item);
		}
		
		for (pse__Expense__c x : items) {
			system.debug('Before item insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; assignment='+ x.pse__Assignment__c +'; expenseReport='+ x.pse__Expense_Report__c +'; isBillable='+ x.pse__Billable__c +'; type='+ x.pse__type__c +'; amount='+ x.pse__amount__c +';');
		}

		insert items;
		
		for (pse__Expense__c x : items) {
			system.debug('After item insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; assignment='+ x.pse__Assignment__c +'; expenseReport='+ x.pse__Expense_Report__c +'; isBillable='+ x.pse__Billable__c +'; type='+ x.pse__type__c +'; amount='+ x.pse__amount__c +';');
		}
		
	}
	
}