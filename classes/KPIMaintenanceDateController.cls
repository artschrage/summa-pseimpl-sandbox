public class KPIMaintenanceDateController {
    public List<SelectOption> futureQuarters { get; set; }
    public String selectedQuarterName { get; set; }
    
    public KPIMaintenanceDateController() {
        List<pse__Time_Period__c> quarters = [SELECT Id, Name FROM 
                pse__Time_Period__c WHERE pse__Type__c = 'Quarter'
                AND pse__End_Date__c > TODAY 
                ORDER BY pse__Start_Date__c LIMIT 6];
                
        futureQuarters = new List<SelectOption>();
        
        for (pse__Time_Period__c quarter : quarters) {
            SelectOption option = new SelectOption(quarter.Name, quarter.Name);
            futureQuarters.add(option);
        }
    }
    
    public PageReference loadEditor() {
        System.debug('loadEditor()');
        PageReference ref = new PageReference('/apex/KPI_Maintenance_3');
        
        if (selectedQuarterName == null) {
            return null;
        }
        
        ref.getParameters().put('selectedQuarter', selectedQuarterName);
        return ref;
    }
}