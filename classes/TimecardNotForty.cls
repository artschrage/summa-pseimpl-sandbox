global class TimecardNotForty implements Schedulable
{ 

 String<Id> templateId;
 List<Id> recipientList = new List<Id>();
 String<Id> needforty;

 public TimecardNotForty()
 {
 }
 
 global void execute(SchedulableContext ctx)
 {
    templateId = getEmailTemplate();
    recipientList = getEmailAddresses();
    sendEmail();
 }
 
  public void sendEmail()
  {
     if (templateId == null){
     templateId = '00X40000001aPbh'; //hard coded template ID in case query fails
     //System.debug('hard coded email template');
     }
     //System.debug('before if, recipientList = ' +recipientList );
      if (recipientList.isEmpty() || recipientList == null){
        recipientList.add('0034000000pWMgh'); //hard coded Contact ID in case recipientList is null to make sure email is sent
        //System.debug('in if, recipientList = ' +recipientList);
        //System.debug('no email list, no emails sent');
      }
       
     //System.debug('Start sendEmail');
  
       Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
       mail.setTargetObjectIds(recipientList);
       mail.setTemplateId(templateId);
       mail.setReplyTo('psaAdmin@summa-tech.com');
       mail.setSenderDisplayName('PSA Admin');
       Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });

   }
   
  List<Id> getEmailAddresses()  //get the list of people whose submitted timecard has less than the minimum 40 hrs for this past week
  { 
  	recipientList.clear();
    //this query gets the list of those who submitted timecards for less than 40 hrs
      AggregateResult[] ar = [SELECT pse__Resource__c res, SUM(pse__Total_Hours__c)hours from pse__Timecard_Header__c where pse__End_Date__c = THIS_WEEK and pse__Submitted__c = true and pse__External_Resource__c = false group by pse__Resource__c having SUM(pse__Total_Hours__c)<40];   
       System.debug('forty list size=' +ar.size());
       
     //create the recipient list  
       List<Id> lstids = new List<Id>();
       
          if (!ar.isEmpty()) { // check to see if anyone has a timecard with less than 40 hrs, if so, add them to the list
            
            for(Integer j =0; j < ar.size(); j++){
               needforty = (String) ar[j].get('res');
             lstids.add(needforty);
            System.debug('lessThan40 Resource = ' +ar[j].get('res'));
            System.debug('total hours = ' +ar[j].get('hours'));
            }
         ar.clear();
         return lstids;
       } else {
      //return null;
      return recipientList;
      }
  
  }
   
    String<Id> getEmailTemplate()  //get the email template for the reminder notification
    { 
        EmailTemplate e = [Select Id from EmailTemplate where Name = 'Timecard_Not_40'];
        //System.debug('email template ID = ' +e.Id);
        if (e != null) {
        return e.Id;
        } else {
        //return null;
        return templateId;
        }
    }
        
}