public class KPIHitRatioAmountReportLogic extends KPIHitRatioBaseReportLogic {
 
    public KPI_Value__c execute(KPI_Value__c kpiValue) {
        Decimal actual = 0.0;
        
        Map<String, Opportunity> oppMap = getOpportunityMap(
                        kpiValue.Time_Period__r.pse__Start_Date__c,
                        kpiValue.Time_Period__r.pse__End_Date__c);
        List<OpportunityFieldHistory> histories = 
                [SELECT Id, OpportunityId, NewValue FROM OpportunityFieldHistory
                        WHERE Field = 'StageName' AND 
                        OpportunityId in :oppMap.keySet()];
        System.debug('History count = ' + histories.size());
        
        Map<String, List<String>> historyMap = buildHistoryMap(histories);
        
        Decimal total = 0.0;
        Decimal hit = 0.0;
        
        for (Opportunity opp : oppMap.values()) {
            String oppDebug = 'Opportunity: ' + opp.Name + ' Amount: ' + opp.Amount;
            Decimal amount = 0.0;
            if (opp.Amount != null) {
                amount = opp.Amount;
            }
            
            // If the opportunity is won, don't bother processing
            // any history
            total += amount;
            if (opp.StageName == 'Won') {
                oppDebug += ' Won';
                hit += amount; 
            }
            else {
                Decimal highest = getHighestProbability(opp,
                                    historyMap.get(opp.Id));
                oppDebug += ' ' + highest;
                if (highest >= 50.0) {
                    hit += amount;
                    oppDebug += ' HIT';
                }
            }
            System.debug(LoggingLevel.FINE, oppDebug);
        }
        System.debug('Total amount = ' + total);
        System.debug('Hit amount = ' + hit);
        if (total > 0.0) {
            actual = hit / total;
        }
        System.debug('hit/total = ' + actual);
        
        kpiValue.Actual__c = actual;
        return kpiValue; 
    }
}