public with sharing class ExistingClientAnalysisDao {

	public ExistingClientAnalysisDao() {}
	
	public void storeOpportunityChanges(List<Opportunity> opportunities) {
        SavePoint sp = null;
        
        if (opportunities != null && opportunities.size() > 0) {
        	sp = Database.setSavePoint();
            try {
	            system.debug('Upserting ['+ opportunities.size() +'] Opportunity records.');
                upsert opportunities;
            } catch (Exception xcpn) {
                if (sp != null) {
		            system.debug('Rolling back to SavePoint due to ['+ xcpn.getMessage() +'].');
                    Database.rollback(sp);
                }
                throw xcpn;
            }
        }
	}
	
	public void storeNewClientValueChanges(List<New_Client_Value__c> newClientValues) {
        SavePoint sp = null;
        
        if (newClientValues != null && newClientValues.size() > 0) {
        	sp = Database.setSavePoint();
            try {
	            system.debug('Upserting ['+ newClientValues.size() +'] New Client Value records.');
                upsert newClientValues;
            } catch (Exception xcpn) {
                if (sp != null) {
		            system.debug('Rolling back to SavePoint due to ['+ xcpn.getMessage() +'].');
                    Database.rollback(sp);
                }
                throw xcpn;
            }
        }
	}
	
	public List<Opportunity> fetchWonOpportunities(Set<Id> accountIds, Date analysisBeginDate) {
		
		List<Opportunity> returnValue = [
			select	o.Id, o.Name, o.ForecastCategory, o.StageName, o.CloseDate, o.AccountId, o.Amount, o.Is_New_Client_Opp__c, o.Allow_System_to_Determine_if_New_Opp__c, o.New_Client_Value__c 
			from	Opportunity o
			where	o.AccountId in :accountIds
			and		o.CloseDate >= :analysisBeginDate
			and		o.ForecastCategory = 'Closed' 
			and		o.StageName = 'Won'
			order by	o.AccountId, o.CloseDate asc
		];

		return returnValue;
	}
	
	public List<Opportunity> fetchNewClientOpportunities(Set<Id> accountIds, Date analysisBeginDate) {
		
		List<Opportunity> returnValue = [
			select	o.Id, o.Name, o.ForecastCategory, o.StageName, o.CloseDate, o.AccountId, o.Amount, o.Is_New_Client_Opp__c, o.Allow_System_to_Determine_if_New_Opp__c, o.New_Client_Value__c 
			from	Opportunity o
			where	o.AccountId in :accountIds
			and		o.CloseDate >= :analysisBeginDate
			and		o.ForecastCategory = 'Closed' 
			and		o.StageName = 'Won'
			and		o.Is_New_Client_Opp__c = true
			order by	o.AccountId, o.CloseDate asc
		];

		return returnValue;
	}
	
	public List<New_Client_Value__c> fetchNewClientValues(Set<Id> accountIds, Date analysisBeginDate) {
		
		List<New_Client_Value__c> returnValue = [
			select	o.Id, o.Name, o.Account__c, o.Start_Date__c, o.End_Date__c 
			from	New_Client_Value__c o
			where	o.Account__c in :accountIds
			and		o.End_Date__c >= :analysisBeginDate
			order by	o.Account__c, o.Start_Date__c asc
		];

		return returnValue;
	}
}