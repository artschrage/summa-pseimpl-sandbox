public class KPI_TNMRevenueCapture extends KPIBase {
	/**
		Implement the method that calculates the KPI value
		for the KPI returned by the implementation in
		the specified time period.
	*/
    public override KPI_Value__c calculateImpl(pse__Time_Period__c timePeriod) {
    	// Retrieve the KPI value for the time period and 
    	// KPI.  If no record exists, then create one with zero
    	// actuals
    	KPI_Value__c kpiValue = super.getKPIValue(timePeriod);
    	if (kpiValue == null) {
    		System.debug('No existing value found');
    		kpiValue = new KPI_Value__c();
    		kpiValue.KPI__c = getTargetKPI().Id;
    		kpiValue.Time_Period__c = timePeriod.Id;
			kpiValue.Actual__c = 0.0;
    	}
    	else {
    		System.debug('Existing value found');
    	}
    	
    	Decimal actual = 0.0;
    	
        AggregateResult[] results = [SELECT  
                    SUM(pse__Billings__c) billsum, SUM(pse__Total_Costs__c) costsum
                    FROM pse__Proj__c 
                    WHERE Date_Became_Inactive__c >= :timePeriod.pse__Start_Date__c AND 
                    Date_Became_Inactive__c <= :timePeriod.pse__End_Date__c AND
                    pse__Billing_Type__c = 'Time and Materials' AND 
                    pse__Is_Billable__c = TRUE AND 
                    pse__Master_Project__c = '' AND 
                    pse__Practice__c != ''];
                    
        for (AggregateResult result : results) {
            Decimal billsum = (Decimal) result.get('billsum');
            Decimal costsum = (Decimal) result.get('costsum');
            System.debug(LoggingLevel.FINE, 'Billings: ' + billsum);
            System.debug(LoggingLevel.FINE, 'Total Costs: ' + costsum);
            actual = billsum - costsum;
            System.debug(LoggingLevel.FINE, 'Revenue Capture: ' + actual);
        }
        kpiValue.Actual__c = actual;
        System.debug(kpiValue);
		return kpiValue;
    }
    
    public override KPI__c getTargetKPI() {
        KPI__c kpi = [select Id, Name from KPI__c where Name = 'T&M Revenue Capture' limit 1];
        return kpi;
    }
}