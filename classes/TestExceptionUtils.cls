/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestExceptionUtils {

    static testMethod void testErrorHandling() {
    	list<Error_Log_Summa__c> baselineState = [select id from Error_Log_Summa__c];
    	system.assertEquals(0, baselineState.size(), 'Expected initial error log content to be 0 records');
    	
    	Complimentary_Service__c a = null;
    	try {
    		a.Project__c = null;
    	} catch (Exception e) {
    		ExceptionUtils.logException(e);
	    	list<Error_Log_Summa__c> testingState = [select id from Error_Log_Summa__c];
	    	system.assertEquals(1, testingState.size(), 'Expected error log content after test to be 1 record');
    	}
    }
    
    static testMethod void testErrorHandlingDetails() {
    	list<Error_Log_Summa__c> baselineState = [select id from Error_Log_Summa__c];
    	system.assertEquals(0, baselineState.size(), 'Expected initial error log content to be 0 records');
    	
    	Complimentary_Service__c a = null;
    	try {
    		a.Project__c = null;
    	} catch (Exception e) {
    		ExceptionUtils.logException(e, 'Additional Info about error');
	    	list<Error_Log_Summa__c> testingState = [select id, Additional_Information__c from Error_Log_Summa__c];
	    	system.assertEquals(1, testingState.size(), 'Expected error log content after test to be 1 record');
	    	system.assertEquals('Additional Info about error', ((Error_Log_Summa__c)(testingState.get(0))).Additional_Information__c);
    	}
    }
}