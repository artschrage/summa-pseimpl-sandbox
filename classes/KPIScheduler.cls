global class KPIScheduler implements Schedulable {

	global void execute(SchedulableContext SC) {
		try {
			KPIBatcher batch = new KPIBatcher();
			//The last argument will set the BatchableContext chunk size to keep the number of KPI reports concurrently processed low
			String cronID = System.scheduleBatch(batch, 'Run KPI Reports', 1, 1);
		} catch (Exception e) {
			ExceptionUtils.logException(e);
		}
	}

}