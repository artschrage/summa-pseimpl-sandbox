/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * This class specifically tests the ProjectDeliverableBeforeDeleteTrigger. The trigger 
 * ensures that delete operations are executed by the DEM, if one is assigned.
 */
@isTest(seealldata=true)
private class TestProjDeliverableBeforeDeleteTrigger {
    //Account: Summa Technologies, Inc. = 001f000000AE8LkAAL
    //User: Smail = 00540000001S2lKAAS          Contact = 003f0000009xhZMAAY
    //User: Howell = 00540000001RwUCAA0         Contact = 003f0000009xhaUAAQ
    //User: Goldstein = 00540000001S2lXAAS      Contact = 003f0000009xha5AAA
        
    static testMethod void testDeleteAsDEM() {
        String currentUserId = UserInfo.getUserId();
        sObject currentContact = Database.query('select Id from Contact where pse__Salesforce_User__c = :currentUserId limit 1');
        ID currentContactId = (ID)currentContact.get('Id');
        system.debug('Current User Contact Id = ' + currentContactId);
        
        sObject corpRegion = Database.query('select Id from pse__Region__c where name = \'Corporate\' limit 1');
        ID corpRegionId = (ID)corpRegion.get('Id');
        system.debug('Current Region Id = ' + corpRegionId);
        
//        sObject account = Database.query('select Id from Account where name = \'Summa Technologies, Inc.\' limit 1');
//        ID accountId = (ID)account.get('Id');
//        system.debug('Current Account Id = ' + accountId);
        
        sObject practice = Database.query('select Id from pse__Practice__c where name = \'App Dev\' limit 1');
        ID practiceId = (ID)practice.get('Id');
        system.debug('Current Practice Id = ' + practiceId);
        
        sObject pmContact = Database.query('select Id from Contact where name = \'Mitch Goldstein\' limit 1');
        ID pmContactId = (ID)pmContact.get('Id');
        system.debug('PM Contact Id = ' + pmContactId);
        
		Account newAccount = new Account(
			Name='TestAcct',
			CurrencyIsoCode='USD');
		insert newAccount;
		System.debug('Account = ' + newAccount.Name);
		System.assertEquals('TestAcct', newAccount.Name);
      
		Account account = [Select Id from Account where Name = 'TestAcct'];
		ID accountId = (ID)account.get('Id');
		system.debug('Current Account Id = ' + accountId);

        Opportunity o = new Opportunity();
        o.AccountId = accountId;
        o.Name = 'Opportunity to Test';
        o.pse__Practice__c = practiceId;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.StageName = 'Won';
        o.CloseDate = date.today();
        o.Start_Date__c = date.today()+1;
        o.pse__Region__c = corpRegionId;
        o.ForecastCategoryName = 'Closed';
        
        insert o;

        pse__Proj__c p = new pse__Proj__c();
        p.Name = 'Test Data for testDeleteAsDEM';
        p.pse__Account__c = accountId;
        p.pse__Opportunity__c = o.Id;
        p.pse__Project_Manager__c = pmContactId;
        p.pse__Region__c = corpRegionId;
        p.pse__Start_Date__c = date.today()+1;
        p.pse__End_Date__c = date.today()+30;
        p.pse__Is_Active__c = true; 
        p.Delivery_Engagement_Manager__c = currentContactId;
        
        insert p;
        
        Project_Deliverable__c pd = new Project_Deliverable__c();
        pd.Project__c = p.Id; 
        pd.Artifact__c = 'Report of Findings';
        
        insert pd;
        
        try {
            delete pd;
        } catch (Dmlexception xcpn) {
            //delete should have worked
            system.debug('Project Deliverable DEM is ' + pd.Delivery_Engagement_Manager__c);
            system.assert(false, xcpn.getMessage());
        }
    }
    
    static testMethod void testDeleteNotAsDEM() {
        sObject corpRegion = Database.query('select Id from pse__Region__c where name = \'Corporate\' limit 1');
        ID corpRegionId = (ID)corpRegion.get('Id');
        system.debug('Current Region Id = ' + corpRegionId);
        
//        sObject account = Database.query('select Id from Account where name = \'Summa Technologies, Inc.\' limit 1');
//        ID accountId = (ID)account.get('Id');
//        system.debug('Current Account Id = ' + accountId);
        
        sObject practice = Database.query('select Id from pse__Practice__c where name = \'App Dev\' limit 1');
        ID practiceId = (ID)practice.get('Id');
        system.debug('Current Practice Id = ' + practiceId);
        
        sObject pmContact = Database.query('select Id from Contact where name = \'Mitch Goldstein\' limit 1');
        ID pmContactId = (ID)pmContact.get('Id');
        system.debug('PM Contact Id = ' + pmContactId);

		Account newAccount = new Account(
			Name='TestAcct',
			CurrencyIsoCode='USD');
		insert newAccount;
		System.debug('Account = ' + newAccount.Name);
		System.assertEquals('TestAcct', newAccount.Name);
      
		Account account = [Select Id from Account where Name = 'TestAcct'];
		ID accountId = (ID)account.get('Id');
		system.debug('Current Account Id = ' + accountId);

        Opportunity o = new Opportunity();
        o.AccountId = accountId;
        o.Name = 'Opportunity to Test';
        o.pse__Practice__c = practiceId;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.StageName = 'Won';
        o.CloseDate = date.today();
        o.Start_Date__c = date.today()+1;
        o.pse__Region__c = corpRegionId;
        o.ForecastCategoryName = 'Closed';
        
        insert o;

        pse__Proj__c p = new pse__Proj__c();
        p.Name = 'Test Data for testDeleteNotAsDEM';
        p.pse__Account__c = accountId;
        p.pse__Opportunity__c = o.Id;
        p.pse__Project_Manager__c = pmContactId;
        p.pse__Region__c = corpRegionId;
        p.pse__Start_Date__c = date.today()+1;
        p.pse__End_Date__c = date.today()+30;
        p.pse__Is_Active__c = true; 
        p.Delivery_Engagement_Manager__c = pmContactId;
        
        insert p;
        
        Project_Deliverable__c pd = new Project_Deliverable__c();
        pd.Project__c = p.Id; 
        pd.Artifact__c = 'Report of Findings';
        
        insert pd;
        
        try {
            delete pd;
        } catch (Dmlexception xcpn) {
        	//delete should fail with specific error message
            system.assert(xcpn.getMessage().contains('You do not have entitlements to delete this record'), xcpn.getMessage());
        } catch (Exception xcpn) {
        	//should not have failed here...
        	system.assert(false, xcpn.getMessage());
        }
    }

    
    static testMethod void testDeleteNoAssignedDEM() {
        sObject corpRegion = Database.query('select Id from pse__Region__c where name = \'Corporate\' limit 1');
        ID corpRegionId = (ID)corpRegion.get('Id');
        system.debug('Current Region Id = ' + corpRegionId);
        
//        sObject account = Database.query('select Id from Account where name = \'Summa Technologies, Inc.\' limit 1');
//        ID accountId = (ID)account.get('Id');
//        system.debug('Current Account Id = ' + accountId);
        
        sObject practice = Database.query('select Id from pse__Practice__c where name = \'App Dev\' limit 1');
        ID practiceId = (ID)practice.get('Id');
        system.debug('Current Practice Id = ' + practiceId);
        
        sObject pmContact = Database.query('select Id from Contact where name = \'Mitch Goldstein\' limit 1');
        ID pmContactId = (ID)pmContact.get('Id');
        system.debug('PM Contact Id = ' + pmContactId);

		Account newAccount = new Account(
			Name='TestAcct',
			CurrencyIsoCode='USD');
		insert newAccount;
		System.debug('Account = ' + newAccount.Name);
		System.assertEquals('TestAcct', newAccount.Name);
      
		Account account = [Select Id from Account where Name = 'TestAcct'];
		ID accountId = (ID)account.get('Id');
		system.debug('Current Account Id = ' + accountId);

        Opportunity o = new Opportunity();
        o.AccountId = accountId;
        o.Name = 'Opportunity to Test';
        o.pse__Practice__c = practiceId;
        o.Primary_Solution_Area__c = 'Application Modernization';
        o.StageName = 'Won';
        o.CloseDate = date.today();
        o.Start_Date__c = date.today()+1;
        o.pse__Region__c = corpRegionId;
        o.ForecastCategoryName = 'Closed';
        
        insert o;

        pse__Proj__c p = new pse__Proj__c();
        p.Name = 'Test Data for testDeleteNoAssignedDEM';
        p.pse__Account__c = accountId;
        p.pse__Opportunity__c = o.Id;
        p.pse__Project_Manager__c = pmContactId;
        p.pse__Region__c = corpRegionId;
        p.pse__Start_Date__c = date.today()+1;
        p.pse__End_Date__c = date.today()+30;
        p.pse__Is_Active__c = true; 
        
        insert p;
        
        Project_Deliverable__c pd = new Project_Deliverable__c();
        pd.Project__c = p.Id; 
        pd.Artifact__c = 'Report of Findings';
        
        insert pd;
        
        try {
            delete pd;
        } catch (Dmlexception xcpn) {
            //delete should have worked
            system.debug('Project Deliverable DEM is ' + pd.Delivery_Engagement_Manager__c);
            system.assert(false, xcpn.getMessage());
        } 
    }
}