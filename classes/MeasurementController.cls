public class MeasurementController {
    public List<MeasurementValue__c> values { get; set; }
    
    public MeasurementController() {
        values = [SELECT MeasurementName__c, Value__c from MeasurementValue__c order by Name];
    }
}