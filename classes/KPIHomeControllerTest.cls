@isTest(seealldata=true)
private class KPIHomeControllerTest {
	static testMethod void controllerTest() {
		KPIHomeController controller = new KPIHomeController();
		List<KPI_Value__c> financialValues = controller.FinancialValues;
		for (KPI_Value__c value : financialValues) System.debug('Financial: ' + value);
		List<KPI_Value__c> customerValues = controller.CustomerValues;
		for (KPI_Value__c value : customerValues) System.debug('Customer: ' + value);
		List<KPI_Value__c> deliveryValues = controller.DeliveryValues;
		for (KPI_Value__c value : deliveryValues) System.debug('Delivery: ' + value);
		List<KPI_Value__c> employeeValues = controller.EmployeeValues;
		for (KPI_Value__c value : employeeValues) System.debug('Employee: ' + value);
	}
}