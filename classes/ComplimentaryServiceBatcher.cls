global class ComplimentaryServiceBatcher implements Database.Batchable<pse__Proj__c>, Database.Stateful {

	private static list<pse__Time_Period__c> timePeriods;
	
	static {
		timePeriods = ComplimentaryServiceBso.getTimePeriods();
	}
	
	global List<pse__Proj__c> start(Database.BatchableContext BC) {
		return ComplimentaryServiceBso.getProjectsForAnalysis();
	}
	
	global void execute(Database.BatchableContext BC, List<pse__Proj__c> projects) {
		system.debug('Number of time periods in batch ['+ timePeriods.size() +']');
		system.debug('Number of projects in batch ['+ projects.size() +']');
		ComplimentaryServiceBso.runAnalysis(timePeriods, projects);
	}
	
	global void finish(Database.BatchableContext BC){

	}
}