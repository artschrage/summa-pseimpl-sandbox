public with sharing class DataFactoryTestAssignments {

	private static list<pse__Assignment__c> assignments;
	
	static {
		assignments = new list<pse__Assignment__c>();
	}
	
	public static void createAssignments() {
		pse__Schedule__c s;
    	pse__Assignment__c a;
    	list<pse__Proj__c> projects = DataFactoryTestProjects.getProjects();
    	map<Id, pse__Schedule__c> projectToScheduleMap = new map<Id, pse__Schedule__c>();
		
    	for (pse__Proj__c project : projects) {
			s = new pse__Schedule__c();
			s.pse__Start_Date__c = project.pse__Start_Date__c;
			s.pse__End_Date__c = project.pse__End_Date__c;
			s.pse__Sunday_Hours__c = 0;
			s.pse__Monday_Hours__c = 8;
			s.pse__Tuesday_Hours__c = 8;
			s.pse__Wednesday_Hours__c = 8;
			s.pse__Thursday_Hours__c = 8;
			s.pse__Friday_Hours__c = 8;
			s.pse__Saturday_Hours__c = 0;
			
			projectToScheduleMap.put(project.Id, s);
    	}

		for (pse__Schedule__c x : projectToScheduleMap.values()) {
			system.debug('Before schedule insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +';');
		}

		insert projectToScheduleMap.values();
		
		for (pse__Schedule__c x : projectToScheduleMap.values()) {
			system.debug('After schedule insert values: id='+ x.id +'; name='+ x.name +'; startDate='+ x.pse__Start_Date__c +'; endDate='+ x.pse__End_Date__c +';');
		}

    	for (pse__Proj__c project : projects) {
   			a = new pse__Assignment__c();
			a.name = DataFactoryTestBasicValues.johnSmail.name + ' on ' + project.name;
			a.pse__Project__c = project.Id;
			a.pse__Resource__c = DataFactoryTestBasicValues.johnSmail.Id;
			a.pse__Schedule__c = projectToScheduleMap.get(project.Id).Id;
			a.pse__Is_Billable__c = false;
			a.pse__Bill_Rate__c = 0;
			a.pse__Cost_Rate_Amount__c = 100;
			a.pse__Cost_Rate_Currency_Code__c = 'USD';
			assignments.add(a);
			
			a = new pse__Assignment__c();
			a.name = DataFactoryTestBasicValues.mitchGoldstein.name + ' on ' + project.name;
			a.pse__Project__c = project.Id;
			a.pse__Resource__c = DataFactoryTestBasicValues.mitchGoldstein.Id;
			a.pse__Schedule__c = projectToScheduleMap.get(project.Id).Id;
			a.pse__Is_Billable__c = true;
			a.pse__Bill_Rate__c = 0;
			a.pse__Cost_Rate_Amount__c = 100;
			a.pse__Cost_Rate_Currency_Code__c = 'USD';
			assignments.add(a);
			
			a = new pse__Assignment__c();
			a.name = DataFactoryTestBasicValues.jeffHowell.name + ' on ' + project.name;
			a.pse__Project__c = project.Id;
			a.pse__Resource__c = DataFactoryTestBasicValues.jeffHowell.Id;
			a.pse__Schedule__c = projectToScheduleMap.get(project.Id).Id;
			a.pse__Is_Billable__c = true;
			a.pse__Bill_Rate__c = 150;
			a.pse__Cost_Rate_Amount__c = 100;
			a.pse__Cost_Rate_Currency_Code__c = 'USD';
			assignments.add(a);
			
			a = new pse__Assignment__c();
			a.name = DataFactoryTestBasicValues.viaTsuji.name + ' on ' + project.name;
			a.pse__Project__c = project.Id;
			a.pse__Resource__c = DataFactoryTestBasicValues.viaTsuji.Id;
			a.pse__Schedule__c = projectToScheduleMap.get(project.Id).Id;
			a.pse__Is_Billable__c = true;
			a.pse__Bill_Rate__c = 150;
			a.Complimentary_Service_True_Override__c = true;
			a.pse__Cost_Rate_Amount__c = 100;
			a.pse__Cost_Rate_Currency_Code__c = 'USD';
			assignments.add(a);
    	}

		for (pse__Assignment__c x : assignments) {
			system.debug('Before assignment insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; isBillable='+ x.pse__Is_Billable__c +'; billRate='+ x.pse__Bill_Rate__c +';');
		}

		insert assignments;
		
		for (pse__Assignment__c x : assignments) {
			system.debug('After assignment insert values: id='+ x.id +'; name='+ x.Name +'; projectId='+ x.pse__Project__c +'; resourceId='+ x.pse__Resource__c +'; isBillable='+ x.pse__Is_Billable__c +'; billRate='+ x.pse__Bill_Rate__c +';');
		}
    }
	
    public static list<pse__Assignment__c> getAssignments() {
    	if (assignments.size() == 0) {
    		assignments = [select id, name, pse__Project__c, pse__Resource__c, pse__Schedule__c, pse__Is_Billable__c, pse__Bill_Rate__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, Complimentary_Service_True_Override__c, pse__Start_Date__c, pse__End_Date__c from pse__Assignment__c where pse__Project__c in :DataFactoryTestProjects.getProjectIds()];
    	} else if (assignments[0].pse__Start_Date__c == null) {
    		assignments.clear();
    		assignments = [select id, name, pse__Project__c, pse__Resource__c, pse__Schedule__c, pse__Is_Billable__c, pse__Bill_Rate__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, Complimentary_Service_True_Override__c, pse__Start_Date__c, pse__End_Date__c from pse__Assignment__c where pse__Project__c in :DataFactoryTestProjects.getProjectIds()];
    	}
    	
    	return assignments;
    }

    public static pse__Assignment__c getAssignment(string assignmentId) {
		return [select id, name, pse__Project__c, pse__Resource__c, pse__Schedule__c, pse__Is_Billable__c, pse__Bill_Rate__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, Complimentary_Service_True_Override__c, pse__Start_Date__c, pse__End_Date__c from pse__Assignment__c where id = :assignmentId];
    }

    public static list<pse__Assignment__c> getProjectAssignments(string projectId) {
		return [select id, name, pse__Project__c, pse__Resource__c, pse__Schedule__c, pse__Is_Billable__c, pse__Bill_Rate__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, Complimentary_Service_True_Override__c, pse__Start_Date__c, pse__End_Date__c from pse__Assignment__c where pse__Project__c = :projectId];
    }

    public static list<pse__Assignment__c> getProjectAssignments(list<string> projectIds) {
		return [select id, name, pse__Project__c, pse__Resource__c, pse__Schedule__c, pse__Is_Billable__c, pse__Bill_Rate__c, pse__Cost_Rate_Amount__c, pse__Cost_Rate_Currency_Code__c, Complimentary_Service_True_Override__c, pse__Start_Date__c, pse__End_Date__c from pse__Assignment__c where pse__Project__c in :projectIds];
    }
}