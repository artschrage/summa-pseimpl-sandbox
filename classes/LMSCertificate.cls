/**
 * NAME: LMSCertificate
 * DEVELOPER: Andrey Melnikov
 * CREATED DATE: 2013/11/29
 *
 * DESCRIPTION:
 * General controller
 * Get Data for Certificate page
 *
 * CHANGE LOG
 * DEVELOPER NAME             CHANGE DESCRIPTION                      MODIFY DATE
 * ------------------------------------------------------------------------------
 * Andrey Melnikov               Created class                           2013/11/29
 * Andrey Melnikov               Updated class                           2014/01/13
 */


public without sharing class LMSCertificate {
/**
    * Method: Constructor LMSCertificate
    *
    * Starting method that get all the values.
    *
    * @param Void
    * @return void
    */

    public LMSCertificate (){

        Id gid = ApexPages.currentPage().getParameters().get('id');

        if(gid==null){
            //if id parameter = null, return error
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Please enter Id.'));
        }

        try {
            //try set values for Course Object, if gid = Course ID
            try {
                lmscons__Training_Path_Assignment_Progress__c  ret = ([select Id, lmscons__Training_Path__r.Name, lmscons__Transcript__r.lmscons__Trainee__r.LastName, lmscons__Transcript__r.lmscons__Trainee__r.FirstName , lmscons__Transcript__r.lmscons__Trainee__r.Name, lmscons__Completion_Date__c
                                                    from lmscons__Training_Path_Assignment_Progress__c
                                                    where Id = :gid Limit 1]);
                //set complition date
                CompletionDate = ret.lmscons__Completion_Date__c;
                try {
                    ExpDate = ret.lmscons__Completion_Date__c.addYears(1);
                } catch (Exception e2) {}
                //set Full User Name
                UserName = ret.lmscons__Transcript__r.lmscons__Trainee__r.Name;
                //Set First Name
                FName = ret.lmscons__Transcript__r.lmscons__Trainee__r.FirstName;
                //Set Last Name
                LName = ret.lmscons__Transcript__r.lmscons__Trainee__r.LastName;
                //Set Title Course
                Title = ret.lmscons__Training_Path__r.Name;
            } catch(system.exception e){
            }
            try {
                //try set values for Module or Quiz, if gid = module ID or quiz id
                lmscons__Transcript_Line__c  ret = ([select Id, lmscons__Transcript__r.lmscons__Trainee__r.LastName, lmscons__Transcript__r.lmscons__Trainee__r.FirstName, lmscons__Transcript__r.lmscons__Trainee__r.Name, lmscons__Content_Title__c, lmscons__Completion_Date__c, lmscons__Trainee__c, lmscons__Score__c, lmscons__Completed_Date__c
                                                    from lmscons__Transcript_Line__c
                                                    where Id = :gid Limit 1]);
                CompletionDate = ret.lmscons__Completion_Date__c;
                try {
                ExpDate = ret.lmscons__Completion_Date__c.addYears(1);
                } catch (Exception e2) {}
                UserName = ret.lmscons__Transcript__r.lmscons__Trainee__r.Name;
                FName = ret.lmscons__Transcript__r.lmscons__Trainee__r.FirstName;
                LName = ret.lmscons__Transcript__r.lmscons__Trainee__r.LastName;
                Title = ret.lmscons__Content_Title__c;

                system.debug('Title: '+Title);
            } catch(system.exception e){
            }
            try {
                //try set values for Learning Path, if gid = Learning Path ID
                lmscons__Learning_Path_Assignment__c  ret = ([select Id, lmscons__Transcript__r.lmscons__Trainee__r.LastName, lmscons__Transcript__r.lmscons__Trainee__r.FirstName, lmscons__Transcript__r.lmscons__Trainee__r.Name, lmscons__Learning_Path__r.Name, lmscons__Completion_Date__c
                                                    from lmscons__Learning_Path_Assignment__c
                                                    where Id = :gid Limit 1]);
                CompletionDate = ret.lmscons__Completion_Date__c;
                try {
                ExpDate = ret.lmscons__Completion_Date__c.addYears(1);
                } catch (Exception e2) {}
                UserName = ret.lmscons__Transcript__r.lmscons__Trainee__r.Name;
                FName = ret.lmscons__Transcript__r.lmscons__Trainee__r.FirstName;
                LName = ret.lmscons__Transcript__r.lmscons__Trainee__r.LastName;
                Title = ret.lmscons__Learning_Path__r.Name;
            } catch(system.exception e){
            }
            try {
                //try set values for Curriculum, if gid = Curriculum ID
                lmscons__Curriculum_Assignment__c  ret = ([select Id, lmscons__Transcript__r.lmscons__Trainee__r.LastName, lmscons__Transcript__r.lmscons__Trainee__r.FirstName, lmscons__Transcript__r.lmscons__Trainee__r.Name,lmscons__Curriculum__r.Name, lmscons__Completion_Date__c
                                                    from lmscons__Curriculum_Assignment__c
                                                    where Id = :gid Limit 1]);
                CompletionDate = ret.lmscons__Completion_Date__c;
                try {
                ExpDate = ret.lmscons__Completion_Date__c.addYears(1);
                } catch (Exception e2) {}
                UserName = ret.lmscons__Transcript__r.lmscons__Trainee__r.Name;
                FName = ret.lmscons__Transcript__r.lmscons__Trainee__r.FirstName;
                LName = ret.lmscons__Transcript__r.lmscons__Trainee__r.LastName;
                Title = ret.lmscons__Curriculum__r.Name;
            } catch(system.exception e){
            }
        } catch(system.exception e){

        }
    }

    public lmscons__Transcript_Line__c returncert;

    public DateTime CompletionDate {get; set;} //Date certificate completed
    public DateTime ExpDate {get; set;} // Date certificate Expired
    public DateTime TodayDate {get {return date.Today(); } set;} // Date certificate Created
    public String UserName {get; set;} //Full user name
    public String FName {get; set;} //First Name
    public String LName {get; set;} // Last Name
    public String Title {get; set;} // Title Learning Object
}