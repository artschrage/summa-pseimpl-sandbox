public with sharing class ExceptionUtils {

	public static void logException(Exception e) {
		Error_Log_Summa__c newRecord = new Error_Log_Summa__c();
		newRecord.Type__c = e.getTypeName();
		if (e.getCause() != null) {
			newRecord.Cause__c = e.getCause().getTypeName();
		}
		newRecord.Line_Number__c = e.getLineNumber();
		newRecord.Message__c = e.getMessage();
		newRecord.Stack_Trace__c = e.getStackTraceString();
		
		logException(newRecord);
	}
	
	public static void logException(Exception e, String additionalInfo) {
		Error_Log_Summa__c newRecord = new Error_Log_Summa__c();
		newRecord.Type__c = e.getTypeName();
		if (e.getCause() != null) {
			newRecord.Cause__c = e.getCause().getTypeName();
		}
		newRecord.Line_Number__c = e.getLineNumber();
		newRecord.Message__c = e.getMessage();
		newRecord.Stack_Trace__c = e.getStackTraceString();
		newRecord.Additional_Information__c = additionalInfo;
		
		logException(newRecord);
	}
	
	private static void logException(Error_Log_Summa__c newRecord) {
		insert newRecord;
	}
}