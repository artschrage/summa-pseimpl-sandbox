public with sharing class KPIDashboardController {

    public List<KPI_Value__c> FinancialValues { get; set; }
    public List<KPI_Value__c> CustomerValues { get; set; }
    public List<KPI_Value__c> DeliveryValues { get; set; }
    public List<KPI_Value__c> EmployeeValues { get; set; }

	private pse__Time_Period__c prevQuarter = CommonDao.getPreviousQuarter();
	private pse__Time_Period__c currentYear = CommonDao.getCurrentYear();

	public KPIDashboardController() {
		System.debug('PrevQuarter: ' + prevQuarter.Name);
		System.debug('CurrentYear: ' + currentYear.Name);

        FinancialValues = [SELECT Id, Name, Title__c, Target__c, Actual__c, Format__c, Time_Period__c 
                    from KPI_Value__c where (Time_Period__c  = :prevQuarter.Id  or
					Time_Period__c = :currentYear.Id) and Category__c = 'Financial' and
					KPI__r.Is_Active__c = true order by Display_Order__c];
        CustomerValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where (Time_Period__c  = :prevQuarter.Id or
					Time_Period__c = :currentYear.Id) and Category__c = 'Customer' and
					KPI__r.Is_Active__c = true order by Display_Order__c];
        DeliveryValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c 
                    from KPI_Value__c where (Time_Period__c  = :prevQuarter.Id or
					Time_Period__c = :currentYear.Id) and Category__c = 'Delivery' and
					KPI__r.Is_Active__c = true order by Display_Order__c];
        EmployeeValues = [SELECT Name, Title__c, Target__c, Actual__c, Format__c
                    from KPI_Value__c where (Time_Period__c  = :prevQuarter.Id or
					Time_Period__c = :currentYear.Id) and Category__c = 'Employee' and
					KPI__r.Is_Active__c = true order by Display_Order__c];
	}

	public boolean hasFinancialValues {
		get 
		{
			return FinancialValues.size() > 0;
		}
	}
}