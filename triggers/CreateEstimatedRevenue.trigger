trigger CreateEstimatedRevenue on Opportunity (after insert, after update) {
//2012****_Dierdre_Pim: created: create estimated revenue records based on opportunity
/*20130107_Dierdre_Pim: updated: if opportunity start date is moved after resource request start date 
	- move RR Start Date to match the opportunity start date and RR End Date to match the original duration of the request*/
	
List<Estimated_Revenue__c> createEstimatedRevenue = new List <Estimated_Revenue__c> {};

for (opportunity o : trigger.new) {

    Estimated_Revenue__c [] accs;
    String Id1=o.Id;
    system.debug('THIS IS WHAT YOURE LOOKING FOR');
    system.debug(o.Id);
    accs= [SELECT Id FROM Estimated_Revenue__c p where p.Opportunity__c = :o.Id];
    
    DELETE accs;
    
    //all resource requests will have an oppty record per the ResourceRequest_Before.trigger
    pse__Resource_Request__c [] rr01;
    
    //select resource requests with opportunity records
    rr01 = [SELECT Id, pse__Start_Date__c, pse__End_Date__c FROM pse__Resource_Request__c rr1 WHERE rr1.pse__Opportunity__c = :o.Id];
    
    //Date rrStartDate = rr01[].pse__start
    
    //Integer rrDuration = rr01[].pse__Start_Date__c.daysBetween(rr01[].pse__End_Date__c);
    
    system.debug('rr01: ' + rr01);
    system.debug('Oppty Start Date' + o.Start_Date__c);
    
    Boolean needsUpdating = false;
    
    for (pse__Resource_Request__c resourceRequest : rr01){
    
    	Integer rrDuration = resourceRequest.pse__Start_Date__c.daysBetween(resourceRequest.pse__End_Date__c);
    	system.debug('rrDuration: ' +rrDuration);
    	
	    if (o.Start_Date__c > resourceRequest.pse__Start_Date__c && o.ForecastCategoryName != 'Omitted' && o.ForecastCategoryName != 'Closed') {
	    	//update resource request start date to opportunity start date
	    	//loop through all related resource requests
	    	 
	    	resourceRequest.pse__Start_Date__c = o.Start_Date__c;
	    	system.debug('rr01 updated' + resourceRequest.pse__Start_Date__c);
	    	
	    	resourceRequest.pse__End_Date__c = o.Start_Date__c.addDays(rrDuration);
	    	system.debug('rr01 updated' + resourceRequest.pse__End_Date__c);
	    	
	    	needsUpdating = true;
	    }
   }
   if (needsUpdating) update rr01;

    if (o.duration__c != null && o.ForecastCategoryName != 'Omitted' && o.ForecastCategoryName != 'Closed') {
    //if (o.duration__c != null && o.Start_Date__c != null && o.ForecastCategoryName != 'Omitted' && o.ForecastCategoryName != 'Closed') {
    //(o.ForecastCategoryName == 'Omitted' || o.ForecastCategoryName != 'Closed')
    Decimal decimalValue = o.Duration__c;  //4.75 
    Integer durationMonths = Integer.valueOf(decimalValue);  //4
    
    Decimal iVal = decimalValue - durationMonths; //4.75 - 4 = 0.75
    
    Decimal iDaysDec = iVal * 30;  //calculate avg number of days in a partial month...
    Integer iDays = Integer.valueOf(iDaysDec);
    
    DATE endOfProject = o.Start_Date__c.addMonths(durationMonths);
    
    if(decimalValue != durationMonths){
    DATE endOfProject_01 = o.Start_Date__c.addDays(iDays);
    endOfProject = endOfProject_01.addMonths(durationMonths);
    }
    
    Integer durationDays = o.Start_Date__c.daysBetween(endOfProject);
    Decimal dollarsPerDay = o.ExpectedRevenue/durationDays;

for( DATE beginningOfMonth=o.Start_Date__c.toStartOfMonth(); beginningOfMonth < endOfProject; beginningOfMonth = beginningOfMonth.addMonths(1))
{
    DATE beginningOfNextMonth = beginningOfMonth.addMonths(1);
    if(beginningOfNextMonth > endOfProject)
        beginningOfNextMonth = endOfProject;
        
    DATE startDate = beginningOfMonth;
    if(startDate < o.Start_Date__c)
    startDate = o.Start_Date__c;
    
    Integer durationOfPeriod = startDate.daysBetween(beginningOfNextMonth);
    Decimal dollarsPerPeriod = dollarsPerDay * durationOfPeriod;
    system.debug('durationOfPeriod: '+ dollarsPerPeriod);
    
    pse__Time_Period__c [] pseTP;
    
    pseTP = [SELECT Id,Name, pse__Start_Date__c from pse__Time_Period__c tP WHERE tP.pse__Type__c = 'Month' AND tP.pse__Start_Date__c=:beginningOfMonth];
    
    system.debug('pseTP: '+ pseTP );
    
    Integer size = pseTP.size();
    system.debug('Size: ' + size );
    

createEstimatedRevenue.add(new Estimated_Revenue__c (
        Opportunity__c = o.Id, 
        amount__c = dollarsPerPeriod, 
        X1st_Of_Month__c = beginningOfMonth,
        Time_Period__c = pseTP[0].Id
        ));
        }

}
}

//try {
insert createEstimatedRevenue;
//}
//catch (Exception Ex)
//{
//system.debug(Ex);
//}
}