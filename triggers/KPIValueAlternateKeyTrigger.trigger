trigger KPIValueAlternateKeyTrigger on KPI_Value__c (before insert, before update) {

	for (KPI_Value__c kpiValue : trigger.new) {
		kpiValue.Alternate_Key__c = kpiValue.kpi__c + ' ' + kpiValue.time_period__c;
	}
}