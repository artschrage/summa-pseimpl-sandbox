trigger RequireRejectionComment on pse__Timecard_Header__c (before update) {

  Map<Id, pse__Timecard_Header__c> rejectedStatements 
             = new Map<Id, pse__Timecard_Header__c>{};

  for(pse__Timecard_Header__c tc: trigger.new)
  {
    /* 
      Get the old object record, and check if the approval status 
      field has been updated to rejected. If so, put it in a map 
      so we only have to use 1 SOQL query to do all checks.
    */
    pse__Timecard_Header__c oldTC = System.Trigger.oldMap.get(tc.Id);

    if (oldTC.pse__Status__c != 'Rejected' 
     && tc.pse__Status__c == 'Rejected')
    { 
      rejectedStatements.put(tc.Id, tc);  
    }
  }
   
  if (!rejectedStatements.isEmpty())  
  {
    /* 
      Get the last approval process for the rejected approvals, 
      and check the comments.
    */
    for (ProcessInstance pi : [SELECT TargetObjectId, 
                              (  
                                 SELECT Id, StepStatus, Comments 
                                 FROM Steps
                                 WHERE StepStatus = 'Rejected'
                                 ORDER BY CreatedDate DESC
                                 LIMIT 1 
                              )
                               FROM ProcessInstance
                               WHERE TargetObjectId In 
                                 :rejectedStatements.keySet()
                               ORDER BY CreatedDate DESC
                              ])
    {                   
      if ((pi.Steps[0].Comments == null || 
           pi.Steps[0].Comments.trim().length() == 0))
      {
        rejectedStatements.get(pi.TargetObjectId).addError(
          'Timecard Rejection Cancelled: Please provide a rejection reason in the Comments section!\n<br/>Click your browser back button to return to the previous page.');
      }
    }  
  }

}