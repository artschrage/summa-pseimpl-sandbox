trigger UpdateOpportunityPractice on Opportunity (before insert, before update) {

Set<Id> projectIds = new Set<Id>();
List<pse__Proj__c> projList = new List<pse__Proj__c>();
Map<Id,Id> projMap = new Map<Id,Id>();

//pse__Practice__c practice_CRM = [Select Id from pse__Practice__c where Name='Salesforce'];
pse__Practice__c practice_CRM = [Select Id from pse__Practice__c where Name='Salesforce'];
pse__Practice__c practice_SMM = [Select Id from pse__Practice__c where Name='SMM'];
pse__Practice__c practice_Core = [Select Id from pse__Practice__c where Name='App Dev'];
//if(!Test.isRunningTest()){
for (Opportunity opp : Trigger.new){
	
	if (opp.Solution_Area1__c == 'SFDC - Sales Cloud' || opp.Solution_Area1__c == 'SFDC - Service Cloud' || opp.Solution_Area1__c == 'SFDC - Managed Services' || opp.Solution_Area1__c == 'SFDC - Heroku/Force.com' || opp.Solution_Area1__c == 'SFDC - Other'){
	opp.pse__Practice__c = practice_CRM.Id;
	}
	else if (opp.Solution_Area1__c == 'SMM - Sales Cloud' || opp.Solution_Area1__c == 'SMM - Service Cloud' || opp.Solution_Area1__c == 'SMM - Managed Services'){
	opp.pse__Practice__c = practice_SMM.Id;
	}
	else if (opp.Solution_Area1__c == 'Software IBM' || opp.Solution_Area1__c == 'Software Rally' || opp.Solution_Area1__c == 'Software Salesforce'){
	opp.pse__Practice__c = null;
	}
	else{
	opp.pse__Practice__c = practice_Core.Id;
	}

	if (opp.pse__Primary_Project__c != null){
	projectIds.add(opp.pse__Primary_Project__c);
	projMap.put(opp.pse__Primary_Project__c, opp.pse__Practice__c);
	}
	
}
for (pse__Proj__c proj: [Select Id, pse__Practice__c from pse__Proj__c where Id IN :projectIds]){
	proj.pse__Practice__c = projMap.get(proj.Id);
	if (proj != null){
	projList.add(proj);
	}
}
if (!projList.isEmpty()){
update projList;
}
//}
}