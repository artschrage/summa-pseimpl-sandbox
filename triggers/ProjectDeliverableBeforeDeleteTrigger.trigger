trigger ProjectDeliverableBeforeDeleteTrigger on Project_Deliverable__c (before delete) {
	// @author = John Smail
	// In order to delete the Project Deliverable record, the Project must either:
	//  a) have no DEM assigned, or
	//  b) have an assigned DEM, which is the current user

	String currentUserId = UserInfo.getUserId();
    sObject currentUserContact = Database.query('SELECT Id FROM Contact WHERE pse__Salesforce_User__c = :currentUserId LIMIT 1');    
	ID contactId = (ID)currentUserContact.get('Id');
    
    for (Project_Deliverable__c pd: trigger.old) {
        if (! String.isBlank(pd.Delivery_Engagement_Manager__c)) {
        	ID demId = ID.valueOf(pd.Delivery_Engagement_Manager__c);
            if (demId != contactId) {
                trigger.oldMap.get(pd.Id).addError('You do not have entitlements to delete this record');
            } 
        }
    }
}