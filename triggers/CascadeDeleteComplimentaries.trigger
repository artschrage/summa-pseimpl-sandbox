trigger CascadeDeleteComplimentaries on pse__Proj__c (before delete) {
    // Select all CS records with projects that are being deleted and whack em
    List<Complimentary_Service__c> comps = [SELECT ID from Complimentary_Service__c WHERE Project__c in :trigger.oldMap.keySet()]; 
    
    if (comps != null && comps.size() > 0) {
		system.debug('Trigger CascadeDeleteComplimentaries attempting to delete ['+ comps.size() +'] Complimentary Services records for ['+ trigger.oldMap.keySet().size() +'] Projects...');
		delete comps;
		system.debug('Trigger CascadeDeleteComplimentaries successfully completed.');
    }
}