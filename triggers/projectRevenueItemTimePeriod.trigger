trigger projectRevenueItemTimePeriod on ProjectRevenueItem__c (before insert, before update) {

List <ProjectRevenueItem__c> updateTimePeriod = new List <ProjectRevenueItem__c>{};

for (ProjectRevenueItem__c PRI : trigger.new){

Integer m1 = integer.valueOf(PRI.Month__c);
Integer y1 = integer.valueOf(PRI.Year__c);

PRI.First_Of_Month__c = date.newInstance(y1,m1,1);

pse__Time_Period__c [] pseTP;

//Id pseTP;

pseTP = [SELECT Id,Name from pse__Time_Period__c tP WHERE tP.pse__Type__c = 'Month' AND tP.pse__Start_Date__c=:PRI.First_Of_Month__c];

system.debug('pseTP: '+ pseTP );

PRI.Time_Period__c = pseTP[0].Id;

}
}