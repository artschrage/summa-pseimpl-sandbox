trigger UpdateAccountSummaHistory on Opportunity (after update) {
Opportunity theOpportunity = trigger.new[0];
//System.debug('StageName = ' +theOpportunity.StageName);
//System.debug('Practice = ' +theOpportunity.pse__Practice__c);
pse__Practice__c practiceName = [Select Name from pse__Practice__c where Id=:theOpportunity.pse__Practice__c];
//System.debug('Practice Name = ' +practiceName.Name);
Account theAccount = [Select Type, BTS__c, Core__c, CRM__c, UX__c from Account where Id=:theOpportunity.AccountId];
if(theOpportunity.StageName =='Won'){
//System.debug('We are in the StageName IF');
    if(theAccount.Type !='Customer'){
    theAccount.Type='Customer';
    }

    if(practiceName.Name == 'BTS'){
    theAccount.BTS__c='WON';
    } else if(practiceName.Name == 'Core'){
    theAccount.Core__c='WON';
    } else if(practiceName.Name =='CRM'){
    theAccount.CRM__c='WON';
    } else if(practiceName.Name == 'UX Design'){
    theAccount.UX__c='WON';
    }
update theAccount;
} else if (theOpportunity.StageName == 'Lost'){
    if(practiceName.Name == 'BTS' && theAccount.BTS__c != 'WON'){
    theAccount.BTS__c='LOST';
    } else if(practiceName.Name == 'Core' && theAccount.Core__c != 'WON'){
    theAccount.Core__c='LOST';
    } else if(practiceName.Name =='CRM' && theAccount.CRM__c != 'WON'){
    theAccount.CRM__c='LOST';
    } else if(practiceName.Name == 'UX Design' && theAccount.UX__c != 'WON'){
    theAccount.UX__c='LOST';
    }
update theAccount;
}
}