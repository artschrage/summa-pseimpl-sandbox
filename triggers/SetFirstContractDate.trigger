trigger SetFirstContractDate on Opportunity (before update) {
/*    
Opportunity theOpportunity = trigger.new[0];
Account theAccount = [Select Type, First_Contract_Date__c from Account where Id=:theOpportunity.AccountId];
List<Account> fcd = [
    SELECT First_Contract_Date__c
    FROM Account
    WHERE Id=:theOpportunity.AccountId
    AND First_Contract_Date__c != null 
    ORDER BY First_Contract_Date__c DESC
    LIMIT 1
];

if(theOpportunity.StageName =='Won'){

    if(theAccount.Type !='Customer'){
    theAccount.Type='Customer';
    }
    
    if(fcd.IsEmpty()) {
    theAccount.First_Contract_Date__c = Date.today();
    } 
 update theAccount;
}
*/
Set<Id> acctIds = new Set<Id>();
List<Account> acctList = new List<Account>();

for (Opportunity opp : Trigger.new){
    if (opp.StageName == 'Won'){
        acctIds.add(opp.AccountId);
    }
}
for (Account acct : [Select Id, Type, First_Contract_Date__c from Account where Id IN : acctIds]){
    acct.Type = 'Customer';
    if(acct.First_Contract_Date__c == null){
        acct.First_Contract_Date__c = Date.today();
    }
    acctList.add(acct);
}
update acctList;
}