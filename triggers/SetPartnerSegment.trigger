trigger SetPartnerSegment on Opportunity (before update) {
Set<Id> oppIds = new Set<Id>();
Set<Id> acctIds = new Set<Id>();
List<Account> acctList = new List<Account>();
List<Opportunity> oppList = new List<Opportunity>();
Map<Id,String> acctMap = new Map<Id,String>();

for (Opportunity opp : Trigger.new){
    if (opp.StageName == 'Won' && (opp.Type == 'Software' || opp.Primary_Solution_Area__c == 'Salesforce.com' || opp.Primary_Solution_Area__c == 'Salesforce.com-MidMarket' || opp.Secondary_Solution_Area__c == 'Salesforce.com' || opp.Secondary_Solution_Area__c == 'Salesforce.com-MidMarket')) {
        oppIds.add(opp.Id);
        acctIds.add(opp.AccountId);
    }
}
for (Opportunity opps : [Select Id, Type, Software_Type__c, Primary_Solution_Area__c, Secondary_Solution_Area__c, AccountId, Account.Partner_Segment__c from Opportunity where Id IN : oppIds]){
    
    if(opps.Type == 'Software'){
       if(opps.Software_Type__c == 'IBM New Licenses' || opps.Software_Type__c == 'IBM Renewal' || opps.Software_Type__c == 'IBM SVI'){
       	  opps.Account.Partner_Segment__c = opps.Account.Partner_Segment__c + ';IBM';
       } else if(opps.Software_Type__c == 'Rally Referral Fee'){
       	  opps.Account.Partner_Segment__c = opps.Account.Partner_Segment__c + ';Rally';
       } else if(opps.Software_Type__c == 'Apttus Referral Fee'){
       	  opps.Account.Partner_Segment__c = opps.Account.Partner_Segment__c + ';Apttus';
       } else if(opps.Software_Type__c == 'Jitterbit Referral Fee'){
       	  opps.Account.Partner_Segment__c = opps.Account.Partner_Segment__c + ';Jitterbit';
       } else if(opps.Software_Type__c == 'Salesforce Referral Fee'){
       	  opps.Account.Partner_Segment__c = opps.Account.Partner_Segment__c + ';Salesforce';
       } 
    } else if(opps.Primary_Solution_Area__c == 'Salesforce.com' || opps.Primary_Solution_Area__c == 'Salesforce.com-MidMarket' || opps.Secondary_Solution_Area__c == 'Salesforce.com' || opps.Secondary_Solution_Area__c == 'Salesforce.com-MidMarket'){
    	opps.Account.Partner_Segment__c = opps.Account.Partner_Segment__c + ';Salesforce';
    }
    acctMap.put(opps.AccountId,opps.Account.Partner_Segment__c);
    //acctList.add(acct);
}
for (Account acct : [Select Id, Partner_Segment__c from Account where Id IN :acctIds]){
	acct.Partner_Segment__c = acctMap.get(acct.Id);
	acctList.add(acct);
}
update acctList;
}