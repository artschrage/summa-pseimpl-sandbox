trigger PSR_After on Project_Status_Report__c (after insert, after update) {

/* trigger built to update the Project Project Status when a Project Status Report is submitted*/

List<Id> projectIds = new List<Id>();
//Set<Id> projectIds = new Set<Id>();

List<pse__Proj__c> updateProjectStatus = new List<pse__Proj__c>();

for(Project_Status_Report__c psr : Trigger.New){
    projectIds.add(psr.Project__c);
    }
    
Map<Id, pse__Proj__c> devMap = new Map<Id, pse__Proj__c>();

   
for(pse__Proj__c proj : [SELECT Id, pse__Project_Status__c FROM pse__Proj__c WHERE Id IN:projectIds])
   {
   devMap.put(proj.ID ,proj );
   system.debug('Project Project Status 01: ' + proj.pse__Project_Status__c);
   }
   
For(Project_Status_Report__c psr : Trigger.New)
    {
   
    system.debug('PSR Project Status: ' + psr.project_Status__c);
    pse__Proj__c updateProject = devMap.get(psr.Project__c);
    system.debug('updateProject: ' + updateProject);
    updateProject.pse__Project_Status__c = psr.Project_Status__c;
    system.debug('Project Project Status: ' + updateProject.pse__Project_Status__c);
    update updateProject;

    }
        
}