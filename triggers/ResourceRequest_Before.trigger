trigger ResourceRequest_Before on pse__Resource_Request__c (before insert, before update) {
//20130104_Dierdre_Pim: created: if resource request is created with a project and no opportunity - update the opportunity lookup field.

    List<pse__Resource_Request__c> newRR = new List<pse__Resource_Request__c>();
    
    pse__Resource_Request__c [] rr01;
    
    pse__Proj__c [] proj01;
    pse__Proj__c [] proj02;
    Opportunity [] oppty01;
    
    for(pse__Resource_Request__c thisRR :trigger.new){
    
	    proj01 = [SELECT Id FROM pse__Proj__c proj WHERE proj.Id = :thisRR.pse__Project__c];
	    
	    proj02 = [SELECT pse__Proj__c.pse__Opportunity__c FROM pse__Proj__c proj WHERE proj.Id = :thisRR.pse__Project__c];
	    
	    oppty01 = [SELECT Id, Start_Date__c, ForecastCategoryName FROM Opportunity oppty WHERE oppty.Id = :thisRR.pse__Opportunity__c];
	    
	    Integer rrDuration = thisRR.pse__Start_Date__c.daysBetween(thisRR.pse__End_Date__c);
	    
	    system.debug('proj01: '+proj01);
	    
	    
	    if (thisRR.pse__Opportunity__c == null) {
	    	system.debug('proj02: '+proj02);
	    	thisRR.pse__Opportunity__c = proj02[0].pse__Opportunity__c;
		}
		
		if(thisRR.Opportunity_Start_Date__c > thisRR.pse__Start_Date__c && thisRR.Forecast_Category__c != 'Omitted' && thisRR.Forecast_Category__c != 'Closed'){
			//Forecast_Category__c
			thisRR.pse__Start_Date__c = thisRR.Opportunity_Start_Date__c;
			thisRR.pse__End_Date__c = thisRR.Opportunity_Start_Date__c.addDays(rrDuration);
			
		}
    	
    }
    
}