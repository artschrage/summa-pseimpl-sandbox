<apex:page controller="KPIHomeController">
    <style type="text/css">
        <!-- Default table text to white -->
        .pbBody td { color:White; font-size:16px;}
        
        <!-- Surround each table with a rounded rectangle -->
        .financialClass, .deliveryClass, .customerClass, .employeeClass
        { 
            margin-top:10px;
            margin-bottom:10px;
            border:2px solid; 
            border-radius:25px; 
            padding:10px;
            font-size:16px;
            color:Yellow;
        }
        .financialClass { background-color:DarkGreen; }
        .customerClass { background-color:#CC6600; }
        .deliveryClass { background-color:MidnightBlue; }
        .employeeClass { background-color:Indigo; }

    </style>
    <apex:sectionHeader title="KPI Values"/>
    <apex:pageBlock title="Previous Fiscal Quarter = [xxx]">
        <apex:panelGrid columns="2" >
    		<apex:pageBlock rendered="{!HasFinancialValues}">
            <apex:dataTable value="{!FinancialValues}" var="finKPI" id="financialTable" 
                                                    styleClass="financialClass">
                <apex:column width="240" >
                    <apex:facet name="header">Financial</apex:facet>
                    <apex:outputText value="{!finKPI.Title__c}"/>
                </apex:column>
                <apex:column width="90" style="text-align:right">
                    <apex:facet name="header">Target</apex:facet>
                    <apex:outputText value="{0, number, {!finKPI.Format__c}}">
                        <apex:param value="{!finKPI.Target__c}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column width="90" style="text-align:right">
                    <apex:facet name="header">Actual</apex:facet>
                    <apex:outputText value="{0, number, {!finKPI.Format__c}}">
                        <apex:param value="{!finKPI.Actual__c}"/>
                    </apex:outputText>
                </apex:column>
            </apex:dataTable>
            </apex:pageBlock>
    		<apex:pageBlock rendered="{!HasCustomerValues}">
            <apex:dataTable value="{!CustomerValues}" var="custKPI" id="customerTable" 
                        styleClass="customerClass">
                <apex:column width="240" >
                    <apex:facet name="header">Customer</apex:facet>
                    <apex:outputText value="{!custKPI.Title__c}"/>
                </apex:column>
                <apex:column width="90" style="text-align:right">
                    <apex:facet name="header">Target</apex:facet>
                    <apex:outputText value="{0, number, {!custKPI.Format__c}}">
                        <apex:param value="{!custKPI.Target__c}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column width="90" style="text-align:right">
                    <apex:facet name="header">Actual</apex:facet>
                    <apex:outputText value="{0, number, {!custKPI.Format__c}}">
                        <apex:param value="{!custKPI.Actual__c}"/>
                    </apex:outputText>
                </apex:column>
            </apex:dataTable>
            </apex:pageBlock>
    		<apex:pageBlock rendered="{!HasDeliveryValues}">
            <apex:dataTable value="{!DeliveryValues}" var="delKPI" id="deliveryTable" 
                        styleClass="deliveryClass">
                <apex:column width="240" >
                    <apex:facet name="header">Delivery</apex:facet>
                    <apex:outputText value="{!delKPI.Title__c}"/>
                </apex:column>
                <apex:column width="100" style="text-align:right">
                    <apex:facet name="header">Target</apex:facet>
                    <apex:outputText value="{0, number, {!delKPI.Format__c}}">
                        <apex:param value="{!delKPI.Target__c}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column width="100" style="text-align:right">
                    <apex:facet name="header">Actual</apex:facet>
                    <apex:outputText value="{0, number, {!delKPI.Format__c}}">
                        <apex:param value="{!delKPI.Actual__c}"/>
                    </apex:outputText>
                </apex:column>
            </apex:dataTable>
            </apex:pageBlock>
    		<apex:pageBlock rendered="{!HasEmployeeValues}">
            <apex:dataTable value="{!EmployeeValues}" var="empKPI" id="employeeTable" 
                        styleClass="employeeClass">
                <apex:column width="240" >
                    <apex:facet name="header">Employee</apex:facet>
                    <apex:outputText value="{!empKPI.Title__c}"/>
                </apex:column>
                <apex:column width="90" style="text-align:right">
                    <apex:facet name="header">Target</apex:facet>
                    <apex:outputText value="{0, number, {!empKPI.Format__c}}">
                        <apex:param value="{!empKPI.Target__c}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column width="90" style="text-align:right">
                    <apex:facet name="header">Actual</apex:facet>
                    <apex:outputText value="{0, number, {!empKPI.Format__c}}">
                        <apex:param value="{!empKPI.Actual__c}"/>
                    </apex:outputText>
                </apex:column>
            </apex:dataTable>
            </apex:pageBlock>
        </apex:panelGrid>
    </apex:pageBlock>
</apex:page>